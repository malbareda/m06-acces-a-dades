@extends('layouts.app')

@section('content')
<div class="container">
                @if (Auth::check())
                        <h2>AAAAAATasks List for {{$nUsers}} users</h2>
                        <table class="table">
                            <thead><tr>
                                <th colspan="2">Taskasasdas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!--Aqui hi ha dos bucles:
                                el primer son tots els usuaris de la collection d'usuaris
                                el segon son totes les tasques per a cada usuari -->
                            @foreach($users as $user)
                                @foreach($user->tasks as $task)
                                <tr>
                                    <td>
                                        {{$user->name}}
                                    </td>

                                    <td>
                                        {{$task->description}}
                                    </td>
                                    <td>
                                        {{$task->created_at}}
                                    </td>

                                    <td>

                                    </td>
                                </tr>

                                @endforeach
                            @endforeach
                        </tbody>
                        </table>
                @else
                    <h3>You need to log in. <a href="/login">Click here to login</a></h3>
                @endif

</div>
@endsection
