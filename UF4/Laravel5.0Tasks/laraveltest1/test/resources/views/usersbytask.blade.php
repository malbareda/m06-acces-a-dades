@extends('layouts.app')

@section('content')
<div class="container">
                @if (Auth::check())
                        <h2>AAAAAATasks List for users</h2>
                        <table class="table">
                            <thead><tr>
                                <th colspan="2">{{sizeof($count)}}</th>
                            </tr>
                        </thead>
                        <tbody>@foreach($count as $c)
                            <tr>
                                <td>
                                    {{$c->user_id}}
                                </td>
                                <td>
                                    {{$c->nom}}
                                </td>
                                <td>
                                    {{$c->tasques}}
                                </td>
                            </tr>


                        @endforeach</tbody>
                        </table>


                @else
                    <h3>You need to log in. <a href="/login">Click here to login</a></h3>
                @endif

</div>
@endsection
