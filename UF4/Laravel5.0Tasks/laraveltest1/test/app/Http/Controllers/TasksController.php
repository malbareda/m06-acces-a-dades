<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Task;
//importa tota la DB
use DB;
//importa el model User, equivalent a la taula users
use App\User;

class TasksController extends Controller
{

    public function listwusers(){

        //Eloquent, metode d'acces a tots els ususaris
        /**
         * Metodes d'acces a eloquent
         * all()  retorna tot com a collection
         * find(1) retorna el que te com a id 1
         * where('camp','valor') retorna els objectes
         *  que en el seu camp tinguin el valor
         */
        //puc cridar a User perque es un model que esta a app
        $users = User::all();
        //en el compact pots tornar una classe de model
        //ho tornara com un objecte/array de php
        $nUsers = DB::table('users')->count();
        $numUsers = User::list()->count();
        return view('all',compact('users','nUsers'));
    }


    public function index()
    {
    	$user = Auth::user();
    	return view('welcome',compact('user'));
    }

    public function add()
    {
    	return view('add');
    }

    public function create(Request $request)
    {
        ///creas nueva tarea
        $task = new Task();
        //pones en la descripcion el campo description del formulario
        $task->description = $request->description;
        //pones en la llave foranea el id
        $task->user_id = Auth::id();
        //guardas en BD
        $task->save();
        //redirecciona
    	return redirect('/');
    }

    public function edit(Task $task)
    {

    	if (Auth::check() && Auth::user()->id == $task->user_id)
        {
                return view('edit', compact('task'));
        }
        else {
             return redirect('/');
         }
    }

    public function update(Request $request, Task $task)
    {
    	if(isset($_POST['delete'])) {
    		$task->delete();
    		return redirect('/');
    	}
    	else
    	{
    		$task->description = $request->description;
	    	$task->save();
	    	return redirect('/');
    	}
    }

    public function list()
    {
        ///agafo la taula tasks de la database, i agafo tot
        $tasks = DB::table('tasks')->get();
        ///els aggregations com count o avg es posen despres de la query
        $nUsers = DB::table('users')->count();
        //$numUsers = User::all()->count();
        $ntasks = DB::table('tasks')->count();
        //si vols passar multiples parametres els poses al compact
        return view('allbackup',compact('tasks','nUsers','ntasks'));
    }


    public function assign()
    {
        $users = User::where('name','<>','admin')->orderBy('name','desc')->get();
        return view('assign',compact('users'));

    }

    public function usersbytask()
    {
        $count = Task::join('users','users.id','=','tasks.user_id')->select(DB::Raw('user_id, users.name as nom, COUNT(*) as tasques'))->groupBy('user_id')->orderBy('tasques','asc')->get();
        return view('usersbytask',compact('count'));

    }

    public function give(Request $request)
    {
        $task = new Task();
    	$task->description = $request->description;
    	$task->user_id = $request->username;
    	$task->save();
    	return redirect('/');
    }
}
