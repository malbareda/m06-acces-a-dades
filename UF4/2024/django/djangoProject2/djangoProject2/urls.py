"""
URL configuration for djangoProject2 project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from examenprova import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('/', views.CatalegLlibresView.as_view(), name='cataleg_llibres'),
    path('llibres/create/', views.CrearLlibreView.as_view(), name='crear_llibre'),
    path('prestecs/crear/<int:id_llibre>/<int:id_membre>/', views.CrearPrestecView.as_view(), name='crear_prestec'),
    path('llibres/cataleg/', views.CatalegLlibresView.as_view(), name='cataleg_llibres'),
    path('llibres/eliminar/<int:id_llibre>/', views.EliminarLlibreView.as_view(), name='eliminar_llibre'),
    path('llibres/senseprestecs/', views.LlibresSensePrestecView.as_view(), name='llibres_sense_prestecs'),
]
