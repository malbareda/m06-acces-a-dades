# Create your models here.
from django.db import models


class Llibre(models.Model):

    CATEGORIA_CHOICES = [
        ('FICCIO', 'Ficció'),
        ('NOFICCIO', 'No Ficció'),
    ]

    titol = models.CharField(max_length=200)
    categoria = models.CharField(max_length=10, choices=CATEGORIA_CHOICES)


class Membre(models.Model):
    nom = models.CharField(max_length=100)


class Prestec(models.Model):
    llibre = models.ForeignKey(Llibre, on_delete=models.CASCADE, related_name='prestecs')
    membre = models.ForeignKey(Membre, on_delete=models.CASCADE, related_name='prestecs')
    actiu = models.BooleanField(default=True)
    data = models.DateTimeField(auto_now_add=True)
