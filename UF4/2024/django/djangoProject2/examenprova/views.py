from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.views.generic import View, ListView

from examenprova.models import Llibre, Membre, Prestec


class CrearLlibreView(View):
    template_name = 'create.html'

    def get(self, request):
        # Passem les opcions de categoria al template
        categories = Llibre.CATEGORIA_CHOICES
        return render(request, self.template_name, {'categories': categories})

    def post(self, request):
        titol = request.POST.get('titol')
        categoria = request.POST.get('categoria')

        if titol and categoria:
            Llibre.objects.create(
                titol=titol,
                categoria=categoria
            )
            return redirect('cataleg_llibres')

        # Si hi ha errors, tornem al formulari amb les dades
        categories = Llibre.CATEGORIA_CHOICES
        return render(request, self.template_name, {
            'categories': categories,
            'error': 'Si us plau, completa tots els camps',
            'titol_value': titol,
            'categoria_value': categoria
        })


class CrearPrestecView(View):
    def get(self, request, id_llibre, id_membre):
        llibre = get_object_or_404(Llibre, id=id_llibre)
        membre = get_object_or_404(Membre, id=id_membre)

        prestec = Prestec(
            llibre=llibre,
            membre=membre,
            actiu=True,
            data=timezone.now()
        )
        prestec.save()

        return redirect('cataleg_llibres')


class CatalegLlibresView(ListView):
    model = Llibre
    template_name = 'cataleg.html'
    context_object_name = 'llibres'

    def get_queryset(self):
        return Llibre.objects.all()

class EliminarLlibreView(View):
    def get(self, request, id_llibre):
        llibre = get_object_or_404(Llibre, id=id_llibre)
        llibre.delete()
        return redirect('cataleg_llibres')

class LlibresSensePrestecView(ListView):
    model = Llibre
    template_name = 'cataleg.html'
    context_object_name = 'llibres'

    def get_queryset(self):
        return Llibre.objects.filter(prestecs__isnull=True)