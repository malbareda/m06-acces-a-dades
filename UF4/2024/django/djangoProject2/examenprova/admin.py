from django.contrib import admin
from .models import Llibre, Membre, Prestec

admin.site.register(Llibre)
admin.site.register(Membre)
admin.site.register(Prestec)
