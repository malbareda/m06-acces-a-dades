# Models a Django

## 1. Instruccions Generals del Model

Els models a Django són la font única de veritat sobre les teves dades. Contenen els camps i comportaments essencials de les dades que estàs emmagatzemant.

```python
from django.db import models

class NomModel(models.Model):
    # Els teus camps aquí
    
    class Meta:
        # Metadades del model
        ordering = ['-data_creacio']
        verbose_name = 'Nom en singular'
        verbose_name_plural = 'Nom en plural'
        
    def __str__(self):
        return self.camp_representatiu
        
    def metode_personalitzat(self):
        # Lògica personalitzada
        pass
```

### Conceptes Clau:
- Cada model és una classe de Python que hereta de `django.db.models.Model`
- Cada atribut de la classe representa un camp a la base de dades
- Django proporciona una API d'abstracció de base de dades automàtica

## 2. Tipus de Fields

### Fields de Text
```python
# Text curt
text_curt = models.CharField(max_length=100)
# Text llarg
text_llarg = models.TextField()
# Email
email = models.EmailField()
# URL
url = models.URLField()
# Slug (per URLs)
slug = models.SlugField()
```

### Fields Numèrics
```python
# Enter
enter = models.IntegerField()
# Enter petit
enter_petit = models.SmallIntegerField()
# Enter gran
enter_gran = models.BigIntegerField()
# Decimal
decimal = models.DecimalField(max_digits=5, decimal_places=2)
# Float
float = models.FloatField()
# Autoincrement
auto = models.AutoField(primary_key=True)
```

### Fields de Data/Hora
```python
# Data
data = models.DateField()
# Hora
hora = models.TimeField()
# Data i hora
data_hora = models.DateTimeField()
# Duració
duracio = models.DurationField()
```

### Fields Binaris
```python
# Booleà
actiu = models.BooleanField(default=False)
# Binari
binari = models.BinaryField()
# Fitxer
fitxer = models.FileField(upload_to='documents/')
# Imatge
imatge = models.ImageField(upload_to='imatges/')
```

### Fields Especials
```python
# JSON
json = models.JSONField()
# UUID
uuid = models.UUIDField()
# IP
ip = models.GenericIPAddressField()
```

## 3. Tipus de Relacions

### One-to-Many (ForeignKey)
```python
class Comentari(models.Model):
    post = models.ForeignKey(
        'Post',
        on_delete=models.CASCADE,
        related_name='comentaris'
    )
```

### Many-to-Many
```python
class Article(models.Model):
    tags = models.ManyToManyField(
        'Tag',
        related_name='articles',
        blank=True
    )
```

### One-to-One
```python
class Perfil(models.Model):
    usuari = models.OneToOneField(
        'User',
        on_delete=models.CASCADE,
        related_name='perfil'
    )
```

### Opcions de on_delete
- `CASCADE`: Elimina en cascada
- `PROTECT`: Evita l'eliminació
- `SET_NULL`: Estableix a NULL
- `SET_DEFAULT`: Estableix al valor per defecte
- `DO_NOTHING`: No fa res

## 4. Arguments Comuns dels Fields

### Arguments Bàsics amb Exemples

```python
class Ship(models.Model):
    # CharField amb verbose_name
    nom = models.CharField(
        verbose_name='Nom del ship',  # Nom que es mostra al admin/forms
        max_length=100
    )
    
    # TextField que pot ser NULL i buit
    descripcio = models.TextField(
        verbose_name='Descripció',
        null=True,       # Permet NULL a la base de dades
        blank=True      # Permet que el camp estigui buit als formularis
    )
    
    # DateTimeField amb default
    data_creacio = models.DateTimeField(
        verbose_name='Data de creació',
        default=timezone.now  # Valor per defecte
    )
    
    # IntegerField amb tots els arguments comuns
    nivell_popularitat = models.IntegerField(
        verbose_name='Nivell de popularitat',
        null=True,
        blank=True,
        default=0,
        help_text='Número de l\'1 al 100 que indica la popularitat',
        validators=[
            MinValueValidator(1),
            MaxValueValidator(100)
        ]
    )
    
    # BooleanField amb default
    actiu = models.BooleanField(
        verbose_name='Actiu',
        default=False    # Per defecte estarà desactivat
    )
    
    # Altres arguments útils
    meta_tags = models.CharField(
        max_length=255,
        unique=True,         # Valor únic a la BD
        db_index=True,       # Crea índex per cerques
        editable=False      # No es pot editar al admin
    )
```

Nota: És una bona pràctica sempre posar `verbose_name` als camps per tenir una interfície d'administració més amigable. Els arguments `null` i `blank` són diferents:
- `null=True`: Permet valors NULL a la base de dades
- `blank=True`: Permet que el camp estigui buit als formularis

Per `CharField` i `TextField`:
- Si `blank=True` però `null=False`: Es guardarà com string buit ('')
- Si ambdós són `True`: Es guardarà NULL quan estigui buit
```

### Exemple de Choices
```python
class Ship(models.Model):
    TIPUS_SHIP = [
        ('CAN', 'Canon - Parella oficial de la història'),
        ('FAN', 'Fanon - Parella acceptada pels fans'),
        ('CRK', 'Crack - Parella impossible/peculiar'),
    ]
    
    tipus = models.CharField(
        max_length=3,
        choices=TIPUS_SHIP,
        default='FAN'
    )
```

### Arguments Específics per Tipus
- **CharField/TextField**: `max_length`
- **DecimalField**: `max_digits`, `decimal_places`
- **FileField/ImageField**: `upload_to`
- **ForeignKey/ManyToManyField/OneToOneField**: 
  - `to` (model relacionat)
  - `on_delete` (comportament en eliminar)
  - `related_name` (nom invers de la relació)
  - `limit_choices_to` (filtre de opcions)
  - `through` (model intermedi per M2M)
