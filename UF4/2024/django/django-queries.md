# Queries a Django: objects.all() i objects.filter()

## 1. Mètodes Bàsics

### objects.all()
```python
# Retorna tots els objectes
shipeos = Shipeos.objects.all()
```

### objects.filter()
```python
# Filtra per condicions exactes
shipeos = Shipeos.objects.filter(isActive=True)
```

### objects.exclude()
```python
# Exclou elements que compleixen la condició
shipeos = Shipeos.objects.exclude(isActive=False)
```

## 2. Field Lookups

### Comparadors Bàsics
```python
# Exact match (case sensitive)
Model.objects.filter(camp__exact="valor")
# També es pot escriure com:
Model.objects.filter(camp="valor")

# Case insensitive
Model.objects.filter(camp__iexact="valor")

# Contains (case sensitive)
Model.objects.filter(camp__contains="valor")

# Contains (case insensitive)
Model.objects.filter(camp__icontains="valor")
```

### Comparadors Numèrics
```python
# Major que
Model.objects.filter(camp__gt=5)

# Major o igual que
Model.objects.filter(camp__gte=5)

# Menor que
Model.objects.filter(camp__lt=5)

# Menor o igual que
Model.objects.filter(camp__lte=5)

# Entre valors (inclusiu)
Model.objects.filter(camp__range=(1, 5))
```

### Dates
```python
# Any específic
Model.objects.filter(data__year=2024)

# Mes específic
Model.objects.filter(data__month=3)

# Dia específic
Model.objects.filter(data__day=15)

# Data major que
Model.objects.filter(data__gt=datetime.now())

# Rang de dates
Model.objects.filter(data__range=(data_inici, data_fi))

# Week day (1=Diumenge, 7=Dissabte)
Model.objects.filter(data__week_day=2)  # Dilluns
```

### Llistes i Nuls
```python
# IN (està en llista)
Model.objects.filter(camp__in=['valor1', 'valor2'])

# IS NULL
Model.objects.filter(camp__isnull=True)

# IS NOT NULL
Model.objects.filter(camp__isnull=False)
```

### Text
```python
# Comença per
Model.objects.filter(camp__startswith="prefix")
Model.objects.filter(camp__istartswith="prefix")  # case insensitive

# Acaba en
Model.objects.filter(camp__endswith="sufix")
Model.objects.filter(camp__iendswith="sufix")  # case insensitive

# Regex
Model.objects.filter(camp__regex=r'^[A-Z]')
Model.objects.filter(camp__iregex=r'^[A-Z]')  # case insensitive
```

## 3. Queries Complexes

### Múltiples Condicions
```python
# AND (totes les condicions)
Model.objects.filter(
    camp1="valor1",
    camp2="valor2"
)

# OR (alguna condició)
from django.db.models import Q
Model.objects.filter(
    Q(camp1="valor1") | Q(camp2="valor2")
)

# AND i OR combinats
Model.objects.filter(
    Q(camp1="valor1") | Q(camp2="valor2"),
    camp3="valor3"  # AND amb la resta
)

# NOT
Model.objects.filter(~Q(camp="valor"))
```

### Relacions
```python
# Forward (ForeignKey)
Model.objects.filter(relacio__camp="valor")

# Reverse
Model.objects.filter(relacio_set__camp="valor")

# ManyToMany
Model.objects.filter(relacio__in=[obj1, obj2])

# Filtrar per existència de relació
Model.objects.filter(relacio__isnull=False)
```

## 4. Optimització de Queries

### select_related() (per ForeignKey)
```python
# Carrega les relacions ForeignKey en una sola query
Model.objects.select_related('relacio').filter(...)
```

### prefetch_related() (per ManyToMany)
```python
# Carrega les relacions ManyToMany eficientment
Model.objects.prefetch_related('relacio').filter(...)
```

### values() i values_list()
```python
# Només certs camps
Model.objects.values('camp1', 'camp2')

# Llista de valors
Model.objects.values_list('camp', flat=True)
```

### distinct()
```python
# Elimina duplicats
Model.objects.filter(...).distinct()
```

## 5. Exemples Pràctics

```python
# Shipeos actius creats aquest mes
Shipeos.objects.filter(
    isActive=True,
    fechaInicio__month=datetime.now().month
)

# Shipeos amb certs amants
Shipeos.objects.filter(
    amantes__in=[amant1, amant2]
)

# Shipeos amb chismes que continguin cert text
Shipeos.objects.filter(
    chismes__texto_chisme__icontains="text"
)

# Shipeos actius o amb més de 3 chismes
from django.db.models import Count
Shipeos.objects.filter(
    Q(isActive=True) |
    Q(chismes__count__gt=3)
).annotate(
    Count('chismes')
)
```
