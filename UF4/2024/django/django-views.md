# Views a Django

## 1. Views Bàsiques (View)

Les views bàsiques hereten de `django.views.View` i ens permeten definir mètodes per cada tipus de petició HTTP.

```python
from django.views import View
from django.http import HttpResponse

class BestShipeo(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse("Mael y Jordi")
```

### Mètodes HTTP
```python
class ExempleView(View):
    # GET per obtenir dades
    def get(self, request, *args, **kwargs):
        # Lògica pel GET
        return HttpResponse()
    
    # POST per crear dades
    def post(self, request, *args, **kwargs):
        # Lògica pel POST
        return HttpResponse()
        
    # PUT per actualitzar dades
    def put(self, request, *args, **kwargs):
        # Lògica pel PUT
        return HttpResponse()
        
    # DELETE per eliminar dades
    def delete(self, request, *args, **kwargs):
        # Lògica pel DELETE
        return HttpResponse()
```

### Respostes a Views

#### Render (per mostrar templates)
```python
class ShipeoDetail(View):
    def get(self, request, *args, **kwargs):
        shipeo = {...}  # Obtenim dades
        
        # Context: diccionari amb dades pels templates
        context = {
            "detalle": shipeo,
            "altres_dades": "valor"
        }
        
        # render(request, template, context)
        return render(
            request,
            "feliciano/detalle.html", 
            context
        )
```

#### Redirect (per redirigir a altres views)
```python
class ToggleActive(View):
    def get(self, request, *args, **kwargs):
        # Fer alguna cosa...
        
        # Redirect a una URL definida a urls.py amb name="shipeos"
        return redirect("shipeos")
        
        # També pots redirigir a una URL directa
        return redirect("/shipeos/")
```

### Variables del Path
Les variables definides al urls.py es reben als kwargs:

```python
# Al urls.py:
# path('shipeo/<str:nombre>/', ShipeoDetail.as_view(), name='detalle')

class ShipeoDetail(View):
    def get(self, request, *args, **kwargs):
        # Accedim a 'nombre' dels kwargs
        nom_shipeo = self.kwargs['nombre']
```

### Guardar Models
```python
class ToggleActive(View):
    def get(self, request, *args, **kwargs):
        shipeo = {...}  # Obtenim el model
        
        # Modifiquem atributs
        shipeo.isActive = not shipeo.isActive
        
        # Guardem a la BD
        shipeo.save()
```

## 2. ListView

ListView és una vista genèrica per mostrar llistes d'objectes.

```python
from django.views.generic import ListView

class AllShipeos(ListView):
    # Model a utilitzar
    model = Shipeos
    
    # Template a renderitzar
    template_name = 'feliciano/all_shipeos.html'
    
    # Nom de la variable al context (opcional)
    context_object_name = 'shipeos'
    
    # Paginació (opcional)
    paginate_by = 10
```

### Personalitzar el QuerySet
```python
class ElShipeoDelDia(ListView):
    model = Shipeos
    template_name = 'feliciano/shipeo_del_dia.html'

    def get_queryset(self):
        # Sobreescrivim per personalitzar les dades
        return super().get_queryset().filter(
            fechaInicio__day=datetime.now().day
        )
```

### Context Addicional a ListView
```python
class AllShipeos(ListView):
    model = Shipeos
    template_name = 'feliciano/all_shipeos.html'

    def get_context_data(self, **kwargs):
        # Obtenim el context base
        context = super().get_context_data(**kwargs)
        
        # Afegim dades addicionals
        context['titol'] = 'Tots els Shipeos'
        context['total'] = self.get_queryset().count()
        
        return context
```

## 3. Gestió d'Errors

### Error 404
```python
from django.http import Http404

class ShipeoDetail(View):
    def get(self, request, *args, **kwargs):
        shipeo = {...}  # Obtenim shipeo
        
        if not shipeo:
            # Si no trobem el shipeo, llancem 404
            raise Http404("Shipeo not Found")
```

### Altres Errors HTTP
```python
from django.http import HttpResponseForbidden

class ShipeoDetail(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            # Error 403: Forbidden
            return HttpResponseForbidden()
            
        # També hi ha:
        # HttpResponseBadRequest (400)
        # HttpResponseNotAllowed (405)
        # HttpResponseServerError (500)
```

## 4. Flow ListView -> Template

### Accés a les Dades

1. ListView -> Template (amb `object_list`)
```python
class AllShipeos(ListView):
    model = Shipeos
    template_name = 'feliciano/all_shipeos.html'
    # object_list estarà disponible automàticament
```

```html
<!-- all_shipeos.html -->
{% for item in object_list %}
    <!-- Accés directe a camps del model -->
    {{ item.nombreShip }}
    {{ item.fechaInicio }}
    {{ item.isActive }}
    
    <!-- Related Managers (ManyToMany) -->
    {% for amante in item.amantes.all %}
        {{ amante }}
    {% endfor %}
    
    <!-- Related Managers (ForeignKey reverse) -->
    {% for chisme in item.chismes.all %}
        {{ chisme.texto_chisme }}
    {% endfor %}
{% endfor %}
```

2. View -> Template (amb context personalitzat)
```python
class ShipeoDetail(View):
    def get(self, request, *args, **kwargs):
        shipeo = {...}  # Obtenim shipeo
        return render(request, "feliciano/detalle.html", {
            "detalle": shipeo
        })
```
