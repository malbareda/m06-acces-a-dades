"""
URL configuration for djangoProject1 project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth import views as auth_views

from feliciano import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.AllShipeos.as_view(), name='index'),  # /shipeos te va a llevar a la view AllShipeos
    path('shipeos', views.AllShipeos.as_view(), name='shipeos'), #/shipeos te va a llevar a la view AllShipeos
    path('shipeoDelDia', views.ElShipeoDelDia.as_view(), name='shipeoDelDia'),
    path('best', views.BestShipeo.as_view(), name='best'),
    path('toggle/<nombre>', views.toggleActiveShipeos.as_view()),
    path('shipeo/<nombre>', views.ShipeoDetail.as_view(), name='detail'),
    path('addAmante', views.AddAmante.as_view(), name='addAmante'),
    path('amanteAnadido', views.AddAmante2.as_view(), name='amanteAnadido'),
    path('vote', views.Vote.as_view(), name='vote'),
    ##PATHS DE LOGIN Y LOGOUT AUTOMATICOS
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),

]
