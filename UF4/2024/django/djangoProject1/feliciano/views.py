# Importem el mòdul datetime per treballar amb dates i temps
from datetime import datetime

from django.contrib.auth.models import User
# Importem Http404 per gestionar errors quan no es troba una pàgina
from django.http import Http404, HttpResponse
# Importem render per renderitzar templates
from django.shortcuts import render, redirect
from django.views import View
# Importem ListView, una vista genèrica de Django per mostrar llistes d'objectes
from django.views.generic import ListView

# Importem el model Shipeos de l'aplicació feliciano
from feliciano.models import Shipeos, Amante


# Definim les nostres vistes aquí

# Vista per mostrar tots els shipeos utilitzant ListView

# EXPLICACIÓ DEL FLUX QUERYSET -> TEMPLATE:
# 1. ListView automàticament passa el queryset a la template amb el nom 'object_list'
# 2. També es pot accedir com {model_name}_list, en aquest cas 'shipeos_list'
# 3. Si volem canviar el nom de la variable a la template, podem usar 'context_object_name'
# 4. El queryset es pot paginar automàticament amb 'paginate_by'


class AllShipeos(ListView):
    # Especifiquem quin model utilitzarem
    model = Shipeos
    # Definim el queryset que volem mostrar (tots els shipeos en aquest cas)
    queryset = Shipeos.objects.all()

    # Indiquem la plantilla HTML que s'utilitzarà per mostrar les dades
    template_name = 'feliciano/all_shipeos.html'

class Vote(View):

    def get(self, request, *args, **kwargs):
        return render(request, "feliciano/vote.html", {"shipeos": Shipeos.objects.all()})

    def post(self, request, *args, **kwargs):
        cosa = Shipeos.objects.filter(nombreShip=request.POST['nombre'])
        for n in cosa:
            votos = n.votes + 1
            n.votes=votos
            n.save()
        print(cosa[0].nombreShip,cosa[0].votes,votos,cosa[0].id)
        return render(request, "feliciano/vote.html", {"shipeos": Shipeos.objects.all()})

class BestShipeo(View):
    def get(self, request, *args, **kwargs):
        ##Podem agafar totes les dades de l'usuari loguejat mitjançant request.user
        if request.user.username == "marc":
            return HttpResponse("Mael y Jordi")
        else:
            return HttpResponse("Best Shipeo")



class ShipeoDetail(View):
    # Especifiquem quin model utilitzarem
    model = Shipeos
    def get(self, request, *args, **kwargs):
        cosa = Shipeos.objects.filter(nombreShip=self.kwargs['nombre'])
        # Si existeix algun shipeo amb aquest nom
        if cosa.exists():
            # Agafem el primer shipeo amb aquest nom
            i = cosa[0]
            # un cop acabat, en una View (no list view) podem tornar un render o un redirect
            # tornar un render et deixa carregar una template
            # El context es un diccionari amb totes les dades que li vols passar a la template
            # la template podra accedir amb {{ clau }}
            return render(request,"feliciano/detalle.html", {"detalle":i, "grup":request.user.groups.all()})

        # Si no trobem cap shipeo amb aquest nom
        else:
            # Llancem un error 404
            raise Http404("Shipeo not Found")


class AddAmante(View):
    def get(self, request, *args, **kwargs):
        ##request.user et torna les dades del usuari loguejat
        ##user.is_authenticated torna si està loguejat
        if not request.user.is_authenticated:
            # si no estas loguejat et prota al login
            return redirect("login")
        num = Amante.objects.count()
        return render(request,"feliciano/formulario.html",{"chen":"chenchen","num":num})

class AddAmante2(View):
    def post(self, request, *args, **kwargs):
        nombre = request.POST['nombre']
        edad = request.POST['edad']

        nuevoAmante = Amante(name=nombre,edad=edad)
        nuevoAmante.save()
        #return(HttpResponse("<h2>Amante Added</h2>"))
        return redirect('shipeos')



# Vista per mostrar els shipeos del dia actual
class ElShipeoDelDia(ListView):
    # Especifiquem el model
    model = Shipeos
    # Indiquem la plantilla per aquesta vista
    template_name = 'feliciano/shipeo_del_dia.html'

    # Sobreescrivim el mètode get_queryset per filtrar només els shipeos d'avui
    def get_queryset(self):
        # Filtrem els shipeos que tenen la data d'inici igual al dia actual
        return Shipeos.objects.filter(fechaInicio__day=datetime.now().day)

# Vista per canviar l'estat actiu/inactiu dels shipeos
class toggleActiveShipeos(View):

    def get(self, request, *args, **kwargs):
        # Definim la plantilla que s'utilitzarà
        template_name = 'feliciano/all_shipeos.html'
        # Busquem els shipeos amb el nom especificat en la URL
        cosa = Shipeos.objects.filter(nombreShip=self.kwargs['nombre'])
        # Si existeix algun shipeo amb aquest nom
        if cosa.exists():
            # Per cada shipeo trobat
            for i in cosa:
                # Canviem l'estat actiu/inactiu (toggle)
                i.isActive = not i.isActive
                # Guardem els canvis a la base de dades
                i.save()

            # Retornem els shipeos modificats
        # Si no trobem cap shipeo amb aquest nom
        else:
            # Llancem un error 404
            raise Http404("Shipeo not Found")

        # un cop acabat, en una View (no list view) podem tornar un render o un redirect
        # tornar un redirect carrega la view que tingui aquest nom a urls.py (parametre name)
        return redirect("shipeos")




