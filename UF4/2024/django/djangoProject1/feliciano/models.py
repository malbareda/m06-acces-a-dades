from django.utils import timezone
from django.db import models

# Create your models here.

class Amante(models.Model):
    name = models.CharField(max_length=100)
    edad = models.IntegerField()

    # toString
    def __str__(self):
        return self.name

class Shipeos(models.Model):
    # Definim el primer nom del shipeo
    # CharField és per text curt, max_length limita la longitud a 50 caràcters
    amantes = models.ManyToManyField(Amante, blank=False, null=False, related_name='shipeos' )

    # Nom combinat del shipeo (podria ser algo com "nombre1+nombre2")
    nombreShip = models.CharField('nombreShip', max_length=50)

    # Data i hora d'inici del shipeo
    # DateTimeField guarda tant la data com l'hora
    # default=timezone.now fa que s'assigni automàticament la data/hora actual quan es crea
    fechaInicio = models.DateTimeField('fechaInicio', default=timezone.now)

    # Camp booleà per marcar si el shipeo està actiu o no
    # Per defecte es crea com False (inactiu)
    isActive = models.BooleanField('active', default=False)

    # Nota: Falta importar timezone dalt:
    # from django.utils import timezone

    # També podríem afegir el mètode __str__ per una millor representació:
    # def __str__(self):
    #     return f"{self.nombreShip} ({self.fechaInicio})"

    # Si volem ordenació per defecte podríem afegir:
    # class Meta:
    #     ordering = ['-fechaInicio']
    votes = models.IntegerField(default=0)

class Chisme(models.Model):
    #se especifica desde el chisme
    #defines la relacion desde los dos lados con el related_name
    shipeo = models.ForeignKey(Shipeos, on_delete=models.CASCADE, related_name='chismes')
    texto_chisme =models.CharField('texto_chisme', max_length=500)
