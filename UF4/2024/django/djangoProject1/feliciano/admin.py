from django.contrib import admin

from feliciano.models import Shipeos, Chisme, Amante

# Register your models here.

admin.site.register(Shipeos)
admin.site.register(Chisme)
admin.site.register(Amante)
