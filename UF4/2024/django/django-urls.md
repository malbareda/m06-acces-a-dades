# URLs a Django

## 1. Estructura Bàsica

```python
from django.urls import path
from myapp import views

urlpatterns = [
    # path(ruta, view, name)
    path('ruta/', views.MyView.as_view(), name='nom_url'),
]
```

## 2. Tipus de Paths

### Path Simple
```python
# Ruta exacta
path('shipeos', views.AllShipeos.as_view(), name='shipeos')

# Ruta arrel
path('', views.AllShipeos.as_view(), name='index')
```

### Paths amb Paràmetres
```python
# Paràmetre string
path('shipeo/<str:nombre>', views.ShipeoDetail.as_view(), name='detail')

# Paràmetre enter
path('shipeo/<int:id>', views.ShipeoDetail.as_view())

# Paràmetre slug
path('shipeo/<slug:slug>', views.ShipeoDetail.as_view())

# Paràmetre path (inclou /)
path('categoria/<path:subpath>', views.Category.as_view())
```

### Path Redirecció Admin
```python
from django.contrib import admin

path('admin/', admin.site.urls)
```

## 3. Tipus de Paràmetres URL

### Path Parameters (a la URL)
```python
# URL: /shipeo/maeljordi
path('shipeo/<nombre>', views.ShipeoDetail.as_view())

# A la view:
def get(self, request, *args, **kwargs):
    nombre = self.kwargs['nombre']
```

### Query Parameters (?param=valor)
```python
# URL: /shipeos?tipo=canon&active=true
path('shipeos', views.AllShipeos.as_view())

# A la view:
def get(self, request, *args, **kwargs):
    tipo = request.GET.get('tipo')
    active = request.GET.get('active')
```

## 4. Named URLs

### Definir Named URLs
```python
path('shipeo/<nombre>', 
     views.ShipeoDetail.as_view(), 
     name='detail')
```

### Usar Named URLs en Templates
```html
<!-- URL simple -->
<a href="{% url 'shipeos' %}">Tots els shipeos</a>

<!-- URL amb paràmetres -->
<a href="{% url 'detail' nombre=shipeo.nombreShip %}">
    Veure detall
</a>

<!-- URL amb query params -->
<a href="{% url 'shipeos' %}?tipo=canon">
    Shipeos Canon
</a>
```

### Usar Named URLs en Views
```python
from django.shortcuts import redirect

def get(self, request, *args, **kwargs):
    # Redirigir a URL named
    return redirect('shipeos')
    
    # Redirigir amb paràmetres
    return redirect('detail', nombre='maeljordi')
```

## 5. Include i Namespace

### Include URLs d'Altres Apps
```python
from django.urls import include, path

urlpatterns = [
    path('app/', include('app.urls')),
]
```

### Namespace per Apps
```python
# app/urls.py
app_name = 'myapp'

urlpatterns = [
    path('detail/', views.Detail.as_view(), name='detail'),
]

# Template
{% url 'myapp:detail' %}
```

## 6. Expressions Regulars en URLs

```python
from django.urls import re_path

urlpatterns = [
    # Qualsevol digit
    re_path(r'^shipeo/(?P<id>\d+)/$', views.ShipeoDetail.as_view()),
    
    # Pattern específic
    re_path(r'^shipeo/(?P<nombre>[a-zA-Z]+)/$', 
            views.ShipeoDetail.as_view()),
]
```

## 7. Exemples Pràctics

```python
urlpatterns = [
    # Ruta base (/) i /shipeos
    path('', views.AllShipeos.as_view(), name='index'),
    path('shipeos', views.AllShipeos.as_view(), name='shipeos'),
    
    # Ruta amb paràmetre
    path('shipeo/<nombre>', 
         views.ShipeoDetail.as_view(), 
         name='detail'),
    
    # Ruta amb acció i paràmetre
    path('toggle/<nombre>', 
         views.toggleActiveShipeos.as_view()),
    
    # Ruta simple
    path('shipeoDelDia', 
         views.ElShipeoDelDia.as_view(), 
         name='shipeoDelDia'),
]
```

### Templates Corresponents
```html
<!-- Link a la llista -->
<a href="{% url 'shipeos' %}">Veure tots</a>

<!-- Link amb paràmetre -->
<a href="{% url 'detail' nombre=shipeo.nombreShip %}">
    Veure {{ shipeo.nombreShip }}
</a>

<!-- Link amb query params -->
<a href="{% url 'shipeos' %}?active=true">
    Veure actius
</a>
```

### Views Corresponents
```python
class ShipeoDetail(View):
    def get(self, request, *args, **kwargs):
        # Path parameter
        nombre = self.kwargs['nombre']
        # Query parameter
        tipo = request.GET.get('tipo', 'todos')
```
