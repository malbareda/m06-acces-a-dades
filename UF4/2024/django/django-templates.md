# Templates i Related Managers a Django

## 1. Sintaxi Bàsica de Templates

### Variables
```html
{{ variable }}
{{ variable.atribut }}
{{ variable.metode }}
```

### Loops
```html
{% for item in items %}
    {{ item }}
{% empty %}
    No hi ha items
{% endfor %}
```

### Condicions
```html
{% if condicio %}
    Cert!
{% elif altra_condicio %}
    Altra condició
{% else %}
    Fals!
{% endif %}
```

## 2. Related Managers

### Important sobre Related Managers

1. Per accedir a relacions ManyToMany o ForeignKey reverse:
   - `model.relationname` retorna un Related Manager
   - Cal fer `model.relationname.all` per obtenir la llista d'objectes

```html
<!-- ❌ INCORRECTE: item.chismes és un manager -->
{% for chisme in item.chismes %}

<!-- ✅ CORRECTE: item.chismes.all retorna la llista -->
{% for chisme in item.chismes.all %}
```
2. Altres mètodes útils del Related Manager:
```html
<!-- Comptar relacions -->
{{ item.chismes.count }}

<!-- Comprovar si té relacions -->
{% if item.chismes.exists %}

<!-- Filtrar relacions -->
{% for chisme in item.chismes.filter.active %}
```


### ManyToMany
```html
<!-- Manager -->
{{ item.amantes }}

<!-- Llista d'objectes -->
{% for amante in item.amantes.all %}
    {{ amante.name }}
{% endfor %}
```

### ForeignKey Reverse
```html
<!-- Manager -->
{{ item.chismes }}

<!-- Llista d'objectes -->
{% for chisme in item.chismes.all %}
    {{ chisme.texto_chisme }}
{% endfor %}
```

### Mètodes de Manager
```html
<!-- Comptar -->
{{ item.chismes.count }}

<!-- Existència -->
{% if item.chismes.exists %}

<!-- Filtrar -->
{% for chisme in item.chismes.filter.actiu %}
```

## 3. Paginació amb ListView

### View
```python
class AllShipeos(ListView):
    model = Shipeos
    paginate_by = 10
```

### Template
```html
{% if is_paginated %}
    <!-- Previous -->
    {% if page_obj.has_previous %}
        <a href="?page={{ page_obj.previous_page_number }}">
            Anterior
        </a>
    {% endif %}
    
    <!-- Current -->
    Pàgina {{ page_obj.number }} 
    de {{ page_obj.paginator.num_pages }}
    
    <!-- Next -->
    {% if page_obj.has_next %}
        <a href="?page={{ page_obj.next_page_number }}">
            Següent
        </a>
    {% endif %}
{% endif %}
```

## 4. Cridar Funcions

### Des del Template
```html
<!-- Mètodes del Model -->
{{ objecte.metode_model }}

<!-- Propietats -->
{{ objecte.propietat }}

<!-- Funcions amb Paràmetres -->
{{ objecte.funcio_amb_params|default:"valor" }}
```

### Filters
```html
<!-- Dates -->
{{ objecte.data|date:"d/m/Y" }}

<!-- Text -->
{{ objecte.text|upper }}
{{ objecte.text|truncatewords:30 }}

<!-- Números -->
{{ objecte.numero|floatformat:2 }}
```
