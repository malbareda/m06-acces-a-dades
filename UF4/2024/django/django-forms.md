# Formularis a Django

## 1. Estructura Bàsica HTML

### Form HTML
```html
<form action="{% url 'nom_url' %}" method="post">
    {% csrf_token %}
    
    <fieldset>
        <legend>Títol del Formulari</legend>
        
        <!-- Text input -->
        <label>Nom:</label>
        <input type="text" name="nombre" id="nombreAmante">
        
        <!-- Number input -->
        <label>Edat:</label>
        <input type="number" name="edad" id="edadAmante">
    </fieldset>
    
    <input type="submit" value="Enviar">
</form>
```

### Elements Importants
- `action`: URL on s'enviarà el formulari
- `method`: "post" o "get"
- `{% csrf_token %}`: Token de seguretat (obligatori en POST)
- `name`: Identificador per accedir al valor

## 2. Views per Formularis

### GET (Mostrar Formulari)
```python
class AddAmante(View):
    def get(self, request, *args, **kwargs):
        # Preparem context pel formulari
        context = {
            "chen": "chenchen",
            "num": Amante.objects.count()
        }
        
        # Mostrem el formulari
        return render(
            request,
            "feliciano/formulario.html",
            context
        )
```

### POST (Processar Dades)
```python
class AddAmante2(View):
    def post(self, request, *args, **kwargs):
        # Obtenim dades del POST
        nombre = request.POST['nombre']
        edad = request.POST['edad']
        
        # Creem i guardem objecte
        nuevoAmante = Amante(
            name=nombre,
            edad=edad
        )
        nuevoAmante.save()
        
        # Redirigim després de l'acció
        return redirect('shipeos')
```

## 3. Django Forms

### Form Class
```python
from django import forms

class AmanteForm(forms.Form):
    nombre = forms.CharField(
        label='Nom de l\'Amante',
        max_length=100
    )
    edad = forms.IntegerField(
        label='Edat',
        min_value=0
    )
```

### View amb Form
```python
from .forms import AmanteForm

class AddAmante(View):
    def get(self, request):
        # Creem formulari buit
        form = AmanteForm()
        return render(request, 
                     'formulario.html',
                     {'form': form})
    
    def post(self, request):
        # Creem formulari amb dades
        form = AmanteForm(request.POST)
        
        if form.is_valid():
            # Dades validades
            nombre = form.cleaned_data['nombre']
            edad = form.cleaned_data['edad']
            
            # Processem dades...
            return redirect('success')
        
        # Si no és vàlid, tornem al formulari
        return render(request, 
                     'formulario.html',
                     {'form': form})
```

### Template amb Form
```html
<form method="post">
    {% csrf_token %}
    {{ form.as_p }}
    <input type="submit" value="Enviar">
</form>
```

## 4. ModelForms

### ModelForm Class
```python
from django.forms import ModelForm

class AmanteForm(ModelForm):
    class Meta:
        model = Amante
        fields = ['name', 'edad']
        # O tots els camps:
        # fields = '__all__'
```

### View amb ModelForm
```python
class AddAmante(View):
    def get(self, request):
        form = AmanteForm()
        return render(request, 
                     'formulario.html',
                     {'form': form})
    
    def post(self, request):
        form = AmanteForm(request.POST)
        if form.is_valid():
            # Guarda directament al model
            amante = form.save()
            return redirect('success')
        
        return render(request, 
                     'formulario.html',
                     {'form': form})
```

## 5. Validació

### Al Form
```python
class AmanteForm(forms.Form):
    nombre = forms.CharField(
        validators=[MinLengthValidator(2)]
    )
    
    def clean_nombre(self):
        nombre = self.cleaned_data['nombre']
        if nombre.lower() == 'admin':
            raise ValidationError(
                'No es pot usar aquest nom'
            )
        return nombre
    
    def clean(self):
        cleaned_data = super().clean()
        # Validació entre camps
        return cleaned_data
```

### A la View
```python
def post(self, request):
    nombre = request.POST['nombre']
    edad = request.POST['edad']
    
    # Validació manual
    if len(nombre) < 2:
        return render(request, 
                     'formulario.html',
                     {'error': 'Nom massa curt'})
    
    # Continuar si és vàlid...
```

## 6. Arxius i Imatges

### Form HTML
```html
<form method="post" enctype="multipart/form-data">
    {% csrf_token %}
    <input type="file" name="imagen">
    <input type="submit">
</form>
```

### View
```python
def post(self, request):
    # Arxius a request.FILES
    imagen = request.FILES['imagen']
    
    # Guardar amb Model
    amante = Amante(
        name=request.POST['nombre'],
        imagen=imagen
    )
    amante.save()
```

## 7. Filtres i Cerca

### Form
```html
<form method="get">
    <input type="text" name="q">
    <input type="submit" value="Cercar">
</form>
```

### View
```python
def get(self, request):
    query = request.GET.get('q', '')
    if query:
        # Filtre per cerca
        resultats = Model.objects.filter(
            nombre__icontains=query
        )
    else:
        resultats = Model.objects.all()
        
    return render(request, 
                 'template.html',
                 {'resultats': resultats})
```
