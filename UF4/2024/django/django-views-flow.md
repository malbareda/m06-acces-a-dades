# Flow Bàsic de Django Views

## 1. View Bàsica (View)

```python
from django.views import View
from django.shortcuts import render, redirect

class ShipeoDetail(View):
    def get(self, request, *args, **kwargs):
        # 1. Obtenim dades dels kwargs (URL)
        nombre = self.kwargs['nombre']
        
        # 2. Obtenim dades del model
        shipeo = Shipeos.objects.filter(nombreShip=nombre)[0]
        
        # 3. Creem el context pel template
        context = {
            "detalle": shipeo
        }
        
        # 4. Renderitzem template amb context
        return render(request, "feliciano/detalle.html", context)
```

Al template:
```html
<!-- feliciano/detalle.html -->
<h2>{{ detalle.nombreShip }}</h2>
```

## 2. ListView

```python
from django.views.generic import ListView

class AllShipeos(ListView):
    # 1. Definim el model
    model = Shipeos
    
    # 2. Especifiquem el template
    template_name = 'feliciano/all_shipeos.html'
```

Al template:
```html
<!-- feliciano/all_shipeos.html -->
{% for item in object_list %}
    {{ item.nombreShip }}
{% endfor %}
```

## 3. Tipus de Resposta

### Render (mostrar template)
```python
# Render passa dades a un template
return render(
    request,               # Request sempre necessari
    "ruta/template.html",  # Ruta al template
    {"clau": valor}       # Context (opcional)
)
```

### Redirect (redirigir a altra view)
```python
# Redirect a una URL name
return redirect("shipeos")

# Redirect a un path
return redirect("/shipeos/")
```

### HttpResponse (resposta directa)
```python
# Resposta simple
return HttpResponse("Hola!")
```

## 4. Rebre Dades

### De la URL (kwargs)
```python
# urls.py
path('shipeo/<str:nombre>/', ShipeoDetail.as_view())

# views.py
def get(self, request, *args, **kwargs):
    nombre = self.kwargs['nombre']
```

### Del QueryString
```python
def get(self, request, *args, **kwargs):
    # URL: /shipeos/?tipus=canon
    tipus = request.GET.get('tipus')
```

### Del POST
```python
def post(self, request, *args, **kwargs):
    # Dades del formulari
    nom = request.POST.get('nom')
```

## 5. Desar Model

```python
def post(self, request, *args, **kwargs):
    # 1. Crear/obtenir instància
    shipeo = Shipeos(nombreShip="Nou Ship")
    
    # 2. Modificar atributs
    shipeo.isActive = True
    
    # 3. Desar a la BD
    shipeo.save()
```
