<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Practica extends Model
{
    //relacion con la tabla user
    public function user()
    {
        //pertenece a user. Es la N en una relacion 1-N
    	return $this->belongsTo(User::class);
    }
}
