<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\TestController@welcome');

Route::get('/covid/create','App\Http\Controllers\CovidController@add');
Route::post('/covid/create','App\Http\Controllers\CovidController@create');
Route::get('/covid/createWithUser/{user}','App\Http\Controllers\CovidController@addWithUser');
Route::post('/covid/createWithUser/{user}','App\Http\Controllers\CovidController@createWithUser');
Route::get('/covid/getByUser/{user}','App\Http\Controllers\CovidController@getByUser');
Route::get('/covid/diesBaixa/{user}','App\Http\Controllers\CovidController@diesBaixa');
Route::get('/covid/diesBaixa/{user}/{variant}','App\Http\Controllers\CovidController@diesBaixaVariant');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
