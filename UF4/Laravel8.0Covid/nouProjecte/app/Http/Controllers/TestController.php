<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Task;
//importa tota la DB
use DB;
//importa el model User, equivalent a la taula users
use App\User;

class TestController extends Controller
{

    function welcome()
    {
        $persona = "Kane";
        return view('welcome',compact('persona'));
    }
}
