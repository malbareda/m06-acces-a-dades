<?php

namespace App\Http\Controllers;

use App\Models\CasCovid;
use App\Models\Contagi;
use App\Models\Covid;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;
use Log;

class CovidController extends Controller
{
    //
    public function addWithUser(User $user){
        return view('addWithUser',compact('user'));
    }

    public function createWithUser(User $user, Request $request)
    {
        error_log("hola");
        //si estas loguejat
        if (Auth::check()) {
            error_log("rehola");

            ///creas nou covid
            $covid = new Contagi();
            //poses variant
            $covid->variant = $request->variant;
            //pones en la descripcion el campo description del formulario
            $covid->descripcio = $request->descripcio;
            $covid->dies_baixa = 0;
            //pones en la llave foranea el id
            $covid->user_id = $user->id;
            //guardas en BD
            $covid->descripcio = $request->color;

            if($request->check){
                $covid->dies_baixa = 1;
            }
            $covid->save();
            //redirecciona
            return redirect('/');
        }
    }

    public function add(){
        return view('add');
    }

    public function create(Request $request)
    {

        //si estas loguejat
        if (Auth::check()) {
            ///creas nou covid
            $covid = new Contagi();
            //poses variant
            $covid->variant = $request->check;
            //pones en la descripcion el campo description del formulario
            $covid->descripcio = $request->descripcio;
            $covid->dies_baixa = 0;
            //pones en la llave foranea el id
            $covid->user_id = Auth::id();
            //guardas en BD
            $covid->descripcio = $request->color;

            if($request->check){
                $covid->dies_baixa = 1;
            }
            $covid->save();
            //redirecciona
            return redirect('/');
        }
    }

    public function getByUser(User $user){
        return view('getByUser',compact('user'));
    }

    public function diesBaixa(User $user){
        error_log("aaaaaaaaaaaa");
        error_log($user->name);
        $nom = $user->name;
        //contagis
        $hola = $user->contagis;

        $dies_totals = 0;
        foreach($hola as $valor){
            if($valor->variant=="Omicron") {
                $dies_totals += $valor->dies_baixa;
            }
        }
        error_log($dies_totals);

        return view('diesBaixa',compact('user','dies_totals'));
    }


    public function diesBaixaVariant(User $user, string $variant){
        error_log("aaaaaaaaaaaa");
        error_log($user->name);
        $nom = $user->name;
        //contagis
        $hola = $user->contagis;

        $dies_totals = 0;
        $coses = Contagi::where('variant','=',$variant)->where('user_id','=',$user->id)->orderBy('variant','desc')->get();

        dies
        foreach($coses as $valor){
            error_log($valor->variant." de ".$valor->user->name);
            $valor->dies_baixa++;
            $valor->save();
        }
/*
        foreach($hola as $valor){
            if($valor->variant==$variant) {
                $dies_totals += $valor->dies_baixa;
            }
        }
        error_log($dies_totals);
*/
        return view('diesBaixa',compact('user','dies_totals'));
    }


}
