<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contagi extends Model
{
    public function user(){
        //som la part N d'una relacio
        return $this->belongsTo(User::class);
    }
}
