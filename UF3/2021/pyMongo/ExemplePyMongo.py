import pymongo
from pymongo import MongoClient
from pprint import pprint

client = MongoClient()
pprint("las bases de datos son ")
pprint(client.database_names())

#client va a ser un diccionario con las bases de datos
db = client["people"]
pprint("la db es ")
pprint(db)
#cada db va a ser tambien un diccionario con las colecciones
coll = db["friends"]
pprint("la coleccion es ")
pprint(coll)


#coll tiene un monton de metodos
doc = coll.find_one()
pprint("el primer documento es ")
pprint(doc)
##Acceso a doc por diccionario
##nombre
pprint(doc["name"])
##nombre de su primer amigo (campo friends, primero de la lista, campo nombre)
pprint(doc["friends"][0]["name"])

print("todos los que tienen 27 años")

##find devuelve un iterable (se recorre con un for), de la busqueda
doc27 = coll.find({"age":27})
for i in doc27:
    pprint(i["name"])
##find acepta tres campos, el primero es el filtro (WHERE), el segundo la proyeccion (SELECT)
docnombres = coll.find({"age":27},{"_id":0,"name":1})
for i in docnombres:
    pprint(i)

print("ejemplo de sort")
docnombres = coll.find({"age":27},{"_id":0,"name":1}).sort("name",pymongo.ASCENDING)
for i in docnombres:
    pprint(i)


print("ejemplo de insert")
nuño = {"name": "nuño", "age":28, "email": "nuño@quete.calles", "friends":False}
coll.insert(nuño)

print("ejemplo de update")   
##el update y el delete pueden ser update_one o update_many 
coll.update_many({"age":28},{ "$set": { "age": "PUTO NUÑO VIEJO"} })

#un find sin parametros te devuelve todo
docviejo = coll.find()
for i in docviejo:
    pprint(i["name"]+" "+str(i["age"]))



print("ejemplo de delete")
coll.delete_many({"name":"nuño"})    

#3.6   3.7  (pero yo que se)