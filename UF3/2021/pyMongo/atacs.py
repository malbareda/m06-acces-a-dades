from mongoengine import *
from pprint import pprint
#establecer restricciones

##nos conectamos a la base de datos example
db = connect('projecte')
##borra la bd
#db.drop_database('example')


TYPES = ('Bug','Dark','Dragon','Electric','Fairy','Fight','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')

class Move(Document):
    meta = {'allow_inheritance': True}
    name = StringField(required=True)
    pwr = DecimalField(required=True, min_value=0, max_value=180)
    tipus = StringField(choices=TYPES, required=True)

class FastMove(Move):
    energyGain = IntField(required=True, min_value=0, max_value=20)

class ChargedMove(Move):
    energyCost = IntField(required=True, min_value=33, max_value=100)


steelwing = FastMove(name='Steel Wing', pwr = 11, tipus = 'Steel' )
steelwing.energyGain = 7
steelwing.save()

dragontail = FastMove(name='Dragon Tail', pwr = 15, tipus = 'Dragon' )
dragontail.energyGain = 8
dragontail.save()

doomdesire = ChargedMove(name = 'Doom Desire', pwr = 80, tipus = 'Steel')
doomdesire.energyCost = 50
doomdesire.save()

futuresight = ChargedMove(name = 'Future Sight', pwr = 120, tipus = 'Psychic')
futuresight.energyCost = 100
futuresight.save()