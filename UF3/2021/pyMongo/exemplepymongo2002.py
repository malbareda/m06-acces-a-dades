from pymongo import MongoClient
import pymongo
#totalmente opcional
from pprint import pprint

client = MongoClient()
pprint(client.list_database_names())

#client es un diccionario
db = client["video"]
#db es un diccionario
##wait its all diccionarios?
##always has been


coll = db["moviesDAMVI"]

##cogeme el primero
doc = coll.find_one()

pprint(doc["title"])
pprint(doc["actors"][0])

doca91 = coll.find({"year":1991})
for i in doca91:
    pprint(i["title"])

##FILTRO (WHERE), PROYECCION (SELECT)
#es necesario lo del _id:0 porque si no se va a mostrar la id siempre
doc1 = coll.find({"year":2014},{"_id":0,"title":1,"runtime":1})

for i in doc1:
    pprint(i)   

pprint("ejemplo de sort")
##ejemplo de sort
doc2 = coll.find({"year":2014},{"_id":0,"title":1,"runtime":1}).sort("title",pymongo.ASCENDING)
for i in doc2:
    pprint(i)   

pprint("ejemplo de insert")
sonic = {"title": "Sonic2", "year":2022, "type": "furros"}
coll.insert_one(sonic)



pprint("ejemplo de update")
##el update y el delete pueden ser update_one o update_many 
print("exemple de update one")
coll.update_many({"year":2014},{ "$set": { "type": "animale antropomorficos extrañamente atractivos"} })
doc2 = coll.find({"year":2014},{"_id":0,"title":1,"type":1}).sort("title",pymongo.ASCENDING)
for i in doc2:
    pprint(i)   
doc1 = coll.find({"year":2022},{"_id":0,"title":1,"type":1})

for i in doc1:
    pprint(i)   

pprint("ejemplo de delete")
coll.delete_many({"title": "Sonic2"})    

