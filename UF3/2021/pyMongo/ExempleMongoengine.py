from mongoengine import *
from pprint import pprint

##nos conectamos a la base de datos example
db = connect('example')
##borra la bd
db.drop_database('example')

print(0)

#Choice, algo tiene que estar en estos valores. Funciona como un enum
SINO = ('Si','No')

#Document: Documento base. Creara su propia collection.
##pasar parametro a una declaracion de clase: HERENCIA. User es hijo de Document
class User(Document):
    #usamos campos xField que son clases propias de MongoEngine
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    premium = StringField(choices=SINO)

#Embedded Document, documento que va dentro de otro. Objeto JSON que va dentro de un doc
class Comment(EmbeddedDocument):
    content = StringField()
    name = StringField(max_length=120)

class Post(Document):
    title = StringField(max_length=120, required=True)
    #referencia. Permite referenciar el objectId de otra collection. En Python se accedera como si fuese un objeto de la clase
    author = ReferenceField(User, reverse_delete_rule=CASCADE)
    tags = ListField(StringField(max_length=30))
    comments = ListField(EmbeddedDocumentField(Comment))

    #permite clases heredadas
    meta = {'allow_inheritance': True}

#Clases heredadas: Van dentro de la misma collection que el padre, con un identificador especial
class TextPost(Post):
    content = StringField()

class ImagePost(Post):
    image_path = StringField()

class LinkPost(Post):
    link_url = StringField()

print(1)

#crear INSERT
#recordad que en python los constructores funcionan como en C#. Es decir que puedes definir los campos a mano)
ross = User(email='ross@example.com', first_name='Ross', last_name='Lawley', premium='Si')
ross.save()
marc = User(email='marc@example.com', first_name='Marc', last_name='Albareda', premium='No')
marc.save()

post1 = TextPost(title='Fun with MongoEngine', author=ross)
post1.content = 'Took a look at MongoEngine today, looks pretty cool.'
post1.tags = ['mongodb', 'mongoengine']
post1.save()

#update
post1.content = 'otro content'
post1.save()

#Fijaos que noe stoy creando los posts como Post, sino como sus clases heredadas
post2 = LinkPost(title='MongoEngine Documentation', author=ross)
post2.link_url = 'http://docs.mongoengine.com/'
post2.tags = ['mongoengine']
post2.save()

post3 = ImagePost(title='fotos de gatos', author=marc)
post3.image_path = 'http://docs.mongoengine.com/gatete.png'
post3.tags = ['gatetes']
post3.save()

#borrar
post2.delete()

print(2)



print("ejemplo para carlos")
pprint(post3.author.first_name)
#buscar
#NombreDelaClase.objects es un campo estatico que te deveuvlve una lista con todos los documentos de esa colleciton
for post in Post.objects:
    print(post.title)

#buscar en clase heredada. Automaticamente te cogera los que son TextPost, ignorando los otros
for post in TextPost.objects:
    print(post.content)


print ("\n\n\n Busqueda")

#coges todos los posts
for post in Post.objects:
    ##imprime el titulo y una barra separadora
    print(post.title)
    print('=' * len(post.title))

    #si (isinstance es como el instanceof) el post es un textpost
    if isinstance(post, TextPost):
        print("texto: ",post.content)

    #si el post es un linkpost
    if isinstance(post, LinkPost):
        print('Link: ',post.link_url)


print ("\n\n\n Busqueda con parametros ")

#busqueda con parametros de busqueda
for post in Post.objects(tags='mongodb'):
    print(post.title)

for post in Post.objects(title='fotos de gatos'):
    print(post.author.first_name)

#contar
num_posts = Post.objects(tags='mongodb').count()
print('Found '+str(num_posts)+' posts with tag "mongodb"')

#buscar y modificar
usuario = User.objects(first_name='Ross')[0]
usuario.first_name='raul'
usuario.save()

for user in User.objects:
    pprint(user.first_name)
