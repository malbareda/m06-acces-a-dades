# Generated by Django 3.2.12 on 2022-03-21 17:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pregunta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto_pregunta', models.CharField(max_length=500)),
                ('data', models.DateTimeField(verbose_name='data de la pregunta')),
            ],
        ),
        migrations.CreateModel(
            name='PossibleResposta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto_respuesta', models.CharField(max_length=500)),
                ('vots', models.IntegerField(default=0)),
                ('pregunta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pregunkane.pregunta')),
            ],
        ),
    ]
