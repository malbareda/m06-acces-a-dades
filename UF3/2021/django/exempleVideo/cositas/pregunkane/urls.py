from django.urls import path

from . import views

app_name = "pregunkane"
urlpatterns = [
    #si esta buit, ens envia a la vista index
    path('', views.index, name="index"),
    path('kane', views.index2, name="index2"),

    #a les URL puc posar parametres
    path('<int:question_id>/', views.detail, name='detail'),

    path('<int:question_id>/results/', views.results, name='results'),

    path('<int:question_id>/vote/', views.vote, name='vote'),
]
