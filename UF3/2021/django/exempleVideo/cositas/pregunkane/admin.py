from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Pregunta, PossibleResposta

admin.site.register(Pregunta)
admin.site.register(PossibleResposta)