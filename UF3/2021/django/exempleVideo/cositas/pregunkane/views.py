from django.shortcuts import render
from django.http import Http404
# Create your views here.
from django.http import HttpResponse
from .models import *
from django.shortcuts import get_object_or_404, render


def index(request):
    #agafem amb NomClase.objects . tenim diversos metodes com get i order by. el - significa descendent
    ultimaspreguntas = Pregunta.objects.order_by('-data')[:5]
    #creo un diccionari context
    context = {'ultimaspreguntas': ultimaspreguntas}
    #render enviem el context com el compact
    return render(request, 'pregunkane/index.html', context)

def index2(request):
    return HttpResponse("Kane ya no tiene Kovid pero tiene pendiente")

def detail(request, question_id):
    try:
        pregunta = Pregunta.objects.get(pk=question_id)
    except Pregunta.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'pregunkane/detalle.html', {'pregunta': pregunta})

def results(request, question_id):
    response = "Mirando los resultados de la encuesta %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    pregunta = Pregunta.objects.get(pk=question_id)
    respuestaSeleccionada = pregunta.respuestas.get(pk=request.POST["choice"])
    respuestaSeleccionada.vots+=1
    respuestaSeleccionada.save()
