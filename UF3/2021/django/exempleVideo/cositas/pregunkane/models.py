from django.db import models

# Create your models here.
from django.db import models

class Pregunta(models.Model):
    texto_pregunta = models.CharField(max_length=500)
    data = models.DateTimeField('data de la pregunta')

    def __str__(self):
        return self.texto_pregunta

class PossibleResposta(models.Model):
    pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE, related_name='respuestas')
    texto_respuesta = models.CharField(max_length=500)
    vots = models.IntegerField(default=0)

    def __str__(self):
        return self.texto_respuesta

