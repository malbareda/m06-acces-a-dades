# CLASSIFY IRIS. CLASSIFICA L'EXEMPLE BASIC D'IRIS SETOSA AMB ELS SEGÜENTS ALGORITMES DE CLASSIFICACIO
# K-Nearest-Neighbour
# Naive Bayes Classificator
# Regressió Logística
# Support Vector Machine
# Arbres de Decisió
# Random Forests
# Multi-Layer Perceptron (proto - Xarxa Neuronal)

# imports
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import load_iris
import argparse

#Seleccionar el model
print("Select model: knn, naive_bayes, logit, svm, decision_tree, random_forest, mlp")
modelsel = input()

# Defineix el diccionari de models. 
# Cada model cridarà a una funció diferent del sklearn
# fa l'equivalent a un switch
models = {
	"knn": KNeighborsClassifier(n_neighbors=1),
	"naive_bayes": GaussianNB(),
	"logit": LogisticRegression(solver="lbfgs", multi_class="auto"),
	"svm": SVC(kernel="rbf", gamma="auto"),
	"decision_tree": DecisionTreeClassifier(),
	"random_forest": RandomForestClassifier(n_estimators=100),
	"mlp": MLPClassifier()
}

# Carrega el dataset d'Iris i el divideix en dos datasets aleatoriament
# Fa servir 3/4 per training i 1/4 per avaluació
#TRAINING  Tu le dices al programa. Eh, que ese punto es una setosa.
#EVALUACION  Le das un punto y que el programa te diga que es. Luego evaluas si lo ha hecho bien o no.
print("[INFO] loading data...")
dataset = load_iris()
(trainX, testX, trainY, testY) = train_test_split(dataset.data,
	dataset.target, random_state=3, test_size=0.25)

# Entrena el model amb el dataset de 75%
print("[INFO] using '"+modelsel+"' model")
model = models[modelsel]
model.fit(trainX, trainY)

# Fa prediccions sobre el model d'avaluació i mostra els resultats
print("[INFO] evaluating...")
predictions = model.predict(testX)
print(classification_report(testY, predictions,
	target_names=dataset.target_names))


#precision: Que hi hagi pocs falsos positius. 
#per exemple: Si he dit que 50 plantes son setosa, i d'aquestes ho eran 45. La precisió es 90%

#recall: Que hi hagi pocs falsos negatius.
#per exemple: Si en el model hi havien 100 setoses i jo he dit que 50 ho eren, el recall es 50%



#f1score es la mitja. (mes o menys)

#precisio. He dit que hi havien 92. I els 92 ho eren. 100% precisio. 0 falsos positius
#No obstant, en realitat n'hi havien 100, aixi que tinc 8 falsos negatius. 92% recall

#He dit que hi havien 108. D'aquests 108 només 100 ho eren. 92% precisió. 8 falsos positius.
#No obstant, els 100 que hi havien els he detectat correctament. 0 falsos negatius. 100% recall.