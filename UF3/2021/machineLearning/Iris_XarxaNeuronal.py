# Classificador Iris mitjançant Deep Learning. Fa servir xarxes neuronals


# imports
from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import SGD
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import load_iris

# Carrega el dataset d'Iris i el divideix en dos datasets aleatoriament
# Fa servir 3/4 per training i 1/4 per avaluació
print("[INFO] loading data...")
dataset = load_iris()
(trainX, testX, trainY, testY) = train_test_split(dataset.data,
	dataset.target, test_size=0.25)

# Codifica les etiquetes amb vectors d'entrada
lb = LabelBinarizer()
trainY = lb.fit_transform(trainY)
testY = lb.transform(testY)

#Crea la xarxa neuronal
model = Sequential()
#Afegeix la capa d'entrada, que sera de 4 i fara servir sigmoid
model.add(Dense(3, input_shape=(4,), activation="sigmoid"))
#Afegeix una capa intermitja de 3, també amb sigmoid
model.add(Dense(3, activation="sigmoid"))
#Afegeix una capa final de 3, amb un classificador softmax (donarà output i serà amb percentatges de probabilitat)
model.add(Dense(3, activation="softmax"))

# Inicialitza l'optimitzador i model d'entrenament
print("[INFO] training network...")
opt = SGD(lr=0.1, momentum=0.9, decay=0.1 / 250)
model.compile(loss="categorical_crossentropy", optimizer=opt,
	metrics=["accuracy"])
# Entrenament. Aqui has d'indicar cuantes "epoques" (generacions) vols entrenar l'algoritme.
H = model.fit(trainX, trainY, validation_data=(testX, testY),
	epochs=250, batch_size=16)

# Avalua la xarxa
print("[INFO] evaluating network...")
predictions = model.predict(testX, batch_size=16)
print(classification_report(testY.argmax(axis=1),
	predictions.argmax(axis=1), target_names=dataset.target_names))