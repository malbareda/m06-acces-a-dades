from mongoengine import *
from pprint import pprint

# nos conectamos a la base de datos example
db = connect('video')
# borra la bd


print(0)


# Document: Documento base. Creara su propia collection.
# pasar parametro a una declaracion de clase: HERENCIA. User es hijo de Document
class moviesDAMVI(Document):
    # usamos campos xField que son clases propias de MongoEngine
    title = StringField(required=True)
    year = IntField()
    rated = StringField(max_length=50)
    runtime = IntField()
    countries = ListField(StringField(max_length=30))
    genres = ListField(StringField(max_length=30))
    director = StringField(max_length=50)
    writers = ListField(StringField(max_length=60))
    actors = ListField(StringField(max_length=60))


print(moviesDAMVI.objects)
# busqueda con parametros de busqueda
for movie in moviesDAMVI.objects:
    print(movie.title)
