import pymongo
from pymongo import MongoClient
from pprint import pprint

client = MongoClient()
#nos conectamos a db project
db = client["project"]
#nos conectamos a la collection pokemon que esta dentro de db (por tanto de project)
pokemons = db["pokemon"]
#findOne
doc = pokemons.find_one()
#pprint("el primer documento es ")
#pprint imprime de forma pretty
#pprint(doc)



while(True):
    print("PokeMONGO, Introdueix comanda")
    #split, separa, lineaEntrada
    lineaEntrada = input().split()
    #Search nombrePokemon atributo
    #Catch nombrePokemon
    #Release nombrePokemon
    #Candy
    print(lineaEntrada)
    if(lineaEntrada[0].lower()=='bye'):
        break
    if(lineaEntrada[0].lower()=='search'):
        print('Modo busqueda')
        #modo busqueda de ejemplo, siempre te buscara la altura. Y solo con nombre, no busca por numero
        pokemonAConsultar = lineaEntrada[1]
        #si quisieramos buscar por numero hariamos un isDigit()
        #RECORDAMOS:  ({BUSQUEDA}, {PROYECCION})).sort()
        docnombres = pokemons.find({"name":pokemonAConsultar},{"_id":0,"name":1,"height":1}).sort("height",pymongo.ASCENDING)
        #porque pongo 0? porque solo deberia haber una
        #tupla es una serie de valores que pueden ser distintos tipos y es inmutable
        print(docnombres[0])
        tuplaResultado = (docnombres[0]["name"],docnombres[0]["height"])
        pprint(tuplaResultado)
    if(lineaEntrada[0].lower()=='data'):
        print('Modo Data')
        #modo busqueda de ejemplo, siempre te buscara la altura. Y solo con nombre, no busca por numero
        pokemonAConsultar = lineaEntrada[1]
        #si quisieramos buscar por numero hariamos un isDigit()
        #RECORDAMOS:  ({BUSQUEDA}, {PROYECCION})).sort()
        docnombres = pokemons.find({"name":pokemonAConsultar})
        pprint(docnombres[0])
        '''#forma 1 usando las propiedades de los diccionarios
        #la comanda in funciona de una forma similar a un contains. Funciona tanto para listas como para diccionarios o tuplas
        #si es especifcamente en un diccionario lo buscara en las llaves, no en los valores. Siempre puedes coger dicc.values() que es una lista
        if "candy_count" in docnombres[0]:
            tuplaResultado = (docnombres[0]["name"],docnombres[0]["candy_count"])
            pprint(tuplaResultado)
        else:
            pprint("no")
        '''
        #forma 2, usando un try/except
        ##el try funciona iguyal que en java
        try:
            tuplaResultado = (docnombres[0]["name"],docnombres[0]["candy_count"])
            pprint(tuplaResultado)
        #esto es lo que hara si hay yuna excepcion. tambien podrias poner except KeyError: que solo trataria
        #las excepciones de tipo KeyError
        except:
            pprint("no")  
        #en python tambien hay el equivalente al finally que se llama: finally: 


        
    


