import pymongo
from pymongo import MongoClient
from mongoengine import *
import datetime
import random
from pprint import pprint

db = connect('project')


'''
{
"_id":{"oid":"58f56170ee9d4bd5e610d644"},
"id":1.0,
"num":"001",
"name":"Bulbasaur",
"img":"http://www.serebii.net/pokemongo/pokemon/001.png",
"type":["Grass","Poison"],
"height":"0.71 m",
"weight":"6.9 kg",
"candy":"Bulbasaur Candy",
"candy_count":25.0,
"egg":"2 km",
"spawn_chance":0.69,
"avg_spawns":69.0,
"spawn_time":"20:00",
"multipliers":[1.58],
"weaknesses":["Fire","Ice","Flying","Psychic"],
"next_evolution":[
{"num":"002","name":"Ivysaur"},
{"num":"003","name":"Venusaur"}
]
},
'''
#Tupla
TYPES = ('Bug','Dark','Dragon','Electric','Fairy','Fight','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')

#Tiene que estar obligatoriamente antes de Pokemon. Python carga por orden
class Evolution(EmbeddedDocument):
    num = StringField()
    name = StringField()

class Pokemon(Document):
    #db_field busca el nombre en mongo, si es que es distinto a como lo tienes en el nombre de la clase
    pid = FloatField(required = True, db_field='id')
    num = StringField(required = True)
    name = StringField(required = True)
    img = StringField(required = True)
    #type es lista de strings. Pero tienen que ser aquellas definidas en la tupla TYPES (funciona como un enum)
    type = ListField(choice = TYPES, required = True)
    height = StringField(required = True)
    weight = StringField(required = True)
    candy = StringField(required = True)
    candy_count = FloatField(min_value = 0, max_value = 500)
    egg = StringField(required = True)
    spawn_chance = FloatField(required = True)
    avg_spawns = FloatField(required = True)
    spawn_time = StringField(required = True)
    multipliers = ListField(FloatField(),required = True)
    weaknesses = ListField(choice = TYPES,required = True)
    #next_evolutione s una lista de Evolution, que es el documento incrustado que acabamos de crear
    next_evolution = ListField(EmbeddedDocumentField('Evolution'))
    prev_evolution = ListField(EmbeddedDocumentField('Evolution'))

Chikorita = Pokemon(
    pid = 152.0,
    num = "152",
    name = "Chikorita",
    img = "a.com",
    type = ["Grass"],
    height = "0.9 m",
    weight = "6.4 kg",
    candy = "Chikorita Candy",
    candy_count = 25.0,
    egg = "2 Km",
    spawn_chance = 0.69,
    avg_spawns = 69.0,
    spawn_time = "20:00",
    multipliers = [1.58],
    weaknesses = ["Fire","Ice","Flying","Bug"],
    #next_evolutione s una lista de Evolution, que es el documento incrustado que acabamos de crear
    next_evolution = [ {"num":"153","name":"Bayleaf"}, {"num":"154","name":"Meganium"} ]
)


Bayleaf = Pokemon(
    pid = 153.0,
    num = "153",
    name = "Bayleaf",
    img = "a.com",
    type = ["Grass"],
    height = "0.9 m",
    weight = "6.4 kg",
    candy = "Chikorita Candy",
    candy_count = 100.0,
    egg = "2 Km",
    spawn_chance = 0.69,
    avg_spawns = 69.0,
    spawn_time = "20:00",
    multipliers = [1.58],
    weaknesses = ["Fire","Ice","Flying","Bug"],
    #next_evolutione s una lista de Evolution, que es el documento incrustado que acabamos de crear
    next_evolution = [ {"num":"154","name":"Meganium"} ]
)

Chikorita.save()
Bayleaf.save()


#NombreDelaClase.objects es un campo estatico que te deveuvlve una lista con todos los documentos de esa colleciton
for pkmn in Pokemon.objects:
    print(pkmn.name)
    #pkmn.delete()
