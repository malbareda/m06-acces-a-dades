import pymongo
from pymongo import MongoClient
from mongoengine import *
import datetime
import random
from pprint import pprint

db = connect('project')

'''
{
	"_id": {"oid": "8979dsas8762198asdsafase"},
	"num": "001",
	"name": "Bulbasaur",
	"type": ["Grass", "Poison"],
“catch_date”:,
	"CP": 340.0,
	"HPmax": 300.0,
	"HP": 300.0,
	"atk": 20,
	"def": 20,
	"energy": 0,
	"moves": {
        "fast": "66f56170ee9d4bd5e610d644",
        “charged": "66f56170ee9d4bd5e610d644"
	},
	"candy": "Bulbasaur Candy",
	"candy_count": 25.0,
	"current_candy": 0.0,
	"weaknesses": ["Fire", "Ice", "Flying", "Psychic"],

}
'''

TYPES = ('Bug','Dark','Dragon','Electric','Fairy','Fight','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')


##crear nueva clase que seria embeddeddocument
##que dos cosas tendria
##fast: que es una referencia A MOVE
##charged: que es otra referencia


class Team(Document):
    #el _id por supuesto que no se trata porque ya te lo genera mongo automaticamente. Es el equivalente al autonumerico.
    num = StringField(required = True)
    name = StringField(required = True)
    #type es lista de strings. Pero tienen que ser aquellas definidas en la tupla TYPES (funciona como un enum)
    ptype = ListField(db_field='type', choice = TYPES, required = True)
    #catch date: Data. Muy facil
    #campos numericos, CP, HPmax, HP, atk, def, energy
    candy = StringField(required = True)
    candy_count = FloatField(min_value = 0, max_value = 500)
    #current_candy
    weaknesses = ListField(choice = TYPES,required = True)
    #moves = EmbeddedDocumentField referenciando a la calse que acabamos de crear