print("el resultado es "+str(list(map(lambda x: x+1,(list(map(int,input().split())))))))


#input te lee siempre como un nextLine()
#print(input())
#te divide una string por sus elementos constituyentes en una lista. Por defecto lo hace por espacio
#print(input().split())
#map es una funcion tipica de lenguajes funcionales
#a map le pasas por parametro una funcion (eso en java no se puede) y uyna lista. y aplica esta funcion a todos los elementos de la lista
#print(map(int,input().split()))
#lambda es una funcion anonima, que solo declaras una vez. en ese caso significa "por cada elemento de la lista, sumale 1"
#print(list(map(lambda x: x+1,(list(map(int,input().split()))))))
