print("iker")

a = 4
b = "iker iker"
c = True
b+="1"

print(b+str(1))

a="iker"
if (a=="iker"):
    print("iker")
elif (a=="sergio"):
    print("sergio")
else:
    print("luis")


a = 0
lista = []
while a <10:
    print(a)
    a+=1
    lista.append(a)

print(lista)
#imprime el primero
print(lista[0])
#imprime el ultimo
print(lista[-1])
#imprime los 3 primeros
print(lista[:3])
#imprime los que van despues de los 3 primeros
print(lista[3:])
#imprime los 3 ultimos
print(lista[-3:])
#imprime del 3 al 6
print(lista[3:6])
#imprime del primero al ultimo con un step de 2
print(lista[0:-1:2])
#imprime la lista girada
print(lista[::-1])
#imprime la longitud de la lista
print(len(lista))

tupuedestenerlastring="iker me mira con muchas ganas de pegarse un tiro en la cara"
#las strings funcionan igual que las listas
print(tupuedestenerlastring[3:17])

for i in tupuedestenerlastring:
    print(i)

#for (i=0;i<10;i++)
for i in range(0,10,1):
    print(i)

#las llistes a python son multitipus
terrrorismo= ["iker", 2, True, 4.6, [1,2,3], None, (4,4)]

#TUPLA, combinacio inmutables
coord = ('C',4)
#funcions de llistes tuples etc . count
print(("iker","iker","iker").count("iker"))
print(("iker","iker","iker","sergio").index("sergio"))


#Diccionari
dicc = {"nom":"kane","edat":29,"canes":True}
print(dicc)
print(dicc["canes"])
#EL DICCIONARI DE PYTHON FUNCIONA EXACTAMENT IGUAL QUE UN JSON
#AIXO ES UNA COSA RANDOM QUE POSO EN MAJUSCULES PERO SEGUR QUE NO TE CAP RELLEVANCIA EN AQUESTA UF
#EN UNA UF D'ACCES A BASES DE DADES MONGO
#QUE FUNCIONEN AMB JSON
dicc["edat"]=40
dicc["favoritos"]=["rimuru","Pau","Pingpong","rimuru una altra vegada","tenir 35 de temperatura i no venir","tenir covid 6 setmanes",14]
print(dicc)
dicc["policia?"]=True
print(dicc)
#esborrar
print(dicc.pop("canes"))
print(dicc)
for i in dicc.keys():
    print (dicc[i])