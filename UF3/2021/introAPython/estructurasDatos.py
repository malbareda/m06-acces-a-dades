#listas

lista = [1,2,3]
#listas tienen comandos como en java
lista.append(4)
print(len(lista))
print(lista)
print(lista[2])
print(lista[-1])
print(lista[2:])
print(lista[:2])
print(lista[1:3])
#una string funciona igual que una lista
s = "gatete"
print(s[1:3])
#en python no hay for, solo for each
print("vamos a hacer fors")
for i in lista:
    print(i)
for i in s:
    print(i)
#join, usar esto como separador de todos los elementos de un iterable
print('CORONAVIRUS'.join(map(str,lista)))

#FOR NORMAL, DE LOS DE TODA LA VIDA
#Range: Inicio, Fin, Step
for i in range(1,6,2):
    print(i)
#las listas son MULTITIPO
mlist = [1,"gatete",'a',True,3.4,[1,2,3],None,(4,"gatete")]
for i in mlist:
    print(i)

#TUPLA. La tupla es una combinacion de diferentes tipos, INMUTABLE.
coord = ('A',4)
print(coord)
print(coord[0])
#count, cuenta elementos
print(('A','A','A',4).count('A'))
#index, indexof
print(('A','A','A',4).index(4))
#No se puede modificar
#coord[0]='B'
for i in coord:
    print(i)

#DICCIONARIOS - HashMap - Objeto JSON
dicc = {"nombre":"Marc","edad":28,"coronavirus":True}
print(dicc)
print(dicc["nombre"])
for i in dicc.keys():
    print(i)
dicc["nombre"]="Alvareda"
dicc["favoritos"]=["gatos","mas gatos",8,["silla gaming","impresora 3d","libros","coronavirus"],"olvidarme del main"]
print(dicc)