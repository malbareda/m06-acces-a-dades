print("hola mundo")
print("kernel")
a = 5
a = a + 4
print(a)
if a== 5:
    print("esto se hace dentro del if")
print("esto no")

#comentarios con almohadilla
lista = []

a = 0

while 5-a > 0 :
    a+=1
    lista.append(a)
#la lista se puede imprimir entera
print(lista)
#la lista se accede como un array de java
print(lista[0])

#si pones -1 te devuelve el ultimo, -2 el penultimo, etc
print(lista[-2])

#en listas, el : es el rango
print(lista[:-2])
print(lista[-2:])
