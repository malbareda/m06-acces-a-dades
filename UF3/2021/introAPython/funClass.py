#declaras la clase como en Java
class Profesor:
  #constructor
  
  def __init__(self, name, age):
    self.name = name
    self.age = age
  #todas los métodos deben tener el self, que es
  #como el this en java. No se debe pasar
  def saludar(self):
    print("Hola, soy " + self.name)

  def cogecorre(self,cosa):
    print(self.name+" coge la "+cosa+" y corre")

def suma(a,b):
  res = a+b
  return res

def guauguau():
  print("guau guau")
  #guauguau() la recursivitat funciona
  return

def maxMin(lista):
  maximo = max(lista)
  minimo = min(lista)
  return (maximo,minimo)
  

guauguau()
print(suma(1,2))
print(maxMin([1,2,3,4]))


marc = Profesor("Marc",28)
marc.saludar()
marc.cogecorre("caja")

