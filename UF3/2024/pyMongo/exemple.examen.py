from pymongo import *
from mongoengine import *
from pprint import pprint


def a(titol):
    a = coll.find_one({"titol":titol},{"_id":0,"titol":1,"any":1})
    pprint(a)

def b(titol):
    b = coll.find_one({"titol":titol},{"_id":0,"category.codi":1,"category.type":1})
    pprint(b)

    
def addBook(titol,any,code,type):
    llibre = {
        "titol":titol,
        "any":any,
        "category":{
            "codi":code,
            "type":type,
        },
    }
    
    b = coll.insert_one(llibre)

def updateAny(titol,any):
    coll.update_one({"titol":titol}, {"$set":{"any":any}})
    
    
def deleteOldBooks(any):
    coll.delete_many({"any":{'$lt':any}})
    
TIPUS = ('Fiction', 'Non-Fiction', 'Reference', 'Academic')    
    
class Category(EmbeddedDocument):
    code = IntField(min_value=1)
    type = StringField(choices=TIPUS)
    
class Book(Document):
    titol = StringField()
    any = IntField()
    category = EmbeddedDocumentField(Category)
    
    def clean(self):
        if self.any < 1900 or self.any > 2025:
            raise ValidationError("Año Chino de la Serpiente")

    
class Shelf(Document):
    location = StringField()
    books = ListField(ReferenceField(Book))
    

def createCategory(codi, tipus):
    cat = Category(codi=codi, tipus=tipus)
    return cat

def AssignBook(Ntitol, Nlocation):
    shelf = Shelf.objects(location = Nlocation).first()
    book = Book.objects(titol = Ntitol).first()
    shelf.books.append(book)
    shelf.save()
    

def ListShelf(Nlocation):
    shelf = Shelf.objects(location = Nlocation).first()
    for b in shelf.books:
        print(str(b.titol)+" "+str(b.category.codi))
        
        
        
# Creem una instància del client de MongoDB. Sense paràmetres es connecta a localhost:27017
client = MongoClient()

# Accedim (o creem si no existeix) a la base de dades "Pokemon"
db = client["Examen"]
pprint(db)
# Accedim (o creem si no existeix) a la col·lecció "Pokemon" dins la base de dades Pokemon
coll = db["Book"]
pprint("la coleccion es")
pprint(coll)


addBook("A",1950,1234,"Fiction")
addBook("B",1970,4231,"Fiction")
addBook("C",2000,5678,"Fiction")
addBook("D",1850,9876,"Fiction")

a("A")
b("B")
updateAny("D",2022)
a("D")
deleteOldBooks(1960)

