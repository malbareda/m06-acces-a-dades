# Importacions necessàries per treballar amb MongoDB
import pymongo
from pymongo import MongoClient
from pprint import pprint

# Inicialitzem connexió a MongoDB local
client = MongoClient()
pprint("las bases de datos son ")
pprint(client.list_database_names())

# Seleccionem la base de dades Pokemon
db = client["Pokemon"]
pprint("la db es")
pprint(db)

# Seleccionem la col·lecció shipwrecks (vaixells enfonsats)
coll = db["shipwrecks"]
pprint("la coleccion es")
pprint(coll)

# Busquem documents que tinguin el camp "history" i que no estigui buit
# $exists:True verifica que el camp existeix
# $ne:"" verifica que no està buit
# Només retornem els camps coordinates i history
cosas = coll.find({"history":{"$exists":True, "$ne":""}},{"coordinates":1,"history":1})
for i in cosas:
    pass#pprint(i)
    
# Busquem documents que continguin "explo" en el camp history
# $regex permet fer cerques utilitzant expressions regulars
# En aquest cas comptem quants documents compleixen la condició
cosas = coll.find({"history":{"$regex":"explo"}},{"coordinates":1,"history":1})
pprint(len(list(cosas)))

# Busquem documents on la latitud decimal sigui menor que 5
# $lt és l'operador "less than" (menor que)
# Mostrem la latitud decimal i la història dels vaixells trobats
cosas = coll.find({"latdec":{"$lt":5}},{"latdec":1,"history":1})
for i in cosas:
    pprint(i)