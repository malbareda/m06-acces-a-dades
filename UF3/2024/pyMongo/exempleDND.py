# Importacions necessàries per treballar amb MongoDB
import pymongo
from pymongo import MongoClient
from pprint import pprint

# Inicialitzem connexió a MongoDB local
client = MongoClient()
pprint("las bases de datos son ")
pprint(client.list_database_names())

# Seleccionem la base de dades Pokemon
db = client["Pokemon"]
pprint("la db es")
pprint(db)

# Seleccionem la col·lecció dnd (Dungeons & Dragons)
coll = db["dnd"]
pprint("la coleccion es")
pprint(coll)

# Busquem personatges amb destresa (dex) superior a 14
# stats.dex accedeix a un camp anidat (dex dins de l'objecte stats)
# $gt és l'operador "greater than" (major que)
# Només retornem els camps name i stats de cada document
cosas = coll.find({"stats.dex":{"$gt":14}},{"name":1,"stats":1})
for i in cosas:
    pprint(i)