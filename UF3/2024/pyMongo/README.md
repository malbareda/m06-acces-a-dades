# PyMongo - Driver natiu de MongoDB per Python

## Configuració Inicial

### Importacions Necessàries
```python
import pymongo
from pymongo import MongoClient
from pprint import pprint
```

### Connexió a MongoDB
```python
client = MongoClient()  # Connexió a localhost:27017
```

### Accés a Bases de Dades i Col·leccions
```python
# Llistar bases de dades
client.list_database_names()

# Accedir a una base de dades
db = client["Pokemon"]

# Accedir a una col·lecció
coll = db["Pokemon"]  # o ["shipwrecks"] o ["dnd"]
```

## Operacions de Lectura (Read)

### Cerca Bàsica

#### Find One
Retorna el primer document que troba:
```python
doc = coll.find_one()
```

#### Find
Retorna un cursor amb tots els documents que compleixen els criteris:
```python
resultat = coll.find({"candy_count": 25})
```

### Projecció de Camps
Seleccionar quins camps volem que retorni:
```python
# 1 indica que volem el camp, 0 que no el volem
coll.find(
    {"candy_count": 25},
    {"_id": 0, "name": 1, "candy_count": 1}
)
```

### Operadors de Consulta

#### Operadors de Comparació
```python
# $lt - menor que
coll.find({"latdec": {"$lt": 5}})

# $gt - major que
coll.find({"stats.dex": {"$gt": 14}})
```

#### Operadors d'Existència
```python
# Comprovar si un camp existeix i no està buit
coll.find({
    "history": {
        "$exists": True,  # El camp ha d'existir
        "$ne": ""        # No pot estar buit
    }
})
```

#### Operadors de Text
```python
# $regex - cerca per expressió regular
coll.find({
    "history": {
        "$regex": "explo"  # Cerca textos que continguin "explo"
    }
})
```

### Consultes en Documents Niuats
Per accedir a camps dins d'objectes niuats, utilitzem la notació punt:
```python
# Accedir a dex dins de l'objecte stats
coll.find({"stats.dex": {"$gt": 14}})
```

### Ordenació
```python
# Ordenar resultats per nom
coll.find().sort("name")
```

## Operacions d'Escriptura (Write)

### Inserció
```python
# Inserir un nou document
chen = {
    "name": "chen",
    "id": 152,
    "candy_count": 999,
    # ... més camps
}
coll.insert_one(chen)
```

### Actualització
```python
# Actualitzar múltiples documents
coll.update_many(
    {"candy_count": 999},  # Filtre
    {"$set": {"candy_count": 0}}  # Nou valor
)
```

### Eliminació
```python
# Eliminar múltiples documents
coll.delete_many({"name": "chen"})
```

## Bones Pràctiques

1. **Projecció de Camps**
   - Seleccionar només els camps necessaris per millorar el rendiment
   - Excloure `_id` si no es necessita

2. **Consultes Eficients**
   - Utilitzar els operadors adequats ($gt, $lt, $exists, etc.)
   - Fer servir índexs per camps freqüentment consultats

3. **Gestió de Resultats**
   - Utilitzar iteradors per grans conjunts de resultats
   - Fer servir `pprint` per visualitzar millor els documents

4. **Estructura de Documents**
   - Aprofitar l'estructura niuada quan tingui sentit
   - Mantenir una estructura consistent en documents similars

## Exemples de Casos d'Ús

### Pokémon Collection
- Gestió de dades de Pokémon
- Evolucions i estadístiques
- Exemple de documents amb arrays i referències

### Shipwrecks Collection
- Dades geogràfiques (coordinates, latdec)
- Textos llargs (history)
- Cerca per contingut de text

### D&D Collection
- Estadístiques de personatges
- Documents amb estructura niuada
- Consultes sobre atributs específics

## Operadors Comuns

### Comparació
- `$lt`: Menor que
- `$gt`: Major que
- `$ne`: No igual a

### Text
- `$regex`: Expressions regulars
- `$exists`: Existència de camp

### Modificació
- `$set`: Establir valor