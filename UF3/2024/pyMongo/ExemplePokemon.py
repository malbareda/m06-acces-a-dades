# Importem el mòdul principal de pymongo que conté totes les funcionalitats per MongoDB
import pymongo
# Importem MongoClient que és la classe principal per connectar-nos a MongoDB
from pymongo import MongoClient
# Importem pprint per mostrar les dades de forma més llegible
from pprint import pprint

# Creem una instància del client de MongoDB. Sense paràmetres es connecta a localhost:27017
client = MongoClient()
pprint("las bases de datos son ")
# Obtenim i mostrem la llista de totes les bases de dades disponibles al servidor
pprint(client.list_database_names())

# Accedim (o creem si no existeix) a la base de dades "Pokemon"
db = client["Pokemon"]
pprint("la db es")
# Mostrem l'objecte de la base de dades
pprint(db)
# Accedim (o creem si no existeix) a la col·lecció "Pokemon" dins la base de dades Pokemon
coll = db["Pokemon"]
pprint("la coleccion es")
pprint(coll)

# Busquem i obtenim el primer document de la col·lecció amb find_one()
doc = coll.find_one()
pprint(doc)
pprint("---------------------------------")
# Mostrem el camp "name" del document
pprint(doc["name"])
# Mostrem el nom de la primera evolució del Pokemon (accedint a una array i després a un camp)
pprint(doc["next_evolution"][0]["name"])

# Busquem tots els Pokemon que necessiten 25 caramels per evolucionar
# Només mostrem els camps name i candy_count (el _id el excloem explícitament)
# Ordenem els resultats per nom
cosas = coll.find({"candy_count":25},{"_id":0,"name":1,"candy_count":1}).sort("name")
# Iterem sobre els resultats i els mostrem
for i in cosas:
    print(i)
    
print("insert")
# Creem un diccionari amb les dades del nou Pokemon "chen"
chen = {
    "name":"chen",                # Nom del Pokemon
    "id":152,                     # ID numèric
    "candy_count":999,            # Número de caramels necessaris
    "num":"152",                  # ID en format string
    "type":["Normal"],            # Tipus de Pokemon (array)
    "height":"1.70 m",            # Alçada
    "weight":"57.0 kg",           # Pes
    "candy":"Eeeh Naranja",       # Tipus de caramel
    "egg":"Not in eggs",          # Tipus d'ou
    "next_evolution":[            # Array d'evolucions
        {"num":"153","name":"ChenChen"},        # Primera evolució
        {"num":"154","name":"ChenChenChen"}     # Segona evolució
    ]
}
# Inserim el document "chen" a la col·lecció
coll.insert_one(chen)

# Busquem tots els documents amb name "chen" i els ordenem per nom
chenes = coll.find({"name":"chen"},{}).sort("name")
# Mostrem el primer document trobat
pprint(chenes[0])

# Actualitzem tots els documents que tenen candy_count 999
# Canviem el valor de candy_count a 0
coll.update_many({"candy_count":999}, { "$set":{"candy_count":0}})
# Tornem a buscar els documents amb name "chen" per verificar els canvis
chenes = coll.find({"name":"chen"},{}).sort("name")
# Mostrem tots els documents trobats
for i in chenes:
    pprint(i)
    
    

# Eliminem tots els documents que tenen name "chen"
coll.delete_many({"name":"chen"})