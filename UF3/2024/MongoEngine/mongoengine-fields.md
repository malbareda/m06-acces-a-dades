# MongoEngine Fields

## Tipus Bàsics de Fields

### StringField
Camp per emmagatzemar text.

```python
class User(Document):
    first_name = StringField(max_length=50)
    email = StringField(required=True)
```

Paràmetres comuns:
- `max_length`: Longitud màxima del text
- `min_length`: Longitud mínima del text
- `regex`: Expressió regular per validar el contingut
- `required`: Si és obligatori (True/False)
- `unique`: Si ha de ser únic en la col·lecció
- `choices`: Llista o tupla de valors permesos
- `default`: Valor per defecte

### IntField
Camp per números enters.

```python
class Post(Document):
    views = IntField(default=0)
    likes = IntField(min_value=0)
```

Paràmetres específics:
- `min_value`: Valor mínim permès
- `max_value`: Valor màxim permès

### FloatField
Camp per números decimals.

```python
class Product(Document):
    price = FloatField(min_value=0.0)
```

### BooleanField
Camp per valors True/False.

```python
class User(Document):
    is_active = BooleanField(default=True)
```

### DateTimeField
Camp per dates i hores.

```python
class Post(Document):
    created_at = DateTimeField(default=datetime.utcnow)
```

## Fields per Relacions

### ReferenceField
Crea una referència a un altre document.

```python
class Post(Document):
    author = ReferenceField(User, reverse_delete_rule=CASCADE)
```

Paràmetres específics:
- `document_type`: La classe del document referenciat
- `reverse_delete_rule`: Comportament en eliminar
  - `CASCADE`: Elimina els documents relacionats
  - `DENY`: Impedeix l'eliminació si hi ha referències
  - `NULLIFY`: Posa les referències a null
  - `PULL`: Elimina la referència d'arrays

### EmbeddedDocumentField
Per documents incrustats dins d'altres.

```python
class Comment(EmbeddedDocument):
    content = StringField()
    name = StringField(max_length=120)

class Post(Document):
    comments = ListField(EmbeddedDocumentField(Comment))
```

Característiques:
- No crea una col·lecció separada
- Els documents incrustats existeixen dins del document principal
- Útil per dades que sempre van juntes
- No tenen _id propi

## Fields per Col·leccions

### ListField
Camp per arrays de valors o documents.

```python
class Post(Document):
    tags = ListField(StringField(max_length=30))
    comments = ListField(EmbeddedDocumentField(Comment))
```

Paràmetres:
- `field`: El tipus de camp dels elements
- `max_length`: Longitud màxima de la llista
- `required`: Si la llista és obligatòria

### DictField
Camp per objectes/diccionaris.

```python
class User(Document):
    settings = DictField()
```

## Paràmetres Comuns per tots els Fields

### Validació
- `required`: Si el camp és obligatori
- `default`: Valor per defecte
- `unique`: Si ha de ser únic en la col·lecció
- `choices`: Valors permesos

```python
class User(Document):
    status = StringField(choices=['actiu', 'inactiu'])
    role = StringField(default='user')
```

### Restriccions
- `max_length`: Longitud màxima (strings, llistes)
- `min_length`: Longitud mínima
- `max_value`: Valor màxim (números)
- `min_value`: Valor mínim (números)

### Indexació
- `unique_with`: Crear índex únic compost
- `sparse`: Índex que ignora documents sense el camp
- `db_index`: Crear índex per aquest camp

```python
class User(Document):
    email = StringField(unique=True, sparse=True)
    username = StringField(db_index=True)
```

## Bones Pràctiques

1. **Elecció del Tipus de Field**
   - Usar ReferenceField per relacions entre col·leccions independents
   - Usar EmbeddedDocumentField per dades que sempre van juntes
   - Usar ListField per arrays de valors similars

2. **Validació**
   - Sempre definir `required=True` per camps obligatoris
   - Utilitzar `choices` per restringir valors possibles
   - Definir `max_length` per evitar dades massa llargues

3. **Relacions**
   - Definir `reverse_delete_rule` per gestionar eliminacions
   - Considerar l'impacte en el rendiment de les referències

4. **Indexació**
   - Crear índexs per camps de cerca freqüent
   - Utilitzar `unique=True` per garantir unicitat
   - Considerar índexs compostos quan sigui necessari