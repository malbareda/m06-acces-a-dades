# MongoEngine - Operacions de Cerca

## Sintaxi Bàsica
```python
# Format general
Classe.objects(camp=valor)
```

## Operadors de Comparació

### Igualtat
```python
# Igualtat exacta
User.objects(first_name="Ross")
User.objects(premium="Si")

# Diferent
User.objects(first_name__ne="Ross")
```

### Comparacions Numèriques
```python
# Major que (greater than)
Post.objects(likes__gt=10)

# Major o igual que (greater than or equal)
Post.objects(likes__gte=10)

# Menor que (less than)
Post.objects(likes__lt=10)

# Menor o igual que (less than or equal)
Post.objects(likes__lte=10)
```

### Llistes i Arrays
```python
# Valor en llista
Post.objects(tags__in=['mongodb', 'python'])

# No en llista
Post.objects(tags__nin=['django', 'flask'])

# Array conté tots els elements
Post.objects(tags__all=['mongodb', 'python'])

# Mida de l'array
Post.objects(tags__size=3)
```

## Operadors de Text

### Cerques de Text
```python
# Conté (contains)
Post.objects(content__contains="MongoEngine")

# Comença per (starts with)
User.objects(email__startswith="ross")

# Acaba en (ends with)
User.objects(email__endswith="@example.com")

# Expressions regulars
Post.objects(content__regex=r'^Mon.*')
```

### Operadors de Casos Especials
```python
# Existeix el camp
Post.objects(content__exists=True)

# Camp és null
Post.objects(content__exists=False)

# Tipus de camp
Post.objects(content__type="string")
```

## Cerques en Documents Incrustats

### Referència a Camps Niuats
```python
# Accés directe a camp niuat
Comment.objects(author__name="Ross")

# Múltiples nivells
Post.objects(comments__author__email="ross@example.com")
```

### Cerques en Arrays de Documents
```python
# Document compleix condició en array
Post.objects(comments__name="Ross")

# Element específic en array (per posició)
Post.objects(__raw__={'comments.0.name': 'Ross'})
```

## Operadors Lògics

### AND
```python
# Implícit (AND)
Post.objects(title="MongoEngine", author="Ross")

# Explícit
from mongoengine.queryset.visitor import Q
Post.objects(Q(title="MongoEngine") & Q(author="Ross"))
```

### OR
```python
from mongoengine.queryset.visitor import Q
# OR
Post.objects(Q(title="MongoEngine") | Q(title="MongoDB"))
```

### NOT
```python
# NOT
Post.objects(Q(title__ne="MongoEngine"))
```

## Modificadors de Consulta

### Ordenació
```python
# Ordre ascendent
Post.objects().order_by('title')

# Ordre descendent
Post.objects().order_by('-title')

# Múltiples camps
Post.objects().order_by('author', '-title')
```

### Limitació de Resultats
```python
# Límit
Post.objects().limit(10)

# Salt (skip)
Post.objects().skip(10)

# Paginació
Post.objects().skip(page * size).limit(size)
```

### Projecció de Camps
```python
# Només certs camps
Post.objects().only('title', 'author')

# Excloure camps
Post.objects().exclude('content')
```

## Agregació i Comptatge

### Comptatge
```python
# Comptar resultats
Post.objects(tags='mongodb').count()
```

### Agregació
```python
# Agrupar i comptar
Post.objects().aggregate([
    {'$group': {
        '_id': '$author',
        'total': {'$sum': 1}
    }}
])
```

## Bones Pràctiques

1. **Eficiència**
   - Utilitzar índexs per camps de cerca freqüent
   - Limitar els resultats quan sigui possible
   - Seleccionar només els camps necessaris

2. **Claredat**
   - Utilitzar operadors explícits quan la lògica és complexa
   - Separar consultes complexes en parts
   - Comentar consultes complexes

3. **Rendiment**
   - Evitar cerques de text sense índexs
   - Utilitzar limit() per grans conjunts de dades
   - Considerar la paginació per grans resultats