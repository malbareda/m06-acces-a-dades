from mongoengine import *
from pprint import pprint

# Importem mongoengine que és un ODM (Object-Document Mapper) per MongoDB
# pprint s'utilitza per imprimir de forma més llegible les estructures de dades

##nos conectamos a la base de datos example
db = connect('example')
##borra la bd
db.drop_database('example')

print(0)

#Choice, algo tiene que estar en estos valores. Funciona como un enum
SINO = ('Si','No')
# Definim una tupla amb valors possibles, similar a un enum
# Això s'utilitzarà per restringir els valors possibles en alguns camps

#Document: Documento base. Creara su propia collection.
##pasar parametro a una declaracion de clase: HERENCIA. User es hijo de Document
class User(Document):
    # La classe Document és la base de MongoEngine
    # Cada classe que hereti de Document es convertirà en una col·lecció a MongoDB
    
    #usamos campos xField que son clases propias de MongoEngine
    email = StringField(required=True)  # Camp obligatori, no pot ser null
    first_name = StringField(max_length=50)  # Limitem la longitud màxima a 50 caràcters
    last_name = StringField(max_length=50)
    premium = StringField(choices=SINO)  # Només acceptarà els valors definits a la tupla SINO

#Embedded Document, documento que va dentro de otro. Objeto JSON que va dentro de un doc
class Comment(EmbeddedDocument):
    # Els EmbeddedDocument no creen una col·lecció separada
    # S'emmagatzemen dins del document principal com a subdocuments
    content = StringField()
    name = StringField(max_length=120)

class Post(Document):
    # Classe principal per posts que permet herència
    title = StringField(max_length=120, required=True)
    #referencia. Permite referenciar el objectId de otra collection. En Python se accedera como si fuese un objeto de la clase
    author = ReferenceField(User, reverse_delete_rule=CASCADE)  # CASCADE farà que s'eliminin els posts quan s'elimini l'usuari
    tags = ListField(StringField(max_length=30))  # Array de strings, cada tag màxim 30 caràcters
    comments = ListField(EmbeddedDocumentField(Comment))  # Array de documents Comment incrustats

    #permite clases heredadas
    meta = {'allow_inheritance': True}  # Habilita l'herència per aquesta classe

#Clases heredadas: Van dentro de la misma collection que el padre, con un identificador especial
class TextPost(Post):
    # Hereta tots els camps de Post i afegeix content
    content = StringField()

class ImagePost(Post):
    # Post específic per imatges
    image_path = StringField()

class LinkPost(Post):
    # Post específic per enllaços
    link_url = StringField()

print(1)

#crear INSERT
#recordad que en python los constructores funcionan como en C#. Es decir que puedes definir los campos a mano)
# Creem instàncies dels models i les guardem a la base de dades
ross = User(email='ross@example.com', first_name='Ross', last_name='Lawley', premium='Si')
ross.save()  # El mètode save() insereix el document a MongoDB
marc = User(email='marc@example.com', first_name='Marc', last_name='Albareda', premium='No')
marc.save()

# Creem un TextPost i l'associem a l'usuari ross
post1 = TextPost(title='Fun with MongoEngine', author=ross)
post1.content = 'Took a look at MongoEngine today, looks pretty cool.'
post1.tags = ['mongodb', 'mongoengine']
post1.save()


comment1 = Comment(content='David Feliciano es un tremendo rapado', name='Chen^2')
post1.comments.append(comment1)

#No es necesario hacer un save de comment1 porque, al ser un embedded document, solo puede existir dentro de otro, y por tanto se guardara cuando se guarde el grande
post1.save()


#update
post1.content = 'otro content'
post1.save()  # save() també s'utilitza per actualitzar documents existents

#Fijaos que noe stoy creando los posts como Post, sino como sus clases heredadas
# Creem diferents tipus de posts utilitzant les classes específiques
post2 = LinkPost(title='MongoEngine Documentation', author=ross)
post2.link_url = 'http://docs.mongoengine.com/'
post2.tags = ['mongoengine']
post2.save()

post3 = ImagePost(title='fotos de gatos', author=marc)
post3.image_path = 'http://docs.mongoengine.com/gatete.png'
post3.tags = ['gatetes']
post3.save()

#borrar
post2.delete()  # Elimina el document de la base de dades

print(2)

print("ejemplo para carlos")
pprint(post3.author.first_name)  # Podem accedir als camps de documents referenciats directament
#buscar
#NombreDelaClase.objects es un campo estatico que te deveuvlve una lista con todos los documentos de esa colleciton
# objects és un QuerySet que permet fer consultes a la base de dades
for post in Post.objects:  # Recupera tots els posts independentment del tipus
    print(post.title)

#buscar en clase heredada. Automaticamente te cogera los que son TextPost, ignorando los otros
for post in TextPost.objects:  # Només recupera els posts de tipus TextPost
    print(post.content)

print ("\n\n\n Busqueda")

#coges todos los posts
for post in Post.objects:
    ##imprime el titulo y una barra separadora
    print(post.title)
    print('=' * len(post.title))

    #si (isinstance es como el instanceof) el post es un textpost
    # Utilitzem isinstance per determinar el tipus específic de post
    if isinstance(post, TextPost):
        print("texto: ",post.content)

    #si el post es un linkpost
    if isinstance(post, LinkPost):
        print('Link: ',post.link_url)

print ("\n\n\n Busqueda con parametros ")

#busqueda con parametros de busqueda
# Podem filtrar els documents utilitzant diferents criteris
for post in Post.objects(tags='mongodb'):  # Filtra posts per tag
    print(post.title)

for post in Post.objects(title='fotos de gatos'):  # Filtra per títol exacte
    print(post.author.first_name)

#contar
# count() retorna el número de documents que compleixen els criteris de cerca
num_posts = Post.objects(tags='mongodb').count()
print('Found '+str(num_posts)+' posts with tag "mongodb"')

#buscar y modificar
# Recuperem el primer usuari que compleix els criteris ([0] agafa el primer resultat)
usuario = User.objects(first_name='Ross')[0]
usuario.first_name='Chen'  # Modifiquem el camp
usuario.last_name='Chen'  # Modifiquem el camp
usuario.premium='No'
usuario.save()  # Guardem els canvis

# Iterem sobre tots els usuaris per veure els canvis
for user in User.objects:
    pprint(user.first_name)