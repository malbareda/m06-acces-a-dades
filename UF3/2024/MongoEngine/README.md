# MongoEngine - ODM per MongoDB en Python

## Introducció
MongoEngine és un Object-Document Mapper (ODM) per MongoDB i Python. Permet definir esquemes i models per les dades, i interactuar amb MongoDB d'una manera més orientada a objectes.

## Connexió a MongoDB
```python
from mongoengine import *
db = connect('example')  # Connecta a la base de dades 'example'
```

## Definició de Models

### Document Base
La classe `Document` és la base de MongoEngine. Cada classe que hereti de `Document` es convertirà en una col·lecció a MongoDB.

```python
class User(Document):
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    premium = StringField(choices=SINO)
```

### Tipus de Camps (Fields)
MongoEngine ofereix diversos tipus de camps:

- **StringField**: Per text
  - Opcions: `required=True`, `max_length=X`
  - Exemple: `email = StringField(required=True)`

- **ReferenceField**: Per referències a altres documents
  - Exemple: `author = ReferenceField(User, reverse_delete_rule=CASCADE)`

- **ListField**: Per arrays
  - Exemple: `tags = ListField(StringField(max_length=30))`

- **EmbeddedDocumentField**: Per documents incrustats
  - Exemple: `comments = ListField(EmbeddedDocumentField(Comment))`

### Documents Incrustats (Embedded Documents)
Els documents incrustats són documents que existeixen dins d'altres documents, no en una col·lecció separada.

```python
class Comment(EmbeddedDocument):
    content = StringField()
    name = StringField(max_length=120)
```

### Herència de Documents
MongoEngine permet l'herència de documents. Els documents heretats es guarden en la mateixa col·lecció que el pare.

```python
class Post(Document):
    title = StringField(max_length=120, required=True)
    author = ReferenceField(User)
    meta = {'allow_inheritance': True}  # Habilita l'herència

class TextPost(Post):
    content = StringField()

class ImagePost(Post):
    image_path = StringField()
```

## Operacions CRUD

### Create (Crear)
```python
# Creació d'un usuari
user = User(email='ross@example.com', first_name='Ross')
user.save()

# Creació d'un post
post = TextPost(title='Títol', author=user)
post.content = 'Contingut'
post.save()
```

### Read (Llegir)
```python
# Obtenir tots els documents
for post in Post.objects:
    print(post.title)

# Filtrar documents
posts = Post.objects(tags='mongodb')
user = User.objects(first_name='Ross')[0]

# Comptar documents
num_posts = Post.objects(tags='mongodb').count()
```

### Update (Actualitzar)
```python
# Actualitzar un document
post.content = 'Nou contingut'
post.save()

# Actualitzar després d'una cerca
usuario = User.objects(first_name='Ross')[0]
usuario.first_name = 'raul'
usuario.save()
```

### Delete (Eliminar)
```python
post.delete()  # Elimina el document
```

## Característiques Avançades

### Validació de Dades
MongoEngine permet definir restriccions als camps:
- Camps requerits: `required=True`
- Longitud màxima: `max_length=X`
- Valors permesos: `choices=SINO`

### Relacions entre Documents
- **Referències**: Utilitzant `ReferenceField`
- **Eliminació en cascada**: Utilitzant `reverse_delete_rule=CASCADE`
- **Documents incrustats**: Utilitzant `EmbeddedDocumentField`

### Tipus de Consultes
1. **Consultes bàsiques**:
   ```python
   Post.objects(tags='mongodb')
   Post.objects(title='fotos de gatos')
   ```

2. **Consultes per tipus específic**:
   ```python
   TextPost.objects  # Només posts de text
   ```

3. **Verificació de tipus**:
   ```python
   if isinstance(post, TextPost):
       print(post.content)
   ```

## Bones Pràctiques
1. Utilitzar `EmbeddedDocument` per dades que sempre van juntes amb el document principal
2. Utilitzar `ReferenceField` per relacions entre col·leccions independents
3. Habilitar l'herència només quan sigui necessari amb `meta = {'allow_inheritance': True}`
4. Definir restriccions als camps per mantenir la integritat de les dades
5. Utilitzar els tipus de camps adequats per cada tipus de dada