from datetime import datetime
from mongoengine import *
from pprint import pprint

# Importem mongoengine que és un ODM (Object-Document Mapper) per MongoDB
# pprint s'utilitza per imprimir de forma més llegible les estructures de dades

##nos conectamos a la base de datos example
db = connect('example')
##borra la bd
db.drop_database('example')

print(0)

#Choice, algo tiene que estar en estos valores. Funciona como un enum
SINO = ('Si','No')
# Definim una tupla amb valors possibles, similar a un enum
# Això s'utilitzarà per restringir els valors possibles en alguns camps


#Embedded Document, documento que va dentro de otro. Objeto JSON que va dentro de un doc
class Frase(EmbeddedDocument):
    # Els EmbeddedDocument no creen una col·lecció separada
    # S'emmagatzemen dins del document principal com a subdocuments
    frase = StringField()
    fecha = DateTimeField()
    racista = BooleanField()
    alumnoMentado = ReferenceField("Alumno", reverse_delete_rule=DO_NOTHING)
    
    def clean(self):
        if self.fecha is None:
            self.fecha = datetime.now()
    
#Document: Documento base. Creara su propia collection.
##pasar parametro a una declaracion de clase: HERENCIA. User es hijo de Document
class Alumno(Document):
    # La classe Document és la base de MongoEngine
    # Cada classe que hereti de Document es convertirà en una col·lecció a MongoDB
    
    #usamos campos xField que son clases propias de MongoEngine
    nombre = StringField(max_length=50)  # Limitem la longitud màxima a 50 caràcters
    frases = ListField(EmbeddedDocumentField(Frase))
    
    def clean(self):
        # Validate that only published essays have a `pub_date`
        if 'Feliciano' in self.nombre:
                raise ValidationError("Frases del Feliciano no")

print(1)

#crear INSERT
#recordad que en python los constructores funcionan como en C#. Es decir que puedes definir los campos a mano)
# Creem instàncies dels models i les guardem a la base de dades
chen = Alumno(nombre="Chen Chen")
fraseChen = Frase(frase = "Bombardeen Moviles", fecha = None, racista=False)

miquel = Alumno(nombre="Miquel Avellaneda")
fraseMiquel1 = Frase(frase = "Marruecos ha explotado", fecha = None, racista=True)
fraseMiquel2 = Frase(frase = "Caaabroooon!", fecha = None, racista=False)

chen.frases.append(fraseChen)

miquel.frases.append(fraseMiquel1)
miquel.frases.append(fraseMiquel2)


fel = Alumno(nombre="David")
fraseFel = Frase(frase = "El chen me ha dicho que juegan al borracho volador", fecha = None, racista = False)
fraseFel.alumnoMentado = chen
fel.frases.append(fraseFel)


chen.save()
miquel.save()
fel.save()

# Iterem sobre tots els usuaris per veure els canvis
for user in Alumno.objects:
    pprint(user.nombre)