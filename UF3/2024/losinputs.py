a = input()
print(a)

separadoPorEspacios = a.split(" ")
print(separadoPorEspacios)

n = []
encomillas = False
fusion = ""
for i in separadoPorEspacios:
    #print("cc",i)
    if not encomillas and i[0] == '"':
        encomillas = True
        fusion = fusion + i + " "
    elif i[-1] == '"':
        encomillas = False
        fusion = fusion + i + " "
        n.append(fusion)
        print("la fusion es",fusion)
        fusion=""
    elif encomillas:
        fusion = fusion + i
    
    else:
        n.append(i)
        
print(n)
        
        
        
#una tupla es como una lista, PERO
# Es inmutable
# Es ordenada estricta
# Permite valores duplicados
# Permite valores de tipos distintos

a = ("hola", 2, "Chen", ["Chen","Chen"], 2.4, True)
print(a)

# El acceso a tuplas es como a las listas
print(a[0])
print(a[-1])
print(a[3:])

b = list(a)
b[0]="adios"
a = tuple(b)
print(a)

print("-----------------")
for i in a:
    print(i)

#desempacar tupla, sorprendemente funciona    
(uno, dos, tres, cuatro, cinco, seis) = a
print(tres)