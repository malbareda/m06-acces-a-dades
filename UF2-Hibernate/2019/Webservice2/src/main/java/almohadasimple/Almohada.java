package almohadasimple;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//anotacions

@Entity
@Table(name = "almohada")
public class Almohada {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_almohada")
	private int id;
	
	@Column(name="tamano")
	private String tamano;
	
	@Column(name="tipo")
	private String tipo;
	
	@Column(name="dibujo")
	private String dibujo;
	
	@Column(name="relleno")
	private boolean relleno;
	
	@Column(name="precio")
	private float precio;
	
	@Column(name="sabor")
	private String sabor;
	
	@ManyToMany(mappedBy = "almohadas")
	private Set<Color> colores;
	
	public Set<Color> getColores() {
		return colores;
	}

	public void setColores(Set<Color> colores) {
		this.colores = colores;
	}

	public Almohada() {
		super();
		this.tamano="XL";
		this.tipo="Dakimakura";
		this.dibujo="bowser";
		this.relleno=true;
		this.sabor="limon";
		this.fechaAdquisicion = new Date();
	}
	
	public Almohada(String tipo, String dibujo) {
		super();
		this.tamano="XL";
		this.tipo=tipo;
		this.dibujo=dibujo;
		this.relleno=true;
		this.sabor="limon";
		this.fechaAdquisicion = new Date();
	}

	public Almohada(String tamano, String tipo, String dibujo, boolean relleno, String color, float precio,
			String sabor, Date fechaAdquisicion) {
		super();
		this.tamano = tamano;
		this.tipo = tipo;
		this.dibujo = dibujo;
		this.relleno = relleno;
		this.precio = precio;
		this.sabor = sabor;
		this.fechaAdquisicion = fechaAdquisicion;
		
	}
	
	

	@Column(name="fecha_adquisicion")
	private Date fechaAdquisicion;
	public int getId() {
		return id;
	}
	
	

	public void setId(int id) {
		this.id = id;
	}
	public String getTamano() {
		return tamano;
	}
	public void setTamano(String tamano) {
		this.tamano = tamano;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDibujo() {
		return dibujo;
	}
	public void setDibujo(String dibujo) {
		this.dibujo = dibujo;
	}
	public boolean isRelleno() {
		return relleno;
	}
	public void setRelleno(boolean relleno) {
		this.relleno = relleno;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public String getSabor() {
		return sabor;
	}
	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	public Date getFechaAdquisicion() {
		return fechaAdquisicion;
	}
	public void setFechaAdquisicion(Date fechaAdquisicion) {
		this.fechaAdquisicion = fechaAdquisicion;
	}
	@Override
	public String toString() {
		return "Almohada [id=" + id + ", tama�o=" + tamano + ", tipo=" + tipo + ", dibujo=" + dibujo + ", relleno="
				+ relleno + "  precio=" + precio + ", sabor=" + sabor + ", fechaAdquisicion="
				+ fechaAdquisicion + "]";
	}
	
	

}
