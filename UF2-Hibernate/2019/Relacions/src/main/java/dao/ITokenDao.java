package dao;

import Llops.Token;
import Llops.User;

public interface ITokenDao extends IGenericDao<Token,Integer>{

	public void GenerateToken(User user);
	
	//devolvera null si no hay usuario
	public User checkToken(String token);
	
	public void deleteToken(String token);
	
	public void refreshTokens();
	
}
