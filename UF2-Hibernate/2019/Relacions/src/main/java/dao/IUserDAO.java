package dao;

import java.util.List;

import Llops.User;
import conrelaciones.Modulo;

public interface IUserDAO extends IGenericDao<User, String> {
	
	void saveOrUpdate(User m);

	User get(String id);

	List<User> list();

	void delete(String id);
	
	

}
