package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import Llops.User;

public class UserDAO extends GenericDao<User,String> implements IUserDAO{

	boolean login(String userName, String pw) {
		Session session = sessionFactory.getCurrentSession();
		
		try {
			session.beginTransaction();
			/* el codigo va aqui 
			 */
			boolean b;
			
			User u = this.get(userName);
			if(u==null) {
				b = false;
			}else {
				//if(u.getPassword().equals(pw)) return true; else return false;
				b =  (u.getPassword().equals(pw) ? true : false);	
			}
			session.getTransaction().commit(); 
			return b;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;

		}
	}
	
}
