package dao;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import Llops.Token;
import Llops.User;

public class TokenDao extends GenericDao<Token, Integer> implements ITokenDao{

	@Override
	public void GenerateToken(User user) {
		// TODO Auto-generated method stub
		Random r = new Random();
		long l = r.nextLong();
		System.out.println("el token es "+l);
		while (true) {
			if(sessionFactory.getCurrentSession().createQuery("FROM Token t WHERE t.token = "+l+"").list().isEmpty()) {
				this.saveOrUpdate( new Token(l+"",new Date(), new Date(new Date().getTime()+3600*1000)));
				break;
			}
		}
		
		
		/*
		 * 		Random r = new Random();
		long l = r.nextLong();
		System.out.println("el token es "+l);
		while (true) {
			List<Object> list = sessionFactory.getCurrentSession().createQuery("FROM Token t WHERE t.token = "+l+"").list();
			if(list.isEmpty()) {
				Token t = new Token(l+"",new Date(), new Date(new Date().getTime()+3600*1000));
				this.saveOrUpdate( new Token(l+"",new Date(), new Date(new Date().getTime()+3600*1000)));
				break;
			}
		}
		 */
		
		
		
		
		
	}

	@Override
	public User checkToken(String token) {
		// TODO Auto-generated method stub
		List<Object> list = sessionFactory.getCurrentSession().createQuery("FROM Token t WHERE t.token = "+token).list();
		if(list.isEmpty()) return null;
		Token t = (Token) list.get(0);
		return t.getUser();
	}

	@Override
	public void deleteToken(String token) {
		// TODO Auto-generated method stub
		List<Token> list = this.list();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Token t = (Token) iterator.next();
			if(t.getToken().equals(token)) {
				this.delete(t.getId());
				break;
			}
		}
		
	}

	@Override
	public void refreshTokens() {
		Date now = new Date();
		List<Token> list = this.list();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Token token = (Token) iterator.next();
			if(token.getTimestamp().getTime()+(token.getTtl().getTime()*1000)<now.getTime()) {
				this.delete(token.getId());
			}
			
		}
		
	}


	
}
