package Llops;

import java.util.Date;

import javax.persistence.*;

import conrelaciones.Profesor;

@Entity
@Table(name = "Token")
public class Token {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id", updatable = false, nullable = false)
	private int id;

	@Column(name = "token")
	String token;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "timestamp")
	Date timestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TTL")
	Date ttl;

	@JoinColumn(name = "user", nullable = false)
	@OneToOne(cascade = CascadeType.ALL)
	private User user;

	public Token() {

	}

	public Token(String token, Date timestamp, Date ttl) {
		super();
		this.token = token;
		this.timestamp = timestamp;
		this.ttl = ttl;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Date getTtl() {
		return ttl;
	}

	public void setTtl(Date ttl) {
		this.ttl = ttl;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User u) {
		this.user = u;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Token [id=" + id + ", token=" + token + ", timestamp=" + timestamp + ", ttl=" + ttl + "]";
	}

}
