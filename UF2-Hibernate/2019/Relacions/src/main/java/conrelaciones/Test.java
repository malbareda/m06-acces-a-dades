package conrelaciones;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class Test {
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		/// sessionFactory es un Singleton
		if (sessionFactory == null) {

			// exception handling omitted for brevityaa

			/// pilla la configuracion del fichero de configuraci�n
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static void main(String[] args) {
		
		try {
			//una sesion es una conexi�n a la BD. Pueden haber varias sesiones simultaneas de diferentes usuarios.
			//Lo l�gico es que solo haya una en el programa.
			session = getSessionFactory().openSession();
			
			// Una transaccion es at�mica. Solo puede haber una y hasta que no se cierra no puede ejecutarse otra, ni siquiera de otro usuario.
			// Una sesi�n contendr� m�ltiples transacciones habitualmente
			session.beginTransaction();
			
			Curso c = session.get(Curso.class, 1);
			System.out.println(c);
			
			System.out.println(c.getTutor().getApe2());
			
			System.out.println(c);
			/*
			for(Modulo m : c.getModulos()) {
				System.out.println(m.getNombre()+" "+m.getCurso().getTutor().getApe2());
			}*/
			
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
			
	}

}
