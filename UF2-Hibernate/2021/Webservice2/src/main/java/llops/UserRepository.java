package llops;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import app.Profesor;


//Los repositorios se crean solos.

//hay dos tipos de repositorio, 
//CrudRepository: Create Read(Retrieve) Update Delete
//JPARepository

public interface UserRepository extends CrudRepository<Usuari, String> {

	  List<Usuari> findByAliasOrderByPercentatgevictoriesDes(String alias);
	
	
	

}
