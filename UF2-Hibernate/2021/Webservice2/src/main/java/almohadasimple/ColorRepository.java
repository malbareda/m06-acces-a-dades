package almohadasimple;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import app.Profesor;
import app.User;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete


public interface ColorRepository extends CrudRepository<Color, Integer> {
	  //List<Almohada> findByTipoIgnoreCaseOrderByDibujoAsc(String tipo);
}
