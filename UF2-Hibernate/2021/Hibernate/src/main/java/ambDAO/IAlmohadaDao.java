package ambDAO;

import model.Almohada;

public interface IAlmohadaDao extends IGenericDao<Almohada, Integer> {
	
	String getSaborFromID(Integer id);
	
	void subemeElPrecioUnDiezPorciento(Integer id);

}
