# Apunts de Spring Framework

## 1. Configuració de Spring Tools Suite i Creació d'un Projecte

### 1.1 Instal·lació de Spring Tools Suite (STS)
- Descarregar Spring Tools Suite des de la pàgina oficial de Spring
- És recomanable utilitzar la versió que ve integrada amb Eclipse
- Assegurar-se de tenir instal·lat Java JDK 17 o superior

### 1.2 Crear un nou Spring Starter Project
1. File → New → Spring Starter Project
2. Configurar els camps bàsics:
   - Name: nom del projecte
   - Type: Maven
   - Packaging: Jar
   - Java Version: 17 o superior
   - Group: com.example
   - Artifact: nom del projecte
   - Package: com.example.demo

3. Seleccionar les dependències necessàries:
   - Spring Web
   - Spring Data JPA
   - MySQL Driver (si utilitzem MySQL)
   - Spring Boot DevTools (opcional, però útil pel desenvolupament)

## 2. Configuració de application.properties

El fitxer application.properties és fonamental per configurar la connexió a la base de dades i altres aspectes de l'aplicació. Basant-nos en l'exemple:

```properties
spring.application.name=bocatasExperience
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/bocatas?allowPublicKeyRetrieval=true&useSSL=false
spring.datasource.username=root
spring.datasource.password=super3
```
### 2.2 Configuració avançada de la base de dades

#### URL de la base de dades
La URL de connexió pot incloure diversos paràmetres:

```properties
spring.datasource.url=jdbc:mysql://localhost:3306/bocatas?allowPublicKeyRetrieval=true&useSSL=false&serverTimezone=UTC&characterEncoding=UTF-8
```

Paràmetres comuns:
- `useSSL=false`: Desactiva SSL per desenvolupament local
- `serverTimezone=UTC`: Especifica la zona horària
- `characterEncoding=UTF-8`: Codificació de caràcters
- `allowPublicKeyRetrieval=true`: Permet recuperar la clau pública del servidor
- `createDatabaseIfNotExist=true`: Crea la base de dades si no existeix

#### Configuració addicional
```properties
# Configuració del pool de connexions
spring.datasource.hikari.maximum-pool-size=10
spring.datasource.hikari.minimum-idle=5
spring.datasource.hikari.idle-timeout=300000

# Configuració de JPA
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQLDialect

# Estratègia de noms de taules i columnes
spring.jpa.hibernate.naming.physical-strategy=org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl
spring.jpa.hibernate.naming.implicit-strategy=org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl
```

#### Explicació dels paràmetres de naming strategy
- `physical-strategy`: Determina com es tradueixen els noms de Java a la base de dades
  - `PhysicalNamingStrategyStandardImpl`: Manté els noms exactament igual
  - `CamelCaseToUnderscoresNamingStrategy`: Converteix camelCase a under_score   (Aquesta es la que s'aplica per defecte)
- `implicit-strategy`: Determina com es generen els noms que no estan explícitament mapejats

### 2.1 Explicació dels paràmetres
- `spring.application.name`: Nom de l'aplicació
- `spring.jpa.hibernate.ddl-auto`: Configura com Hibernate gestionarà l'esquema de la base de dades
  - `update`: actualitza l'esquema si cal
  - `create`: crea les taules cada vegada
  - `none`: no fa canvis a l'esquema
- `spring.datasource.url`: URL de connexió a la base de dades
- `spring.datasource.username`: Usuari de la base de dades
- `spring.datasource.password`: Contrasenya de la base de dades

## 3. Creació de Repositoris

Els repositoris en Spring Data JPA són interfícies que estenen CrudRepository i proporcionen mètodes per accedir a la base de dades.

### 3.1 Estructura Bàsica
```java
public interface BocataRepository extends CrudRepository<Bocata,Integer> {
    List<Bocata> findByNombre(String nombre);
    List<Bocata> findByPrecioLessThan(double max);
    List<Bocata> findAllByOrderByPrecioAsc();
}
```

### 3.2 Mètodes predefinits de CrudRepository
- `save(T entity)`: Guarda una entitat
- `findById(ID id)`: Busca per ID
- `findAll()`: Retorna totes les entitats
- `deleteById(ID id)`: Elimina per ID
- `delete(T entity)`: Elimina una entitat

### 3.3 Derived Query Methods
Spring Data JPA permet crear mètodes de consulta automàticament basant-se en el nom del mètode:
- `findBy[NomCamp]`: Busca per un camp
- `findBy[NomCamp]LessThan`: Busca valors menors que
- `findAllByOrderBy[NomCamp]Asc/Desc`: Ordena els resultats

Per una guia completa i detallada sobre Derived Query Methods, incloent tots els operadors possibles i exemples pràctics, pots consultar [la nostra guia específica sobre Derived Query Methods](UF2-Hibernate/2024/bocatasExperience/spring-derived-queries.md).

## 4. Controllers

Els controllers són la part més important de l'API REST, ja que gestionen les peticions HTTP.

### 4.1 Anotacions bàsiques

```java
@Controller
@RequestMapping(path = "/ies/bocataexperience")
public class BocataController {
    @Autowired
    private BocataRepository repository;
}
```

- `@Controller`: Indica que és un controller de Spring
- `@RequestMapping`: Defineix la ruta base del controller
- `@Autowired`: Injecta dependències automàticament

### 4.1.1 Injecció de Dependències amb @Autowired

@Autowired és un mecanisme fonamental en Spring per la injecció de dependències. Quan marquem un camp amb @Autowired, Spring s'encarrega d'injectar automàticament una instància de la dependència adequada.

#### Com funciona amb els Repositoris:
```java
@Controller
public class BocataController {
    @Autowired
    private BocataRepository bocataRepository;
    
    @Autowired
    private ProfesorRepository profesorRepository;
}
```

Quan Spring crea una instància del controller:
1. Detecta els camps marcats amb @Autowired
2. Busca beans del tipus adequat (en aquest cas, implementacions de BocataRepository i ProfesorRepository)
3. Injecta automàticament aquestes implementacions

#### Per què funciona amb Repositoris?
- Spring Data JPA crea automàticament implementacions dels repositoris
- Aquestes implementacions es registren com a beans de Spring
- Quan usem @Autowired, Spring troba aquests beans i els injecta
- No cal crear implementacions manuals dels repositoris

### 4.2 Mètodes del Controller

#### GET Bàsic
```java
@GetMapping(path = "/hola")
public @ResponseBody String test() {
    return "Hola";
}
```
- `@GetMapping`: Defineix una ruta per peticions GET
- `@ResponseBody`: Indica que el retorn és el cos de la resposta

#### Afegir nou registre amb paràmetres
```java
@GetMapping(path = "/add")
public @ResponseBody String addNewBocata(
    @RequestParam String nombre, 
    @RequestParam double precio, 
    @RequestParam String receta) {
    Bocata b = new Bocata();
    b.setNombre(nombre);
    b.setPrecio(precio);
    b.setReceta(receta);
    repository.save(b);
    return "<h2>Todo ha salido a pedir de Milhouse</h2>";
}
```
- `@RequestParam`: Captura paràmetres de la URL (?nombre=valor&precio=5)

#### Buscar per path variable
```java
@GetMapping(path = "/nombre/{nombre}")
public @ResponseBody Iterable<Bocata> getBocatasConNombre(@PathVariable String nombre) {
    return repository.findByNombre(nombre);
}
```
- `@PathVariable`: Captura variables de la ruta URL

#### Consultes amb relacions
```java
@GetMapping(path= "/dar/{bocata}/{profe}")
public @ResponseBody Profesor darBocata(@PathVariable int bocata, @PathVariable int profe) {
    Bocata b = brepository.findById(bocata).get();
    Profesor p = prepository.findById(profe).get();
    p.getBocatas().add(b);
    b.setProfesor(p);
    prepository.save(p);
    brepository.save(b);
    return p;
}
```

### 4.3 Bones pràctiques en Controllers

1. **Organització de rutes**:
   - Utilitzar una ruta base significativa amb `@RequestMapping`
   - Mantenir una jerarquia lògica en les subrutes

2. **Gestió d'errors**:
   - Implementar try-catch per gestionar excepcions
   - Retornar codis HTTP adequats

3. **Nomenclatura**:
   - Utilitzar noms descriptius per als mètodes
   - Seguir convencions REST:
     - GET: per obtenir dades
     - POST: per crear
     - PUT: per actualitzar
     - DELETE: per eliminar

4. **Documentació**:
   - Comentar els endpoints complexos
   - Especificar els paràmetres necessaris
   - Documentar els possibles codis de retorn

### 4.4 Tipus de paràmetres en Controllers

1. **@RequestParam**:
   - Per paràmetres de query string
   - Exemple: `/add?nombre=Bocata&precio=5`
   - Poden ser opcionals: `@RequestParam(required = false)`

2. **@PathVariable**:
   - Per variables en la ruta
   - Exemple: `/nombre/{nombre}`
   - Més adequat per identificadors i valors únics

3. **@RequestBody**:
   - Per rebre objectes JSON en el cos de la petició
   - Útil per POST i PUT

### 4.5 Respostes del Controller

1. **@ResponseBody**:
   - Converteix automàticament a JSON
   - Útil per retornar objectes
   - Exemples:
     ```java
     // Retornar un objecte
     @GetMapping("/bocata/{id}")
     public @ResponseBody Bocata getBocata(@PathVariable int id) {
         return repository.findById(id).orElse(null);
     }

     // Retornar una llista
     @GetMapping("/bocatas/caros")
     public @ResponseBody List<Bocata> getBocatasCaros() {
         return repository.findByPrecioGreaterThan(10.0);
     }

     ```

2. **ResponseEntity<T>**:
   - Permet més control sobre la resposta
   - Permet especificar codi HTTP
   - Permet afegir headers
   - Exemples:
     ```java
     // Resposta bàsica amb control d'errors
     @GetMapping("/bocata/{id}")
     public ResponseEntity<Bocata> getBocata(@PathVariable int id) {
         Optional<Bocata> bocata = repository.findById(id);
         return bocata.map(ResponseEntity::ok)
                     .orElse(ResponseEntity.notFound().build());
     }

     // Resposta amb headers personalitzats
     @GetMapping("/bocata/{id}/download")
     public ResponseEntity<Bocata> downloadBocata(@PathVariable int id) {
         Bocata bocata = repository.findById(id).orElse(null);
         if (bocata == null) {
             return ResponseEntity.notFound().build();
         }
         return ResponseEntity.ok()
                 .header("Content-Disposition", "attachment; filename=bocata.json")
                 .header("Cache-Control", "no-cache")
                 .body(bocata);
     }

     // Resposta amb diferents codis HTTP
     @GetMapping("/bocata/{id}/verify")
     public ResponseEntity<?> verifyBocata(@PathVariable int id) {
         Bocata bocata = repository.findById(id).orElse(null);
         if (bocata == null) {
             return ResponseEntity.notFound().build();
         }
         if (bocata.getPrecio() <= 0) {
             return ResponseEntity.badRequest()
                     .body("El precio no puede ser 0 o negativo");
         }
         if (!bocata.isDisponible()) {
             return ResponseEntity.status(HttpStatus.CONFLICT)
                     .body("El bocata no está disponible");
         }
         return ResponseEntity.ok(bocata);
     }
     ```