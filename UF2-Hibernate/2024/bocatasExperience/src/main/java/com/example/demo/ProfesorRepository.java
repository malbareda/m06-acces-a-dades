package com.example.demo;

import org.springframework.data.repository.CrudRepository;

public interface ProfesorRepository extends CrudRepository<Profesor,Integer>{

}
