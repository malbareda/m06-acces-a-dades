package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.*;

@Entity
@Table
public class Alumno {
	
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	
    @Column(name="nombre")
    private String nombre;

	@Enumerated(EnumType.STRING)
	@Column(name = "curso")
	Curso curso;
	
	@ManyToMany(mappedBy = "alumnos")
	private Set<Profesor> profesores = new HashSet<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Set<Profesor> getProfesores() {
		return profesores;
	}

	public void setProfesores(Set<Profesor> profesores) {
		this.profesores = profesores;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Alumno [id=" + id + ", nombre=" + nombre + ", curso=" + curso + ", profesores=" + profesores + "]";
	}

	public Alumno() {
		super();
	}
	
	
}
