package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table
public class Bocata {
	
	 	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name="Id",updatable = false, nullable = false)
	    private int id;
		
	    //columnDefinition, como quedaria esta columna definida en SQL puro
	    @Column(name="nombre", columnDefinition = "VARCHAR(100) not null")
	    private String nombre;
	    
	    @Column
	    private double precio;
	    
	    //LOB: Large Object. Eso significa que EN EL CASO DE MYSQL, me lo guardara como Text, y no como Varchar. En general bsuca la variable para almacenar objetos grandes, que siempre existe.
	    @Lob
	    @Column
	    private String receta;
	    
	    @ManyToOne
	    @JsonBackReference //eso significa que NO quieres que en el JSON de los bocatas, salga su profesor, cortando el bucle infinito
	    private Profesor profesor;
	    
	    @JoinColumn(name="bocataAlumno", nullable=true)
	    @OneToOne
	    private Alumno alumno;

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public double getPrecio() {
			return precio;
		}

		public void setPrecio(double precio) {
			this.precio = precio;
		}

		public String getReceta() {
			return receta;
		}

		public void setReceta(String receta) {
			this.receta = receta;
		}

		public Profesor getProfesor() {
			return profesor;
		}

		public void setProfesor(Profesor profesor) {
			this.profesor = profesor;
		}

		public Alumno getAlumno() {
			return alumno;
		}

		public void setAlumno(Alumno alumno) {
			this.alumno = alumno;
		}

		public int getId() {
			return id;
		}

		@Override
		public String toString() {
			return "Bocata [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", receta=" + receta
					+ ", profesor=" + profesor + ", alumno=" + alumno + "]";
		}

		public Bocata() {
			super();
		}
	    
	    
	    

}
