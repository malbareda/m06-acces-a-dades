package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.*;
import jakarta.persistence.GenerationType;

@Entity
@Table
public class Profesor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id",updatable = false, nullable = false)
    private int id;
	
    //columnDefinition, como quedaria esta columna definida en SQL puro
    @Column(name="nombre", columnDefinition = "VARCHAR(100) not null")
    private String nombre;
    
    @Column
    private int edad;
    
    @OneToMany(mappedBy = "profesor")
    @JsonManagedReference //eso significa que quieres que en el JSON del profesor se vean los bocatas
    private Set<Bocata> bocatas = new HashSet<>();
    
    @ManyToMany
    @JoinTable(name = "ProfesorAlumno", joinColumns = {@JoinColumn(name="idProfe")},inverseJoinColumns = {@JoinColumn(name="idAlumno")})
    private Set<Alumno> alumnos = new HashSet<>();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Set<Bocata> getBocatas() {
		return bocatas;
	}

	public void setBocatas(Set<Bocata> bocatas) {
		this.bocatas = bocatas;
	}

	public Set<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(Set<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Profesor [id=" + id + ", nombre=" + nombre + ", edad=" + edad + ", bocatas=" + bocatas + ", alumnos="
				+ alumnos + "]";
	}

	public Profesor() {
		super();
	}
    
    
    
}
