package com.example.demo;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller //eso signfica que es un controller
@RequestMapping(path = "/ies/profe") //esto es el path de la RAIZ de la API
public class ProfesorController {
	@Autowired //creanme
	private ProfesorRepository prepository;
	
	@Autowired //creanme
	private BocataRepository brepository;
	
	@GetMapping(path = "/hola")
	public @ResponseBody String test() {
		return "Hola";
	}
	
	@GetMapping(path = "/add") //esto es si accedo a la url /add
	public @ResponseBody String addNewProfe(@RequestParam String nombre, 
			@RequestParam int edad) {
		Profesor b = new Profesor();
		b.setNombre(nombre);
		b.setEdad(edad);
		prepository.save(b);
		return "<h2>Todo ha salido a pedir de Milhouse</h2>";  //las respuestas de String se consideran respuestas en HTML
		
	}
	
	@GetMapping(path= "/dar/{bocata}/{profe}")
	public @ResponseBody Profesor darBocata(@PathVariable int bocata, @PathVariable int profe) {
		Bocata b = brepository.findById(bocata).get();
		Profesor p = prepository.findById(profe).get();
		p.getBocatas().add(b);
		b.setProfesor(p);
		prepository.save(p);
		brepository.save(b);
		return p;
	}

	@GetMapping(path = "/all")
	public @ResponseBody Iterable<Profesor> getAllUsers() {
		return prepository.findAll();//Toda clase de Java la devuelve como un JSON
	}
	
	
	
	
	
}
