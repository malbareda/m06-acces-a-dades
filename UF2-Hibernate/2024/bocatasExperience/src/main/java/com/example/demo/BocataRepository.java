package com.example.demo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface BocataRepository extends CrudRepository<Bocata,Integer>{
	
	//y ya esta.
	List<Bocata> findByNombre(String nombre);
	List<Bocata> findByPrecioLessThan(double max);
	List<Bocata> findAllByOrderByPrecioAsc();
	
	
}
