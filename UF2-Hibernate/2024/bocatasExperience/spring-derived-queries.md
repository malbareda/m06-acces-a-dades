# Derived Query Methods en Spring Data JPA

## 1. Introducció

Els Derived Query Methods són una característica poderosa de Spring Data JPA que permet crear consultes automàticament basant-se en el nom del mètode. No cal escriure la consulta SQL o JPQL, Spring la genera automàticament.

## 2. Estructura Bàsica

```java
findBy[PropietatEntitat][Condició]
```

Per exemple:
```java
List<Usuari> findByEdat(int edat);
```

## 3. Operadors i Keywords

### 3.1 Comparació Simple
```java
// Igual que
List<Bocata> findByPrecio(double precio);
List<Bocata> findByNombre(String nombre);

// Diferent de
List<Bocata> findByPrecioNot(double precio);

// Nul o No Nul
List<Bocata> findByRecetaIsNull();
List<Bocata> findByRecetaIsNotNull();

// True o False (per booleans)
List<Bocata> findByDisponibleTrue();
List<Bocata> findByDisponibleFalse();
```

### 3.2 Comparacions Numèriques
```java
// Menor que
List<Bocata> findByPrecioLessThan(double precio);

// Menor o igual que
List<Bocata> findByPrecioLessThanEqual(double precio);

// Major que
List<Bocata> findByPrecioGreaterThan(double precio);

// Major o igual que
List<Bocata> findByPrecioGreaterThanEqual(double precio);

// Entre dos valors
List<Bocata> findByPrecioBetween(double minPrecio, double maxPrecio);
```

### 3.3 Comparacions de Strings
```java
// Conté
List<Bocata> findByNombreContaining(String text);

// Comença per
List<Bocata> findByNombreStartingWith(String prefix);

// Acaba en
List<Bocata> findByNombreEndingWith(String sufix);

// Like (amb comodins)
List<Bocata> findByNombreLike(String pattern);

// Ignorar majúscules/minúscules
List<Bocata> findByNombreIgnoreCase(String nombre);
```

### 3.4 Col·leccions
```java
// En una llista de valors
List<Bocata> findByPrecioIn(Collection<Double> precios);

// No en una llista de valors
List<Bocata> findByPrecioNotIn(Collection<Double> precios);
```

### 3.5 Múltiples Condicions
```java
// AND
List<Bocata> findByNombreAndPrecio(String nombre, double precio);

// OR
List<Bocata> findByNombreOrPrecio(String nombre, double precio);

// Multiple AND
List<Bocata> findByNombreAndPrecioAndRecetaIsNotNull(String nombre, double precio);
```

### 3.6 Ordenació
```java
// Ordenar per un camp
List<Bocata> findByPrecioLessThanOrderByPrecioAsc(double precio);
List<Bocata> findByPrecioLessThanOrderByPrecioDesc(double precio);

// Ordenar per múltiples camps
List<Bocata> findByPrecioLessThanOrderByPrecioAscNombreDesc(double precio);
```

## 4. Exemples amb Relacions

### 4.1 Navegació per Relacions
```java
// OneToMany/ManyToOne
List<Bocata> findByProfesor_Nombre(String nombreProfesor);
List<Profesor> findByBocatas_PrecioLessThan(double precio);

// ManyToMany
List<Profesor> findByAlumnos_Curso(Curso curso);
List<Alumno> findByProfesores_EdadGreaterThan(int edad);
```

### 4.2 Consultes Anidades
```java
// Consultes amb relacions múltiples
List<Profesor> findByBocatas_AlumnoNombreContaining(String nombreAlumno);
List<Alumno> findByProfesores_Bocatas_PrecioGreaterThan(double precio);
```

## 5. Paginació i Límits

### 5.1 Limitar Resultats
```java
// Primers N resultats
List<Bocata> findTop5ByOrderByPrecioDesc();
List<Bocata> findFirst10ByNombreContaining(String text);
```

### 5.2 Paginació
```java
// Amb Pageable
Page<Bocata> findByPrecioLessThan(double precio, Pageable pageable);
Slice<Bocata> findByNombreContaining(String text, Pageable pageable);
```

## 6. Consultes Avançades

### 6.1 Consultes amb Funcions
```java
// Funcions d'agregació
int countByPrecioLessThan(double precio);
long countByProfesorNombre(String nombreProfesor);

// Funcions de grup
List<Bocata> findByPrecioGreaterThanEqualAndPrecioLessThanEqual(double minPrecio, double maxPrecio);
```

### 6.2 Consultes amb Distinct
```java
List<String> findDistinctNombreBy();
List<Bocata> findDistinctByProfesorNombre(String nombreProfesor);
```

## 7. Exemples Complets d'Interfície Repository

```java
public interface BocataRepository extends CrudRepository<Bocata, Integer> {
    // Consultes bàsiques
    List<Bocata> findByNombre(String nombre);
    List<Bocata> findByPrecioLessThan(double precio);
    
    // Consultes compostes
    List<Bocata> findByNombreAndPrecioLessThan(String nombre, double precio);
    List<Bocata> findByNombreContainingOrPrecioLessThan(String nombre, double precio);
    
    // Ordenació
    List<Bocata> findAllByOrderByPrecioAsc();
    List<Bocata> findByPrecioLessThanOrderByNombreAscPrecioDesc(double precio);
    
    // Paginació
    Page<Bocata> findByRecetaContaining(String texto, Pageable pageable);
    
    // Consultes amb relacions
    List<Bocata> findByProfesor_NombreAndPrecioLessThan(String nombreProfesor, double precio);
    List<Bocata> findByAlumno_CursoOrderByPrecioAsc(Curso curso);
    
    // Agregacions
    long countByProfesor_Nombre(String nombreProfesor);
    double averagePrecioByProfesor_Id(int profesorId);
}
```

## 8. Bones Pràctiques

1. **Nomenclatura Clara**: 
   - Utilitzar noms descriptius que reflecteixin la consulta
   - Seguir les convencions de nomenclatura de Spring

2. **Optimització**:
   - Evitar consultes massa complexes
   - Utilitzar paginació per grans conjunts de dades
   - Considerar l'ús de projeccions per consultes específiques

3. **Testing**:
   - Crear tests per verificar el comportament de les queries
   - Verificar el rendiment amb diferents volums de dades

4. **Documentació**:
   - Comentar les queries complexes
   - Especificar els paràmetres esperats
   - Documentar possibles casos especials