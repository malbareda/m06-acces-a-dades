# Cascade en Hibernate: Guia Completa

## Introducció

El cascade (o cascada) en Hibernate és un mecanisme que permet propagar automàticament les operacions realitzades sobre una entitat a les seves entitats relacionades. És una eina molt útil per mantenir la integritat de les dades i simplificar el codi.

## Tipus de Cascade

Hibernate suporta diversos tipus de cascade:
- **PERSIST**: Propaga l'operació de persistència
- **REMOVE**: Propaga l'operació d'eliminació
- **MERGE**: Propaga l'operació de fusió
- **REFRESH**: Propaga l'operació de refrescament
- **DETACH**: Propaga l'operació de desvinculació
- **ALL**: Inclou tots els tipus anteriors

En els nostres exemples ens centrarem en PERSIST i REMOVE, que són els més comuns.

## Exemple 1: Cascade PERSIST

### Estructura de les Entitats

```java
@Entity
public class A {
    @OneToMany(mappedBy = "a", cascade=CascadeType.PERSIST)
    private Set<B> b = new HashSet<B>();
}

@Entity
public class B {
    @ManyToOne
    @JoinColumn(name="a_id", nullable=false)
    private A a;
}
```

### Com Funciona

1. Quan tenim una relació OneToMany entre A i B amb `cascade=CascadeType.PERSIST`:
   ```java
   A a1 = new A();
   B b1 = new B();
   B b2 = new B();
   
   // Establim les relacions
   b1.setA(a1);
   b2.setA(a1);
   a1.getB().add(b1);
   a1.getB().add(b2);
   
   // Només cal persistir A
   session.persist(a1);  // B1 i B2 es persisteixen automàticament
   ```

2. Sense el cascade, hauríem de fer:
   ```java
   session.persist(a1);
   session.persist(b1);
   session.persist(b2);
   ```

### Consideracions Importants
- És important mantenir la coherència en les relacions bidireccionals
- El cascade PERSIST és útil quan les entitats fills no tenen sentit sense l'entitat pare
- Cal tenir en compte que el cascade PERSIST pot provocar efectes en cadena si no es gestiona adequadament

## Exemple 2: Cascade REMOVE

### Estructura de les Entitats

```java
@Entity
public class D {
    @ManyToMany(cascade = CascadeType.REMOVE)
    private Set<C> c = new HashSet<C>();
}

@Entity
public class C {
    @ManyToMany(mappedBy = "c")
    private Set<D> d = new HashSet<D>();
}
```

### Com Funciona

1. Quan eliminem una entitat D amb `cascade = CascadeType.REMOVE`:
   ```java
   // Creem i relacionem les entitats
   D d1 = new D();
   C c1 = new C();
   C c2 = new C();
   
   d1.getC().add(c1);
   d1.getC().add(c2);
   c1.getD().add(d1);
   c2.getD().add(d1);
   
   // Persistim les entitats
   session.persist(c1);
   session.persist(c2);
   
   // Més tard...
   session.remove(d1);  // C1 i C2 s'eliminen automàticament
   ```

2. Sense el cascade, hauríem de:
   - Eliminar primer totes les relacions
   - Eliminar manualment cada entitat C
   - Finalment eliminar D

### Consideracions Importants
- El cascade REMOVE s'ha d'utilitzar amb precaució en relacions ManyToMany
- És especialment útil quan volem garantir que no quedin entitats "òrfenes"
- Cal tenir en compte l'impacte en el rendiment quan s'eliminen moltes entitats en cascada

## Bones Pràctiques

1. **Disseny de Relacions**
   - Identifica clarament qui és el propietari de la relació
   - Utilitza `mappedBy` per indicar el costat no propietari
   - Manté la coherència en les relacions bidireccionals

2. **Ús del Cascade**
   - Utilitza PERSIST quan les entitats fills depenen completament del pare
   - Utilitza REMOVE amb precaució, especialment en relacions ManyToMany
   - Evita utilitzar ALL si no necessites tots els tipus de cascade

3. **Gestió de Transaccions**
   ```java
   try {
       session.beginTransaction();
       // Operacions amb cascade
       session.getTransaction().commit();
   } catch (Exception e) {
       session.getTransaction().rollback();
   }
   ```

## Exemple Complet de Codi

```java
// Configuració inicial
Session session = getSessionFactory().openSession();
session.beginTransaction();

// Crear i relacionar entitats
A a1 = new A();
a1.nomA = "A1";
B b1 = new B();
b1.nomB = "b1";
B b2 = new B();
b2.nomB = "b2";

// Establir relacions bidireccionals
b1.setA(a1);
b2.setA(a1);
a1.getB().add(b1);
a1.getB().add(b2);

// Persistir amb cascade
session.persist(a1);  // b1 i b2 es persisteixen automàticament

session.getTransaction().commit();
```

## Problemes Comuns i Solucions

1. **Relacions No Sincronitzades**
   ```java
   // Incorrecte
   b1.setA(a1);  // Només s'estableix un costat de la relació
   
   // Correcte
   b1.setA(a1);
   a1.getB().add(b1);
   ```

2. **Cascade Innecessari**
   - Evita utilitzar cascade en relacions on les entitats poden existir independentment
   - Considera l'impacte en el rendiment i la integritat de les dades

3. **Transaccions No Gestionades**
   - Sempre utilitza transaccions per a operacions que modifiquen dades
   - Gestiona adequadament els rollbacks en cas d'error

## Conclusió

El cascade en Hibernate és una eina poderosa que, ben utilitzada, pot simplificar molt el codi i mantenir la integritat de les dades. Cal entendre bé cada tipus de cascade i utilitzar-lo només quan sigui necessari, tenint en compte les implicacions en el rendiment i la integritat de les dades.