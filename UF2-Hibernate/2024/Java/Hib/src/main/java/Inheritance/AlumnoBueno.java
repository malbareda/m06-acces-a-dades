package Inheritance;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
@DiscriminatorValue("Buenos")
public class AlumnoBueno extends Alumno {

	@Column
	//COSABUENA NOMES ESTARA EN ELS ALUMNOS BUENOS. ELS ALUMNOS MALOS NO LA TINDRAN
	//PER TANT, COSABUENA ES NULLABLE
	private int cosaBuena;

	public int getCosaBuena() {
		return cosaBuena;
	}

	public void setCosaBuena(int cosaBuena) {
		this.cosaBuena = cosaBuena;
	}
	
	

}
