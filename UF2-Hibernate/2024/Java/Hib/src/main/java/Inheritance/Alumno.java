package Inheritance;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)  //POR DEFECTO ES SINGLE TABLE
//La forma més fàcil de diferenciar entre una classe i una altra es crear una columna nova que t'indiqui el tipus de classe. Així ho pots comprovar ràpidament
@DiscriminatorColumn(name="tipo_de_alumno", discriminatorType = DiscriminatorType.STRING)  //la discirminació pot ser integer o String. Recomano String
public class Alumno {
	
	//ID ESTA A LA BASE EPRQEU COMAPRTEIXEN ID I VULL QUE ESTIGUIN A LA MATEIXA TAULA
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) // eso significa que es un autonumerico
	private int idAlumno;
	
	//AQUEST ESTA A LA BASE PERQEU TOTS ELS FILLS LA TENEN
	private String nombreAlumno;

	@ManyToOne 
	private Profe profe = null;


	public String getNombreAlumno() {
		return nombreAlumno;
	}


	public void setNombreAlumno(String nombreAlumno) {
		this.nombreAlumno = nombreAlumno;
	}



	

	
	public Profe getProfe() {
		return profe;
	}


	public void setProfe(Profe profe) {
		this.profe = profe;
	}


	public Alumno() {
		super();
	}
	
	

}
