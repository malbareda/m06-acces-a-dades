package Inheritance;

import java.util.HashSet;
import java.util.Set;

import cositas.Parasito;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Profe {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) // eso significa que es un autonumerico
	private int idProfe;
	
	@Column
	private String nomProfe;
	
	//en una herencia, el set es de la clase BASE, no de sus hijos
	@OneToMany(mappedBy = "profe") 
	private Set<Alumno> alumnos = new HashSet<>();

	public String getNomProfe() {
		return nomProfe;
	}

	public void setNomProfe(String nomProfe) {
		this.nomProfe = nomProfe;
	}

	public Set<Alumno> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(Set<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	public Profe() {
		super();
	}
	
	
	
}
