package Inheritance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class TestInheritance {

	// Session: representa una connexió amb la BD
	static Session session;
	// SessionFactory: fàbrica de sessions, configurada una vegada per aplicació
	static SessionFactory sessionFactory;
	// ServiceRegistry: registre de serveis d'Hibernate
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			// Crea el registre de serveis utilitzant el fitxer hibernate.cfg.xml
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void main(String[] args) {
		try {
			// creo la sesion, esto es conectarse a la BD sin hacer nada
			session = getSessionFactory().openSession();

			// abrimos PRIMERA transaccion. Eso se hace siempre.
			// Cada operació d'escriptura necessita una transacció
			session.beginTransaction();
			
			Profe p = new Profe();
			p.setNomProfe("Marc");
			AlumnoBueno iker = new AlumnoBueno();
			iker.setNombreAlumno("Iker");
			iker.setCosaBuena(2);
			
			AlumnoMalo feliciano = new AlumnoMalo();
			feliciano.setNombreAlumno("Feliciano");
			feliciano.setCosaMala(true);
			
			
			p.getAlumnos().add(feliciano);
			p.getAlumnos().add(iker);
			
			iker.setProfe(p);
			feliciano.setProfe(p);
				
			session.persist(p);
			session.persist(iker);
			session.persist(feliciano);
			session.getTransaction().commit();

			System.out.println("todo ha salido a pedir de Milhouse 2");
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
			if (null != session.getTransaction()) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				// Si hi ha error, es fa rollback de la transacció
				session.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {
			// Sempre cal tancar la sessió
			if (session != null) {
				session.close();
			}
		}

	}
}
