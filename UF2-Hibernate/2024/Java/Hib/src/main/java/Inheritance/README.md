# Herència a Hibernate

## Concepte Bàsic
L'herència a Hibernate permet mapejar jerarquies de classes Java a taules de base de dades. En els exemples, tenim una jerarquia simple:
- `Alumno` (classe base)
  - `AlumnoBueno` (classe filla)
  - `AlumnoMalo` (classe filla)

## Estratègia SINGLE_TABLE

```java
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo_de_alumno", discriminatorType = DiscriminatorType.STRING)
public class Alumno {
    // ...
}
```

Aquesta estratègia:
- Crea una única taula per tota la jerarquia de classes
- Tots els atributs de les classes filles es guarden a la mateixa taula
- S'utilitza una columna discriminadora per diferenciar entre tipus d'alumnes

### Columna Discriminadora
- Es defineix amb `@DiscriminatorColumn`
- En aquest cas s'anomena "tipo_de_alumno"
- Pot ser de tipus STRING o INTEGER (es recomana STRING per claredat)

### Classes Filles

Les classes filles s'identifiquen amb `@DiscriminatorValue`:

```java
@Entity
@DiscriminatorValue("Buenos")
public class AlumnoBueno extends Alumno {
    private int cosaBuena;
}

@Entity
@DiscriminatorValue("Malos")
public class AlumnoMalo extends Alumno {
    private boolean cosaMala;
}
```

- Cada classe filla especifica el seu valor discriminador
- Els atributs específics (`cosaBuena`, `cosaMala`) es guarden a la mateixa taula que la classe pare

## Relacions amb Herència

### ManyToOne
```java
public class Alumno {
    @ManyToOne 
    private Profe profe;
}
```

### OneToMany
```java
public class Profe {
    @OneToMany(mappedBy = "profe") 
    private Set<Alumno> alumnos = new HashSet<>();
}
```

Important: En una relació amb herència, el Set es defineix amb la classe base (`Alumno`), no amb les classes filles.

## Exemple Pràctic

```java
Profe p = new Profe();
p.setNomProfe("Marc");

// Crear alumne bo
AlumnoBueno iker = new AlumnoBueno();
iker.setNombreAlumno("Iker");
iker.setCosaBuena(2);

// Crear alumne dolent
AlumnoMalo feliciano = new AlumnoMalo();
feliciano.setNombreAlumno("Feliciano");
feliciano.setCosaMala(true);

// Establir relacions bidireccionals
p.getAlumnos().add(feliciano);
p.getAlumnos().add(iker);
iker.setProfe(p);
feliciano.setProfe(p);

// Persistir els objectes
session.persist(p);
session.persist(iker);
session.persist(feliciano);
```

## Avantatges de SINGLE_TABLE
1. Rendiment òptim en consultes
2. No necessita JOINS per recuperar dades
3. Més simple d'implementar

## Consideracions
- Els atributs específics de les classes filles han de ser nullables
- La columna discriminadora ajuda a identificar ràpidament el tipus d'alumne
- Cal mantenir la bidireccionalitat en les relacions
- Es recomana inicialitzar les col·leccions (com el Set d'alumnes)

Aquest exemple mostra com Hibernate gestiona l'herència de manera eficient, permetent treballar amb jerarquies de classes mentre manté la integritat de les dades i les relacions.