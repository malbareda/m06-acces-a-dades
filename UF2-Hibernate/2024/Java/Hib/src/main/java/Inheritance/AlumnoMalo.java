package Inheritance;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
//DiscriminatorValue serà el valor que sortira a la DiscriminatorColumn en els d'aquiesta classe
@DiscriminatorValue("Malos")
public class AlumnoMalo extends Alumno {

	@Column
	private boolean cosaMala;

	public boolean isCosaMala() {
		return cosaMala;
	}

	public void setCosaMala(boolean cosaMala) {
		this.cosaMala = cosaMala;
	}
	
	
}
