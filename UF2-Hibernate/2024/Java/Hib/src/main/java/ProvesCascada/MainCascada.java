// MainCascada.java
package ProvesCascada;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class MainCascada {
    // Session: representa una connexió amb la BD
    static Session session;
    // SessionFactory: fàbrica de sessions, configurada una vegada per aplicació
    static SessionFactory sessionFactory;
    // ServiceRegistry: registre de serveis d'Hibernate
    static ServiceRegistry serviceRegistry;

    // Mètode per obtenir la SessionFactory de forma thread-safe
    public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // Crea el registre de serveis utilitzant el fitxer hibernate.cfg.xml
            serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
            sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
        }
        return sessionFactory;
    }

    public static void main(String[] args) {
        try {
            // Obtenim una sessió de Hibernate (connexió a la BD)
            session = getSessionFactory().openSession();

            // EXEMPLE 1: CASCADE PERSIST
            session.beginTransaction();

            // Creem les entitats i establim les relacions
            A a1 = new A();
            a1.nomA = "A1";
            B b1 = new B();
            b1.nomB = "b1";
            B b2 = new B();
            b1.nomB = "b2";
            
            // Establim la relació bidireccional entre A i B
            b1.setA(a1);
            b2.setA(a1);
            a1.getB().add(b1);
            a1.getB().add(b2);

            // Només cal persistir A1 gràcies al CascadeType.PERSIST
            // Els objectes B es persistiran automàticament
            session.persist(a1);
            session.getTransaction().commit();

            // EXEMPLE 2: CASCADE REMOVE
            session.beginTransaction();

            // Creem entitats C i D per demostrar el CASCADE REMOVE
            C c1 = new C();
            c1.nomC = "C1";
            C c2 = new C();
            c2.nomC = "C2";
            D d1 = new D();
            d1.nomD = "D1";

            // Establim relacions bidireccionals entre C i D
            d1.getC().add(c1);
            d1.getC().add(c2);
            c1.getD().add(d1);
            c2.getD().add(d1);

            // Cal persistir C1 i C2 ja que no hi ha cascade PERSIST
            session.persist(c1);
            session.persist(c2);
            session.merge(a1);

            session.getTransaction().commit();

            session.clear();  // Netegem la sessió
            
            // Exemple de CASCADE REMOVE
            session.beginTransaction();
            // En eliminar D1, s'eliminaran automàticament totes les C relacionades
            session.remove(d1);
            session.getTransaction().commit();

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            if (null != session.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                session.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}