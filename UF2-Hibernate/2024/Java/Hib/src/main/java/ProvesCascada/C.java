package ProvesCascada;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;

@Entity
public class C {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	
	@Column
	String nomC;
	
	
	public C() {
	}
	
	@ManyToMany(mappedBy = "c")
	private Set<D> d = new HashSet<D>();
	
	public Set<D> getD() {
		return d;
	}
}
