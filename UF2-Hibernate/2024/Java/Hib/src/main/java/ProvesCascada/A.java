package ProvesCascada;

import java.util.HashSet;
import java.util.Set;
import jakarta.persistence.*;

@Entity  // Marca aquesta classe com una entitat JPA
public class A {
    @Id  // Marca aquest camp com la clau primària
    @GeneratedValue(strategy = GenerationType.IDENTITY)  // Generació automàtica de l'ID
    int id;
    
    @Column  // Marca aquest camp com una columna de la taula
    String nomA;
    
    // Relació One-to-Many amb l'entitat B
    // CascadeType.PERSIST significa que quan persistim A, es persistiran automàticament els B relacionats
    // Es a dir, persistiran els B que estiguin dintre de la llista d'A
    @OneToMany(mappedBy = "a", cascade=CascadeType.PERSIST)
    private Set<B> b = new HashSet<B>();

    // Getters i setters
    public String getNomA() { return nomA; }
    public void setNomA(String nomA) { this.nomA = nomA; }
    public Set<B> getB() { return b; }
    public void setB(Set<B> b) { this.b = b; }
}
