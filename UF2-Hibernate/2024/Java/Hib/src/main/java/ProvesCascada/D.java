package ProvesCascada;

import java.util.HashSet;
import java.util.Set;
import jakarta.persistence.*;

@Entity
public class D {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    
    @Column
    String nomD;
    
    // Relació Many-to-Many amb l'entitat C
    // CascadeType.REMOVE significa que quan eliminem un D, s'eliminaran automàticament tots els C relacionats
    // Es a dir, s'eliminaran de la BD tots els C que estaven a la llista de D.
    @ManyToMany(cascade = CascadeType.REMOVE)
    private Set<C> c = new HashSet<C>();
    
    public Set<C> getC() { return c; }
}
