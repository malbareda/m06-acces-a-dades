package examenPruebaHib;

import java.util.Set;

public interface IMenaDAO {

	Set<Dwarf> getDwarves(Integer id);

	void setInitialMinerals(Integer id);

}
