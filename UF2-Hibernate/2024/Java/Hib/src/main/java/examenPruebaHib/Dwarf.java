package examenPruebaHib;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
class Dwarf {
	
	@Id // eso significa que es la llave primaria
	@GeneratedValue(strategy = GenerationType.IDENTITY) // eso significa que es un autonumerico
	@Column
	private int id;
	
	@ManyToOne(cascade = CascadeType.PERSIST, optional = false)
	Mena mena;
	
	@Column
	String nom;
	
	@Column
	int numObjectes;
	
	@Column
	int maxObjectes;
	
	@Column
	@Enumerated(EnumType.STRING)
	EinaType einaEquipada;

	public String getNom() {
		return nom;
	}

	
	public Mena getMena() {
		return mena;
	}


	public void setMena(Mena mena) {
		this.mena = mena;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNumObjectes() {
		return numObjectes;
	}

	public void setNumObjectes(int numObjectes) {
		this.numObjectes = numObjectes;
	}

	public int getMaxObjectes() {
		return maxObjectes;
	}

	public void setMaxObjectes(int maxObjectes) {
		this.maxObjectes = maxObjectes;
	}

	public EinaType getEinaEquipada() {
		return einaEquipada;
	}

	public void setEinaEquipada(EinaType einaEquipada) {
		this.einaEquipada = einaEquipada;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Dwarf [id=" + id + ", nom=" + nom + ", numObjectes=" + numObjectes + ", maxObjectes=" + maxObjectes
				+ ", einaEquipada=" + einaEquipada + "]";
	}

	public Dwarf() {
		super();
	}
	
	

}
