package examenPruebaHib;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import cositas.Collar;
import cositas.Parasito;
import cositas.Raton;
import cositas.TipusParasit;

public class MainPrueba {
	
	// Session: representa una connexió amb la BD
    static Session session;
    // SessionFactory: fàbrica de sessions, configurada una vegada per aplicació
    static SessionFactory sessionFactory;
    // ServiceRegistry: registre de serveis d'Hibernate
    static ServiceRegistry serviceRegistry;

    public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // Crea el registre de serveis utilitzant el fitxer hibernate.cfg.xml
            serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
            sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
        }
        return sessionFactory;
    }
    
    
	public static void main(String[] args) {
		try {
            //creo la sesion, esto es conectarse a la BD sin hacer nada
            session = getSessionFactory().openSession();

            session.beginTransaction();
            	Dwarf d1 = new Dwarf();
            	Dwarf d2= new Dwarf();
            	Dwarf d3= new Dwarf();
            	Dwarf d4= new Dwarf();
            	Dwarf d5= new Dwarf();
            	Mena m1 = new Mena();
            	Mena m2 = new Mena();
            	
            	d1.setNom("Chen");
            	d2.setNom("Chen Chen");
            	d3.setNom("Chen Chen Chen");
            	d4.setNom("Chen Chen Chen Chen");
            	d5.setNom("Chen Chen Chen Chen Chen");
            	d1.setEinaEquipada(EinaType.MARTELL);
            	d2.setEinaEquipada(EinaType.PALA);
            	d3.setEinaEquipada(EinaType.PIC);
            	d4.setEinaEquipada(EinaType.MARTELL);
            	d5.setEinaEquipada(EinaType.MARTELL);
            	d1.setMaxObjectes(30);
            	d2.setMaxObjectes(30);
            	d3.setMaxObjectes(30);
            	d4.setMaxObjectes(30);
            	d5.setMaxObjectes(30);
            	
            	m1.setTipusMena(MenaType.COURE);
            	m2.setTipusMena(MenaType.FERRO);
            	
            	
            	d1.setMena(m1);
            	d2.setMena(m1);
            	d3.setMena(m1);
            	d4.setMena(m2);
            	d5.setMena(m2);
            	
            	session.persist(d1);
            	session.persist(d2);
            	session.persist(d3);
            	session.persist(d4);
            	session.persist(d5);
            session.getTransaction().commit();
            

            System.out.println("todo ha salido a pedir de Milhouse");
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            if (null != session.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                // Si hi ha error, es fa rollback de la transacció
                session.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        } finally {
            // Sempre cal tancar la sessió
            if (session != null) {
                session.close();
            }
        }
    }

}
