package examenPruebaHib;


import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.*;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity
public class Mena {
	
	@Id // eso significa que es la llave primaria
	@GeneratedValue(strategy = GenerationType.IDENTITY) // eso significa que es un autonumerico
	@Column
	private int id;
	
	
	@OneToMany(mappedBy = "mena",fetch = FetchType.EAGER )
	Set<Dwarf> dwarves = new HashSet<>();
	
	@Column
	int numMinerals;
	
	@Column
	@Enumerated(EnumType.STRING)
	MenaType tipusMena;

	public int getNumMinerals() {
		return numMinerals;
	}

	public void setNumMinerals(int numMinerals) {
		this.numMinerals = numMinerals;
	}

	public MenaType getTipusMena() {
		return tipusMena;
	}

	public void setTipusMena(MenaType tipusMena) {
		this.tipusMena = tipusMena;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Mena [id=" + id + ", numMinerals=" + numMinerals + ", tipusMena=" + tipusMena + "]";
	}

	public Mena() {
		super();
		numMinerals=25;
	}

	public Set<Dwarf> getDwarves() {
		return dwarves;
	}

	public void setDwarves(Set<Dwarf> dwarves) {
		this.dwarves = dwarves;
	}
	
	
	
	
	
}
