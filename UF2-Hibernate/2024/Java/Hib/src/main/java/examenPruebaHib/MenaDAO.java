package examenPruebaHib;

import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public class MenaDAO extends GenericDao<Mena, Integer> implements IMenaDAO{
		
	@Override
    public Set<Dwarf> getDwarves(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            // Utilitzem el mètode getEntityClass() per obtenir la classe correcta
            
            return session.get(Mena.class, id).getDwarves();
        } catch (HibernateException e) {
        	e.printStackTrace();
            return null;
        }
    }
	
	@Override
    public void setInitialMinerals(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            // Utilitzem el mètode getEntityClass() per obtenir la classe correcta
            
            Mena m = session.get(Mena.class, id);
            if(m.getTipusMena()==MenaType.FERRO) {
            	if(m.getNumMinerals()>10)m.setNumMinerals(m.getNumMinerals()-10); else m.setNumMinerals(0);
            	
            }
            if(m.getTipusMena()==MenaType.COURE) {
            	if(m.getNumMinerals()>5)m.setNumMinerals(m.getNumMinerals()-5); else m.setNumMinerals(0);   	
            }
            System.out.println(m);
        } catch (HibernateException e) {
        	e.printStackTrace();
        }
    }
	
	
	
}
