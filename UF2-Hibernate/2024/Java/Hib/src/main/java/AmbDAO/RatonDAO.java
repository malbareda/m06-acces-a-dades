package AmbDAO;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import cositas.Raton;

/**
 * Implementació específica del DAO per l'entitat Raton
 * Hereta de GenericDao per tenir la implementació bàsica CRUD
 * i implementa IRatonDAO per afegir funcionalitat específica
 */
public class RatonDAO extends GenericDao<Raton, Integer> implements IRatonDAO {

    @Override
    public int numRatones() {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            // Utilitzem el mètode list() heretat de GenericDao
            int size = this.list().size();
            session.getTransaction().commit();
            return size;
        } catch (HibernateException e) {
        	e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();        }
        return -1;
    }

    @Override
    //aquest mètode és específic de Raton. No té sentit que sigui genèric
    public int cumpleanos(int id) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            Raton r = session.get(Raton.class, id);
            Date date = r.getFechaNacimiento();
            Date now = new Date();
            
            // Comprovem si és el seu aniversari (mateix dia i mes)
            if(date.getDate() == now.getDate() && date.getMonth() == now.getMonth()) {
                System.out.println("ES TU CUMPLEAÑOS");
                r.setEdad(r.getEdad() + 1);
                session.getTransaction().commit();
                return r.getEdad();
            } else {
                System.out.println("Es otro dia miserable de tu existencia como rata");
                session.getTransaction().commit();
                return -1;
            }
        } catch (HibernateException e) {
        	e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();        }
        return -1;
    }
}
