package AmbDAO;

import cositas.Raton;

/**
 * Interfície específica per l'entitat Raton
 * Hereta de IGenericDao per tenir els mètodes CRUD bàsics
 * i afegeix mètodes específics per la gestió de ratolins
 */
public interface IRatonDAO extends IGenericDao<Raton, Integer> {
    /**
     * @return Nombre total de ratolins a la base de dades
     */
    public int numRatones();
    
    /**
     * Comprova si és l'aniversari del ratolí i actualitza la seva edat si cal
     * @param id ID del ratolí
     * @return Nova edat si és el seu aniversari, -1 si no ho és
     */
    int cumpleanos(int id);
}
