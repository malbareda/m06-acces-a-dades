package AmbDAO;

import java.util.Date;

import cositas.Raton;

/**
 * Classe principal que demostra l'ús del patró DAO
 */
public class MainDAO {
    public static void main(String[] args) {
        // Creem una instància del DAO específic per Raton
        RatonDAO ratonDAO = new RatonDAO();
        
        // Creem un nou ratolí
        Raton rat = new Raton("Miquel", 22, "Negro", 0);
        rat.setFechaNacimiento(new Date());
        
        // Utilitzem els mètodes del DAO per interactuar amb la base de dades
        ratonDAO.saveOrUpdate(rat);  // Guardem el ratolí
        System.out.println(ratonDAO.get(1));  // Obtenim el ratolí amb ID 1
        
        // Utilitzem els mètodes específics de RatonDAO
        System.out.println(ratonDAO.numRatones());  // Mostrem el nombre total de ratolins
        System.out.println(ratonDAO.cumpleanos(1));  // Comprovem si és l'aniversari del ratolí 1
    }
}