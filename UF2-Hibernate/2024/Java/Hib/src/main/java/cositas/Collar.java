package cositas;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.ManyToAny;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table
public class Collar {
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// nullable=false significa que el camp no pot ser NULL
	// updatable=true permet actualitzar aquest camp
	@Column(nullable = false, updatable = true)
	private int id;
	
	
	@Column
	private String marca;
	
	@Column
	private double preu;
	
	
	@ManyToMany(mappedBy = "debilA")
	Set<Parasito> tratamiento = new HashSet<Parasito>();

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "collar")
	Raton raton;
	
	
	
	
	public Raton getRaton() {
		return raton;
	}

	public void setRaton(Raton raton) {
		this.raton = raton;
	}

	public int getId() {
		return id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public double getPreu() {
		return preu;
	}

	public void setPreu(double preu) {
		this.preu = preu;
	}

	public Set<Parasito> getTratamiento() {
		return tratamiento;
	}

	public void setTratamiento(Set<Parasito> tratamiento) {
		this.tratamiento = tratamiento;
	}

	public Collar() {
		super();
		tratamiento = new HashSet<Parasito>();

	}
	
	

}
