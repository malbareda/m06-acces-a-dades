package cositas;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.ColumnDefault;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

//@Entity marca aquesta classe per ser mapejada a una taula
@Entity
//@Table sense paràmetres crearà una taula amb el mateix nom que la classe
@Table
public class Parasito {

	// Configuració de la clau primària autoincremental
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// nullable=false significa que el camp no pot ser NULL
	// updatable=true permet actualitzar aquest camp
	@Column(nullable = false, updatable = true)
	private int id;

	// length=20 limita la longitud del camp a la BD a 20 caràcters
	@Column(name = "nombre", length = 20)
	private String nombre;

	// unique=false permet valors duplicats en aquesta columna
	// columnDefault et deixa fer un valor per defecte
	@Column(unique = false)
	@ColumnDefault(value = "0")
	private int peligrosidad;

	// Enumerated et permet posar un Enum. EnumType de String o Ordinal (num
	// d'ordre)
	@Enumerated(EnumType.STRING)
	@Column(name = "tipus_parasit")
	private TipusParasit tipus;

	// @ManyToOne defineix una relació molts-a-un amb Raton
	@ManyToOne
	// @JoinColumn especifica la columna que contindrà la clau forana
	// Si no es posa name, es dirà raton_id per defecte
	@JoinColumn(name = "ratonParasitando") // esta es opcional
	private Raton raton;

	//ManyToMany defineix una relacio N a N
	//Cascade defineix els deletes/update en cascada
	@ManyToMany
	@JoinTable(  //el jointable es totalment opcional, es per definir com queda la join table
			name="Collar_Parasito",
			joinColumns = {@JoinColumn(name = "id_parasito")}, // joinColumn es la columna de la taula intermitja que defineix la clau forana de LA MEVA TAULA
			inverseJoinColumns ={@JoinColumn(name = "id_collar")} // joinColumn es la columna de la taula intermitja que defineix la clau forana de L'ALTRA TAULA  
	)
	private Set<Collar> debilA = new HashSet<Collar>();

	public Set<Collar> getDebilA() {
		return debilA;
	}

	public void setDebilA(Set<Collar> debilA) {
		this.debilA = debilA;
	}

	public Raton getRaton() {
		return raton;
	}

	public void setRaton(Raton raton) {
		this.raton = raton;
	}

	public Parasito() {
		super();
		debilA = new HashSet<Collar>();

	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPeligrosidad() {
		return peligrosidad;
	}

	public void setPeligrosidad(int peligrosidad) {
		this.peligrosidad = peligrosidad;
	}

	public TipusParasit getTipus() {
		return tipus;
	}

	public void setTipus(TipusParasit tipus) {
		this.tipus = tipus;
	}

}
