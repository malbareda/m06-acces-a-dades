package cositas;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.hibernate.type.TrueFalseConverter;
import org.hibernate.type.YesNoConverter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

//FUNCIONA CON ANOTACIONES JPA

//@Entity indica que aquesta classe es mapejara a una taula de la base de dades
@Entity
//@Table defineix el nom de la taula a la BD. Si no es posa, agafa el nom de la classe
@Table(name = "raton") // name es el nombre de la tabla. No es obligatorio

public class Raton {

	// @Id marca aquest camp com a clau primària de la taula
	@Id // eso significa que es la llave primaria
	// @GeneratedValue amb IDENTITY indica que la BD assignarà IDs automàticament
	// (autoincrement)
	@GeneratedValue(strategy = GenerationType.IDENTITY) // eso significa que es un autonumerico
	// @Column permet especificar propietats de la columna, com el nom
	@Column(name = "id_raton") // especificar que es una columna
	private int id;

	// @Column sense paràmetres farà servir el nom de la variable com a nom de
	// columna
	@Column // aqui no he puesto name, asi que se llamara "nombre" por defecto
	private String nombre;

	@Column
	private int edad;

	@Column
	private String colorPelo;

	@Column(precision = 10, scale = 2)
	private BigDecimal tamanoCola;

//columnDefault et deixa fer un valor per defecte
	@Column
	@ColumnDefault(value = "true")
	//el convert es para hacer que las representaciones de boolean no se den en hexadecimal
	//TrueFalseConverter = T/F
	//YesNoConverter = Y/N
	@Convert(converter = TrueFalseConverter.class)
	private boolean vivo;

	// @Temporal especifica com es guardarà el tipus Date
	// TemporalType.DATE només guarda la data sense l'hora
	@Temporal(TemporalType.DATE)
	@Column
	private Date fechaNacimiento;

	// @OneToMany defineix una relació un-a-molts amb Parasito
	// mappedBy indica que la relació està mapejada pel camp "raton" a la classe
	// Parasito
	@OneToMany(mappedBy = "raton", cascade = CascadeType.REMOVE) // tiene que llamarse igual que la variable, la variable de Java, no la columna,
									// que hay en parasito, y que representa a raton
	private Set<Parasito> parasitos = new HashSet<>();

	//OneToOne defineix una relacio 1 a 1.
	//cascade defineix que hi ha una relacio en cascada
	//CascedeType.Persist -> Persist en Cascada. Això significa que quan afegeixis un element que tingui "dintre" un altre element, afegirà l'altre
	@OneToOne(cascade = CascadeType.REMOVE)
	Collar collar;

	
	
	public Collar getCollar() {
		return collar;
	}

	public void setCollar(Collar collar) {
		this.collar = collar;
	}

	public Set<Parasito> getParasitos() {
		return parasitos;
	}

	public void setParasitos(Set<Parasito> parasitos) {
		this.parasitos = parasitos;
	}

	// es bueno tener siempre un constructor vacio. Si no petara
	public Raton() {
		super();
	}

	public Raton(String nombre, int edad, String colorPelo, double tamanoCola) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.colorPelo = colorPelo;
		this.tamanoCola = BigDecimal.valueOf(tamanoCola);
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getColorPelo() {
		return colorPelo;
	}

	public void setColorPelo(String colorPelo) {
		this.colorPelo = colorPelo;
	}

	public BigDecimal getTamanoCola() {
		return tamanoCola;
	}

	public void setTamanoCola(BigDecimal tamanoCola) {
		this.tamanoCola = tamanoCola;
	}

	public boolean isVivo() {
		return vivo;
	}

	public void setVivo(boolean vivo) {
		this.vivo = vivo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public String toString() {
		return "Raton [id=" + id + ", nombre=" + nombre + ", edad=" + edad + ", colorPelo=" + colorPelo
				+ ", tamanoCola=" + tamanoCola + "]";
	}

}
