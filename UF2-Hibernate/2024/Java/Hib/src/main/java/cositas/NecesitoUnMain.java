package cositas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class NecesitoUnMain {
    
    // Session: representa una connexió amb la BD
    static Session session;
    // SessionFactory: fàbrica de sessions, configurada una vegada per aplicació
    static SessionFactory sessionFactory;
    // ServiceRegistry: registre de serveis d'Hibernate
    static ServiceRegistry serviceRegistry;

    public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            // Crea el registre de serveis utilitzant el fitxer hibernate.cfg.xml
            serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
            sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
        }
        return sessionFactory;
    }
    
    public static void main(String[] args) {
        try {
            //creo la sesion, esto es conectarse a la BD sin hacer nada
            session = getSessionFactory().openSession();

            // abrimos PRIMERA transaccion. Eso se hace siempre.
            // Cada operació d'escriptura necessita una transacció
            session.beginTransaction();
            Random rand = new Random();
            for(int i=0;i<4;i++) {
                Raton r = new Raton();
                r.setNombre("Acrasio");
                r.setEdad(i);
                r.setColorPelo("blanco");
                r.setTamanoCola(new BigDecimal(rand.nextDouble()*3));
                r.setFechaNacimiento(new Date());
                //guardar en base de datos
                // persist() guarda l'objecte a la BD
                session.persist(r);
                
            }
            // Commit realiza los cambios en la BD
            //es como el flush de la BD. El commit lo que hace es bloquear la BD, hacer todos los cambios, y desbloquear la BD. Esto mata la transaccion.
            session.getTransaction().commit();
            
            // Nueva transacción para crear parásitos
            session.beginTransaction();        
            // get() recupera un objecte per la seva ID
            Raton rParasitos = session.get(Raton.class, 1);
            Parasito p1 = new Parasito();
            p1.setNombre("pulgarcito");
            p1.setPeligrosidad(10);
            p1.setRaton(rParasitos);
            p1.setTipus(TipusParasit.HELMINTS);
            rParasitos.getParasitos().add(p1);
            Parasito p2 = new Parasito();
            p2.setNombre("piojin");
            //p2.setPeligrosidad(1);
            p2.setRaton(rParasitos);
            p2.setTipus(TipusParasit.ECTOPARASITS);
            rParasitos.getParasitos().add(p2);
            
            session.persist(p1);
            session.persist(p2);
            
            session.getTransaction().commit();
            
            // Transacción para eliminar un ratón
            session.beginTransaction();        
            Raton rMuerto = session.get(Raton.class, 2);
            // remove() elimina l'objecte de la BD
            session.remove(rMuerto);
            session.getTransaction().commit();
            
            // Transacción para consultar y actualizar
            session.beginTransaction();        
            // createQuery executa una consulta HQL (Hibernate Query Language)
            // en aquest cas, "from Raton" torna tots els ratons, com un select all
            ArrayList<Raton> todos = (ArrayList<Raton>) session.createQuery("from Raton", Raton.class).list();
            
            for (Raton raton : todos) {
            	double a = raton.getTamanoCola().doubleValue();
                if(a >5) {
                    raton.setNombre("Gertrudis");
                }
                // merge() actualitza l'objecte a la BD
                session.merge(raton);
            }
            session.getTransaction().commit();
            
            
            session.beginTransaction();       
            
            Collar c = new Collar();
            c.getTratamiento().add(p1);
            c.getTratamiento().add(p2);
            Collar c2 = new Collar();
            c2.getTratamiento().add(p2);
            
            session.persist(c);
            session.persist(c2);
            
            p1.getDebilA().add(c);
            p2.getDebilA().add(c);
            p2.getDebilA().add(c2);
            
            session.merge(p1);
            session.merge(p2);
            
            session.getTransaction().commit();
            
            session.beginTransaction();       
            rParasitos.setCollar(c);
            session.merge(rParasitos);
            c.setRaton(rParasitos);
            session.merge(c);
            session.getTransaction().commit();
            

            session.beginTransaction();       
            //el remove del raton va a borrar su collar, porque la relacion entre raton y collar tiene delete en cascada
            session.remove(rParasitos);
            session.getTransaction().commit();

            System.out.println("todo ha salido a pedir de Milhouse");
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            if (null != session.getTransaction()) {
                System.out.println("\n.......Transaction Is Being Rolled Back.......");
                // Si hi ha error, es fa rollback de la transacció
                session.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        } finally {
            // Sempre cal tancar la sessió
            if (session != null) {
                session.close();
            }
        }
    }
}