import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

public class XML {

	public static void main(String[] args) {
		
		 try {
			File file = new File("alumnes.xml");  
			 JAXBContext jaxbContext = JAXBContext.newInstance(Alumnes.class);  
   
			 Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
			 Alumnes ins= (Alumnes) jaxbUnmarshaller.unmarshal(file);  
			   
			 System.out.println(ins.getNom());  
			 
			 
			 Gatete dani = new Gatete("Dani","Suñe","casa seva","901283201D","dani@dani.dani");
			 
			 ins.getCicles().add(dani);
			 
			 Marshaller marshallerObj = jaxbContext.createMarshaller();  
			 marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  
			
			 marshallerObj.marshal(ins, new FileOutputStream("alumnes2.xml"));
		} catch (PropertyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
           
		
		
	}
	
	
}
