import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;

public class Ganso2JSON {

	public static void main(String[] args) {

		Ganso gregorio = new Ganso(35, "Gregorio", true, 50.6, new Higado("verde", 60.1, true));

		JSONObject higado = new JSONObject();
		higado.put("color", "verde");
		higado.put("peso", 60.1);
		higado.put("foiegras", true);

		JSONObject GregorioSON = new JSONObject();
		GregorioSON.put("higado", higado);
		GregorioSON.put("edad", 35);
		GregorioSON.put("nom", "Gregorio");
		GregorioSON.put("sexe", true);
		GregorioSON.put("pes", 50.5);

		try (FileWriter file = new FileWriter("GSON")) {

			file.write(GregorioSON.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
