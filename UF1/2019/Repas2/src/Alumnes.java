import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "institut")
public class Alumnes {

	private String nom;
	private ArrayList<Gatete> eso;
	private ArrayList<Gatete> cicles;
	
	
	
	public Alumnes() {
		super();
	}

	
	public Alumnes(String nom, ArrayList<Gatete> eso, ArrayList<Gatete> cicles) {
		super();
		this.nom = nom;
		this.eso = eso;
		this.cicles = cicles;
	}


	@XmlElement
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlElementWrapper(name = "eso")
    @XmlElement(name = "alumne")
	public ArrayList<Gatete> getEso() {
		return eso;
	}
	public void setEso(ArrayList<Gatete> lalumnes) {
		this.eso = lalumnes;
	}
	
	@XmlElementWrapper(name = "cicles")
    @XmlElement(name = "alumne")
	public ArrayList<Gatete> getCicles() {
		return cicles;
	}
	public void setCicles(ArrayList<Gatete> lalumnes) {
		this.cicles = lalumnes;
	}
	
	
	
	
}
