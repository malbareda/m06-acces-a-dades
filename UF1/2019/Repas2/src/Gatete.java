import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name="alumne")
@XmlType(propOrder={"nom", "cognoms" , "direccio", "DNI", "mail" })
public class Gatete {
	
	
	private String nom;
	private String cognoms;
	private String direccio;
	private String DNI;
	private String mail;
	public Gatete(String nom, String cognoms, String direccio, String dNI, String mail) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.direccio = direccio;
		DNI = dNI;
		this.mail = mail;
	}
	
	
	public Gatete() {
		super();
	}
	@XmlElement
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@XmlElement
	public String getCognoms() {
		return cognoms;
	}
	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}
	@XmlElement
	public String getDireccio() {
		return direccio;
	}
	public void setDireccio(String direccio) {
		this.direccio = direccio;
	}
	@XmlElement
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	@XmlElement
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	

}
