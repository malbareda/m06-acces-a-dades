import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Text {

	public static void main(String[] args) {

	}

	String[] file2Array(File f, int l) {
		try {
			FileReader fr;
			String[] array = new String[l];
			fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			int posicion = 0;
			while (br.ready()) {
				array[posicion] = br.readLine();
				posicion++;
			}
			fr.close();
			br.close();

			return array;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	ArrayList<String> file2ArrayList(File f) {
		try {
			FileReader fr;
			ArrayList<String> list = new ArrayList<>();
			fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				list.add(br.readLine());
			}
			fr.close();
			br.close();

			return list;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	ArrayList<ArrayList<String>> file2Words(File f) {
		try {
			FileReader fr;
			ArrayList<String> line = new ArrayList<>();
			ArrayList<ArrayList<String>> text = new ArrayList<>();
			fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				
				String s = br.readLine();
				String[] array = s.split(" ");
				line.clear();
				for(String str : array) {
					line.add(str);
				}
				text.add(line);
			}
			fr.close();
			br.close();

			return text;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	File array2File(String path, String[] array) {

		try {
			File f = new File(path);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for (String s : array) {
				bw.write(s);
				bw.newLine();
			}
			bw.flush();
			bw.close();
			fw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	File arrayList2File(String path, ArrayList<String> list) {

		try {
			File f = new File(path);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			for (String s : list) {
				bw.write(s);
				bw.newLine();
			}
			bw.flush();
			bw.close();
			fw.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	String file2String(File f, int l) {

		String[] array = file2Array(f, l);
		String s = "";
		for (int i = 0; i < l; i++) {
			s = s + array[i] + "\n";
		}

		return s;

	}

	File string2File(String path, String s) {

		String[] array = s.split("\n");
		array2File(path, array);

		return null;
	}

}
