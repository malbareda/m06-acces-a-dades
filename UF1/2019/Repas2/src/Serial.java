import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serial {

	public static void main(String[] args) {
		Ganso gregorio = new Ganso(35, "Gregorio", true, 50.6, new Higado("verde", 60.1, true));

		try {
			FileOutputStream fos = new FileOutputStream("ganso");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(gregorio);

			fos.close();
			oos.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			FileInputStream fis = new FileInputStream("ganso");
			ObjectInputStream ois = new ObjectInputStream(fis);
			while (true) {
				ois.readObject();
			}
			
		} catch (EOFException eof) { //si sale esta excepcion ha ido bien la cosa
			System.out.println("this is fine");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			
			e.printStackTrace();
		}
	}

}
