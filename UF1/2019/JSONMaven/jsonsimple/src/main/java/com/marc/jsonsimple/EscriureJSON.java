package com.marc.jsonsimple;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class EscriureJSON {

    public static void main(String[] args) {

    	//JSONObject, funciona igual que un HashMap
    	JSONObject obj = new JSONObject();
        //JSONObject obj = new JSONObject();
    	obj.put("nom", "marc");
    	obj.put("edad", 21);

        //JSONArray, funciona igual que una ArrayList
        //Scanner sc = new Scanner(System.in);
        JSONArray list = new JSONArray();
        list.add("jugar al WoW");
        list.add("programar");
        list.add("riure'm dels alumnes");

        obj.put("aficions", list);
        
        System.out.println(obj.toJSONString());

        try (FileWriter file = new FileWriter("testjson.json")) {

            file.write(obj.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
        //linkedhashmap, permet ordre en el json (per defecte es desordenat)
    	LinkedHashMap<String, Object> lmap = new LinkedHashMap<>();
    	lmap.put("nom", "kernel");
    	lmap.put("edad", 4);
    	lmap.put("menjar", "salmo");
    	
    	JSONObject lobj = new JSONObject(lmap);
    	
    	
        try (FileWriter file = new FileWriter("testjson2.json")) {

            file.write(lobj.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(obj);

    }

}