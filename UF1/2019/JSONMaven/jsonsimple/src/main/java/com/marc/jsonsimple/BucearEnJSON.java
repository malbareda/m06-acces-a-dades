package com.marc.jsonsimple;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


public class BucearEnJSON {
	
	public static void main(String[] args) {
		
		try {
			
			String rip = "3-4-2";
			JSONParser parser = new JSONParser();
			//obtenemos objeto raiz
			Object obj = parser.parse(new FileReader("aules.json"));
			
			//el objeto raiz es un array asi que casteamos array
			JSONArray array = (JSONArray) obj;
			//recorremos el array como si fuera una arraylist
			for(Object o : array) {
				
				//lo que hay dentro del array son jsonobjects asi que casteamos
				JSONObject aula = (JSONObject) o;
				
				//buscamos el campo maquinas. Es un JSONArray
				JSONArray maquines = (JSONArray) aula.get("maquines");
				
				//recorremos el array. Como queremos borrar, lo haremos con un iterator
				for (Iterator iterator = maquines.iterator(); iterator.hasNext();) {
				    
					Object object = (Object) iterator.next();
					//lo que hay dentro del array de maquians son JSONObjects, asi que casteamos
					JSONObject maquina = (JSONObject) object;
					//cogemos el nombre. Es una String.
					String nom = (String) maquina.get("nom");
					//borramos con el iterador
					if(nom.equals(rip)) {
						iterator.remove();
					}
					
				}
				
				
			}
			
			
			//escribir el objeto RAIZ.
			FileWriter file = new FileWriter("testjson");
	        file.write(array.toJSONString());
	        file.flush();
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    
	

}
