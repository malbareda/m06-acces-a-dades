package com.marc.jsonsimple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class IntroHashMaps {
	
	
	public static void main(String[] args) {
		
		
		ArrayList<Integer> list = new ArrayList<>();
		
		
		HashMap<String,String> diccionario = new HashMap<>();
		
		//put, introduce
		diccionario.put("casa", "lugar muy bonito donde vives");
		diccionario.put("duplex", "lugar que no me puedo permitir");
		diccionario.put("anacardo","fruto seco bastante rico");
		diccionario.put("altramuces","fruto seco muy rico");
		diccionario.put("almendra","fruto seco bastante rico");
		diccionario.put("avellana","fruto seco bastante rico");
		diccionario.put("arandano","fruto seco bastante rico");
		System.out.println(diccionario.put("America","fruto seco bastante rico"));
		
		//se ordena a su manera (desordenado)
		System.out.println(diccionario);
		
		///obtiene la definicion
		System.out.println(diccionario.get("altramuces"));
		
		//si ya existe lo sobreescribe, no puede haber dos llaves iguales
		System.out.println(diccionario.put("America","continente"));
		
		System.out.println(diccionario.get("America"));
		
		System.out.println(diccionario.keySet());
		
		for(String s : diccionario.keySet()) {
			System.out.println(s+": "+diccionario.get(s));
		}
		
		//si no lo encunetra devuelve el valor de Default
		System.out.println(diccionario.getOrDefault("Amurica", "not found"));
		
		//contains devuelve un booleano si esta o no
		System.out.println(diccionario.containsKey("America"));
		System.out.println(diccionario.containsValue("America"));
		
		
		LinkedHashMap<String,String> diccionarioOrdenado = new LinkedHashMap<>();
		//put, introduce
		diccionarioOrdenado.put("casa", "lugar muy bonito donde vives");
		diccionarioOrdenado.put("duplex", "lugar que no me puedo permitir");
		diccionarioOrdenado.put("anacardo","fruto seco bastante rico");
		diccionarioOrdenado.put("altramuces","fruto seco muy rico");
		diccionarioOrdenado.put("almendra","fruto seco bastante rico");
		diccionarioOrdenado.put("avellana","fruto seco bastante rico");
		diccionarioOrdenado.put("arandano","fruto seco bastante rico");
		System.out.println(diccionarioOrdenado.put("America","fruto seco bastante rico"));
		
		//se ordena a su manera (desordenado)
		System.out.println(diccionarioOrdenado);
		

		
		
		
		
	}

}
