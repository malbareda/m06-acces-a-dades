package com.marc.jsonsimple;

public class ArgumentosMultiples {
	
	public static void main(String[] args) {
		whatever(1,"a");
		whatever(3,"a","b");
		whatever(5,"a","b","c");
		whatever(6,"a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c","a","b","c");
		
	}
	

	public static int whatever(int numero, String ...strings ) {
		int a =numero;
		String cat = "";
		for(String s : strings) {
			a++;
			cat+=s;
		}
		System.out.println(a+" "+cat);
		return a;
	}

}
