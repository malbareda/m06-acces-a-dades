package com.marc.jsonsimple;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class llegirJSON {

    public static void main(String[] args) {

    	//magia negra
        JSONParser parser = new JSONParser();

        try {
        	//castejes a l'arrel. L'arrel pot ser un objecte o un array json, depèn
            Object obj = parser.parse(new FileReader("testjson.json"));

            JSONObject jsonObject = (JSONObject) obj;
            System.out.println(jsonObject);

            //keyset et torna totes les claus
            System.out.println(jsonObject.keySet());
            //Recordem: Funciona igual que un hashmap
            String name = (String) jsonObject.get("nom");
            System.out.println(name);

            long age = (Long) jsonObject.get("edad");
            System.out.println(age);

            // loop array
            JSONArray aficions = (JSONArray) jsonObject.get("aficions");

            for(Object s : aficions) {
            	System.out.println(s);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}