package repaso;

import java.io.Serializable;

public class Vacuna implements Serializable{
	
	String nom;

	public Vacuna(String nom) {
		super();
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Vacuna [nom=" + nom + "]";
	}
	
	

}
