package repaso;

import java.io.Serializable;
import java.util.ArrayList;

public class Gatete implements Serializable{
	
	String nom;
	int edat;
	String nacimiento;
	String raza;
	ArrayList<Vacuna> vacunas;
	public Gatete(String nom, int edat, String nacimiento, String raza, ArrayList<Vacuna> vacunas) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.nacimiento = nacimiento;
		this.raza = raza;
		this.vacunas = vacunas;
	}
	@Override
	public String toString() {
		return "Gatete [nom=" + nom + ", edat=" + edat + ", nacimiento=" + nacimiento + ", raza=" + raza + ", vacunas="
				+ vacunas + "]";
	}
	
	

}
