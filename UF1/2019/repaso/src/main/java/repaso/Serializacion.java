package repaso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Serializacion {
	
	
	///ObjectInputStream i ObjectOutputStream
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		File f = new File("gatos.bin");
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		ArrayList<Vacuna> v = new ArrayList<>();
		v.add(new Vacuna("hepatitis"));
		Gatete g = new Gatete("kernel",2,"sabadell", "mixta", v);
		
		oos.writeObject(g);
		
		
		FileInputStream fis = new FileInputStream(f);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		try {
		while(true) {
			Object o =  ois.readObject();
			if(o instanceof Gatete) {
				Gatete g2 = (Gatete) o;
				System.out.println(g2);
			}
			
		}
		}catch(Exception e){
			System.out.println("fin del fichero");
		}
		
				
				
		
		
	}
	

}
