package repaso;

import java.io.FileReader;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JSON {
	
	public static JSONObject protectora;
	
	public static void main(String[] args) {
		
        JSONParser parser = new JSONParser();
        try {
        	//castejes a l'arrel. L'arrel pot ser un objecte o un array json, dep�n
            Object obj = parser.parse(new FileReader("protectora.json"));
            protectora = (JSONObject) obj;
            System.out.println(protectora);
            vacunar("kernel","vacuna de ser guapo");
            vacunar("Lluni","hepatitis Lunar");
            vacunar("Lluis","FOL");
            adoptar("kernel");
            System.out.println(protectora);

        }catch(Exception e) {
        	e.printStackTrace();
        }

		
	}
	
	public static void vacunar(String nom, String vacuna) {
		//accedimos al array de gatetes
		JSONArray gatetes = (JSONArray) protectora.get("gatetes");
		for(Object o : gatetes) {
			//el casteo se hace siempre
			JSONObject gatete = (JSONObject) o;
			String nombreGatete = (String) gatete.get("nom");
			if(nombreGatete.equals(nom)) {
				
				JSONArray vacunas = (JSONArray) gatete.get("vacunas");
				if(!vacunas.contains(vacuna)) {
					vacunas.add(vacuna);
				}
				
				
			}
			
		}
	}
	
	/**
	 * Adopta el gat si t� les vacunes al dia (com a m�nim 3)
	 * @param nom
	 */
	public static void adoptar(String nom) {
		JSONArray gatetes = (JSONArray) protectora.get("gatetes");
		for (Iterator iterator = gatetes.iterator(); iterator.hasNext();) {
			JSONObject object = (JSONObject) iterator.next();
			if(object.get("nom").equals(nom) && ((JSONArray)object.get("vacunas")).size() >= 3) {
				iterator.remove();
				long gatsA= (Long) protectora.get("gatsAdoptats");
				gatsA++;
				protectora.put("gatsAdoptats", gatsA);
			}
		}
	}
	
	

}
