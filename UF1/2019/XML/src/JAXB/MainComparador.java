package JAXB;

import java.util.ArrayList;
import java.util.Collections;

public class MainComparador {
	
	public static void main(String[] args) {
		
		GateteSimple g1 = new GateteSimple("Kernel", 10, "mi nariz mientras duermo");
		GateteSimple g2 = new GateteSimple("Proxy", 4, "pienso suave");
		GateteSimple g3 = new GateteSimple("Ala", 4, "Salmon");
		GateteSimple g4 = new GateteSimple("Fuzzy", 8, "At�n de lata");
		
		ArrayList<GateteSimple> gatetes = new ArrayList<>();
		
		System.out.println(g1.nom.compareTo(g1.nom));
		
		gatetes.add(g1);gatetes.add(g2);gatetes.add(g3);gatetes.add(g4);
		
		System.out.println(gatetes);
		
		Collections.sort(gatetes);
		
		System.out.println(gatetes);
		
		
		
	}

}
