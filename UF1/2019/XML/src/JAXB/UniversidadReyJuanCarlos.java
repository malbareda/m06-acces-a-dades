package JAXB;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement (name="UniversidadReyJuanCarlos")
public class UniversidadReyJuanCarlos {
    private ArrayList<Master> masters = new ArrayList<>();
	private String rector;
	private String direccion;
    
	
	
    
	public UniversidadReyJuanCarlos() {
		super();
	}

	public UniversidadReyJuanCarlos(ArrayList<Master> masters, String rector, String direccion) {
		super();
		this.masters = masters;
		this.rector = rector;
		this.direccion = direccion;
	}

	
	
	public String getRector() {
		return rector;
	}

	public void setRector(String rector) {
		this.rector = rector;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@XmlElementWrapper(name = "masters")
    @XmlElement(name = "master")
    public List<Master> getMasters() {
        return masters;
    }

    public void setMasters(ArrayList<Master> masters) {
        this.masters = masters;
    }
}