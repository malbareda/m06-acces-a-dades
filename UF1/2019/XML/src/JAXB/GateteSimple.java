package JAXB;

public class GateteSimple implements Comparable{
	
	String nom;
	int edat;
	String menjatPreferit;
	
	public GateteSimple(String nom, int edat, String menjatPreferit) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.menjatPreferit = menjatPreferit;
	}

	@Override
	public String toString() {
		return "GateteSimple [nom=" + nom + ", edat=" + edat + ", menjatPreferit=" + menjatPreferit + "]";
	}

	@Override
	public int compareTo(Object arg0) {
		GateteSimple otro = (GateteSimple) arg0;
		if(this.edat!=otro.edat) return this.edat-otro.edat;
		else return this.nom.compareTo(otro.nom);
	}
	
	


}
