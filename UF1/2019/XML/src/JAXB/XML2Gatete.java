package JAXB;

import java.io.File;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XML2Gatete {

	public static void main(String[] args) {

		/// <magia negra>

		try {
			// crees un fitxer
			File file = new File("gatete.xml");
			// crees un nou contexte. El contexte ser� de la classe ARREL
			JAXBContext jaxbContext = JAXBContext.newInstance(Gatete.class);
			// crees un unmarshaller. Unmarshaller es llegir i marshaller escriure
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// fas un unmarshal i castejes directament a la classe que vols
			Gatete gatete = (Gatete) jaxbUnmarshaller.unmarshal(file);
			/// </magia negra>

			gatete.edat++;
			System.out.println(gatete);

			//l'iterador es crea automatiacament fent for - ctrl-space - iterate collection
			for (Iterator iterator = gatete.vacunas.iterator(); iterator.hasNext();) {
				//s'obt� el seg�ent objecte mitjan�ant iterator.next, s'ha de castejar
				Vacuna v = (Vacuna) iterator.next();
				if (v.nom.contains("Hepatitis")) {
					//s'esborra l'objecte al qual apunta l'iterador. �s l'unica forma d'esborrar un objecte mentre recorres l'ArrayList
					iterator.remove();
				}

			}

			File file2 = new File("gatete2.xml");
			//El Marshaller funciona amb exactament el mateix context que el Unmarshaller
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			//El m�tode Marshal li passes l'objecte ARREL i l'arxiu.
			jaxbMarshaller.marshal(gatete, file2);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}
}
