package JAXB;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Modul {
    private ArrayList<Alumne> als = new ArrayList<>();
	private String codi;
	private String nom;
	private String professor;
    
	
	
    
	@XmlAttribute  
    public String getCodi() {
		return codi;
	}

	public void setCodi(String codi) {
		this.codi = codi;
	}

	@XmlElement
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement
	public String getProfessor() {
		return professor;
	}

	public void setProfessor(String professor) {
		this.professor = professor;
	}

	//wrapper: Es un envoltori (la llista)
	@XmlElementWrapper(name = "alumnes")
	//element : cada un dels elements de dintre
    @XmlElement(name = "alumne")
    public List<Alumne> getAls() {
        return als;
    }

    public void setAls(ArrayList<Alumne> als) {
        this.als = als;
    }
}