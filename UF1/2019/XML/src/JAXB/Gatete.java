package JAXB;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//Abans de la Classe. Sempre
@XmlRootElement
//Ordre dels elements. Si no es alfab�tic.
@XmlType(propOrder={"nom", "edat" , "longitud","sexo","vacunas" })
public class Gatete {
	
	String nom;
	int edat;
	double longitud;
	String sexo;
	ArrayList<Vacuna> vacunas;
	
	
	public Gatete() {
		super();
	}
	
	
	//Wrapper. Llista d'objectes
	@XmlElementWrapper(name="vacunas")
	//cada un dels objectes individuals
	@XmlElement(name="vacuna")
	public ArrayList<Vacuna> getVacunas() {
		return vacunas;
	}



	public void setVacunas(ArrayList<Vacuna> vacunas) {
		this.vacunas = vacunas;
	}



	//els tags de XMLElement es posen sobre el getter
	@XmlElement
	public String getSexo() {
		return sexo;
	}


	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	//Si el nom en XML es igual al nom en la variable no cal posar el par�metre (name = "nom )
	@XmlElement
	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}

	//El parametre name es per quan es diuen diferent el nom de XML i la variable. Aqu� es innecessari
	@XmlElement(name = "edat")
	public int getEdat() {
		return edat;
	}


	public void setEdat(int edat) {
		this.edat = edat;
	}

	@XmlElement
	public double getLongitud() {
		return longitud;
	}


	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}



	@Override
	public String toString() {
		return "Gatete [nom=" + nom + ", edat=" + edat + ", longitud=" + longitud + ", sexo=" + sexo + ", vacunas="
				+ vacunas + "]";
	}


	
	
	
	
	
	
	
	
	

}
