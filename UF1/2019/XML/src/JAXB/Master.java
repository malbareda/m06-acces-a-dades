package JAXB;



import javax.xml.bind.annotation.XmlAttribute;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  

@XmlRootElement 
public class Master {
	private boolean VIP;
	private String nombre;
	private String profesor;
	private int plazas;
	
	
	public Master() {
		super();
	}


	public Master(boolean vIP, String nombre, String profesor, int plazas) {
		super();
		VIP = vIP;
		this.nombre = nombre;
		this.profesor = profesor;
		this.plazas = plazas;
	}


	//atributo - siempre
	@XmlAttribute  
	public boolean isVIP() {
		return VIP;
	}


	public void setVIP(boolean vIP) {
		VIP = vIP;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getProfesor() {
		return profesor;
	}


	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}


	public int getPlazas() {
		return plazas;
	}


	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}
}
	
	