package JAXB;

import java.io.File;  
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;  

public class Xml2Alumne {  
    public static void main(String[] args) {  
   
     try {  
   
    	///<magia negra>
    	 
    	//crees un fitxer
        File file = new File("alumne.xml");
        //crees un nou contexte. El contexte ser� de la classe ARREL
        JAXBContext jaxbContext = JAXBContext.newInstance(Alumne.class);
        //crees un unmarshaller. Unmarshaller es llegir i marshaller escriure
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller(); 
        //fas un unmarshal i castejes directament a la classe que vols
        Alumne alum= (Alumne) jaxbUnmarshaller.unmarshal(file);  
        ///</magia negra>
        
        
        System.out.println(alum.getId()+" "+alum.getNombre()+" "+alum.getAltura());  
          
        
      } catch (JAXBException e) {  
        e.printStackTrace();  
      }  
   
    }  
}  
