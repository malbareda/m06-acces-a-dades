package JAXB;

import java.io.FileOutputStream;  
  
import javax.xml.bind.JAXBContext;  
import javax.xml.bind.Marshaller;  

public class Alumne2Xml {
	
	public static void main(String[] args) throws Exception{  
	    JAXBContext contextObj = JAXBContext.newInstance(Alumne.class);  
	  
	    Marshaller marshallerObj = contextObj.createMarshaller();  
	    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    marshallerObj.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	  
	    Alumne dani = new Alumne(201,"Dani","Su�e","2DAM",5, 1.71);
	    FileOutputStream fos = new FileOutputStream("dani.xml");
	    
	    marshallerObj.marshal(dani, fos);  
	       
	}  

}
