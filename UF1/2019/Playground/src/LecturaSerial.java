import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;

public class LecturaSerial {
	
	public static void main(String[] args) throws ClassNotFoundException, IOException {
		
		FileInputStream fis = new FileInputStream("Hogwarts.dat");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Random r = new Random();
		
		
		//ternaria
		/// variable = condicion(siempre booleano)? asignacion si true : asignacion si false;
		try {
			while(true) {
				Object o = ois.readObject();
				if(o instanceof Mago) {
				Mago m = (Mago) o;
				System.out.println(m);}
				else {
					Muggle m = (Muggle) o;
					System.out.println(m);
				}
			
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("fin del fichero");
		}
	}

}
