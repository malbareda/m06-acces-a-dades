import java.util.Scanner;

public class Cintas {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		while (sc.hasNext()) {
			int max = sc.nextInt();
			int k = sc.nextInt();
			int[] array = new int[k];
			for (int i = 0; i < k; i++) {
				array[i] = sc.nextInt();
			}
			int a = 0, b = 0, i = 0;

			a += array[0];
			boolean res = go(array, max, a, b, 1);
			if (res) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}

		}

	}

	public static boolean go(int[] array, int max, int a, int b, int i) {

		if (a > max || b > max) {
			return false;
		} else if (i == array.length) {
			return true;
		} else {
			boolean posoA = go(array, max, a + array[i], b, i + 1);
			boolean posoB = go(array, max, a, b + array[i], i + 1);
			return posoA || posoB;
		}

	}

}
