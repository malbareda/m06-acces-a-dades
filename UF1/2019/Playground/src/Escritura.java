import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Escritura {
	
	public static void main(String[] args) throws IOException {
		
		File f = new File("texto2.txt");
		FileWriter fw = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(fw);
		
		long init = System.currentTimeMillis();
		
		/*for (int i = 0; i < 100000000; i++) {
			bw.write("gatete");
			bw.newLine();
		}*/
		for (int i = 0; i < 100000000; i++) {
			fw.write("gatete");
			bw.newLine();
		}
		
		
		long fin = System.currentTimeMillis();
		
		System.out.println(fin-init);
		
		
		
	}

}
