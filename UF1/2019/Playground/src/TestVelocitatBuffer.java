import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class TestVelocitatBuffer {

	public static void main(String[] args) {
		

		try (FileWriter fileWriter = new FileWriter("provaFile.txt");) {
			long temps = testVelocitat(fileWriter);
			System.out.println("provaFile.txt creat en " + temps + " ms");
		} catch (IOException e) {
			System.err.println(e);
		}
		
		try (BufferedWriter bufferedWriter =
				new BufferedWriter(new FileWriter("provaBuffered.txt"));) {
			long temps = testVelocitat(bufferedWriter);
			System.out.println("provaBuffered.txt creat en " + temps + " ms");
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public static long testVelocitat(Writer writer) throws IOException {
		long tempsInicial=System.currentTimeMillis();
		for(int i = 0; i < 10000000; i++) {
			writer.write("prova");
			writer.write('\n');
		}
		long tempsFinal=System.currentTimeMillis();
		return tempsFinal-tempsInicial;
	}
}
