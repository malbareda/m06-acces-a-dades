import java.util.ArrayList;
import java.util.Collections;

public class DiversionConArrayLists {
	
	public static void main(String[] args) {
		
		//Una arraylist puede ser de cualquier clase
		ArrayList<String> list1 = new ArrayList<>();
		ArrayList<Integer> list2 = new ArrayList<>();
		ArrayList<Tortilla> desayuno = new ArrayList<>();
		
		//Array es estatico, Lista es dinamico. No tiene un tama�o fijo
		
		
		System.out.println(list1.size());
		//add, a�ade al final de la lista
		list1.add("hola");
		System.out.println(list1.size());
		//metodos cucos
		
		//a�adir en el punto que quieras
		list1.add("adios");
		list1.add("marc");
		list1.add("egocentrismo");
		list1.add("gatete");
		list1.add("patata");
		list1.add("arraylists");
		list1.add("ElenaQueHasHecho");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		
		
		System.out.println(list1);
		
		list1.remove(3);
		System.out.println(list1);
		//borra el primero
		list1.remove("gatete");
		System.out.println(list1);
		//borralos todos
		while(list1.remove("gatete"));
		
		System.out.println(list1);
		
		//contains - boolean si est� en lista
		System.out.println("contains "+list1.contains("kebabs"));
		//indexof - posicion en lista del primero. -1 si no est�
		System.out.println("indexof "+list1.indexOf("kebabs"));
		
		//metodos estaticos
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		list1.add("gatete");
		
		
		System.out.println(Collections.frequency(list1,"gatete"));
		
		Collections.reverse(list1);
		System.out.println(list1);
		
		Collections.sort(list1);
		System.out.println(list1);
		
		//max y min, maximo y minimo
		System.out.println(Collections.max(list1));
		
		Collections.replaceAll(list1, "gatete", "perrete");
		System.out.println(list1);
		
		
		
		
		
		
		
		
	}

}
