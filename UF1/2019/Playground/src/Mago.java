import java.io.Serializable;

public class Mago implements Serializable{
	
	
	String nombre;
	int edad;
	String animalFavorito;
	Varita vara;
	
	public Mago(String nombre, int edad, String animalFavorito, Varita vara) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.animalFavorito = animalFavorito;
		this.vara = vara;
	}

	@Override
	public String toString() {
		return "Mago [nombre=" + nombre + ", edad=" + edad + ", animalFavorito=" + animalFavorito + ", vara=" + vara
				+ "]";
	}
	
	
	
	

}
