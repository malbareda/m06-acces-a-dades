import java.io.Serializable;

public class Varita implements Serializable {
	
	long centimetros;
	float dureza;
	String material;
	
	
	public Varita(long centimetros, float dureza, String material) {
		super();
		this.centimetros = centimetros;
		this.dureza = dureza;
		this.material = material;
	}


	@Override
	public String toString() {
		return "Varita [centimetros=" + centimetros + ", dureza=" + dureza + ", material=" + material + "]";
	}
	
	
	
	

}
