import java.io.Serializable;

public class Muggle implements Serializable{
	
	String nombre;
	int edad;
	String animalFavorito;
	@Override
	public String toString() {
		return "Muggle [nombre=" + nombre + ", edad=" + edad + ", animalFavorito=" + animalFavorito + "]";
	}
	public Muggle(String nombre, int edad, String animalFavorito) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.animalFavorito = animalFavorito;
	}

}
