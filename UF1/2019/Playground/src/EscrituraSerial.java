import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Random;

public class EscrituraSerial {
	
	public static void main(String[] args) throws IOException {
		
		FileOutputStream fos = new FileOutputStream("Hogwarts.dat");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		Random r = new Random();
		
		
		//ternaria
		/// variable = condicion(siempre booleano)? asignacion si true : asignacion si false;
		for (int i = 0; i < 10; i++) {
			if(r.nextBoolean()) {
				Mago m = new Mago("harry", r.nextInt(), "lechuza", new Varita(r.nextLong(), r.nextFloat(), r.nextBoolean()?"cedro":"pino"));
				oos.writeObject(m);
			}else {
				Muggle m = new Muggle("no-harry", r.nextInt(), "gatos");
				oos.writeObject(m);
			}
			
		}
		
		
	}

}
