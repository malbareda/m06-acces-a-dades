package examen;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="gins")
public class Gins {
	private ArrayList<Gin> gin;
	
	public Gins() {
		super();
	}

	public Gins(ArrayList<Gin> gin) {
		super();
		this.gin = gin;
	}

	@XmlElementWrapper(name="gins")
	@XmlElement(name="gin")
	public ArrayList<Gin> getGin() {
		return gin;
	}

	public void setGin(ArrayList<Gin> gin) {
		this.gin = gin;
	}

	@Override
	public String toString() {
		return "Gins [gin=" + gin + "]";
	}

}
