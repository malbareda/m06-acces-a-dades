package examen;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Gin {
	
	private String nom; 
	private String graduacio;
	private String sabor;
	private ArrayList<Ingredients> ingredients;

	
	public Gin(String nom, String graduacio, String sabor, ArrayList<Ingredients> ingredients){
		super();
		this.nom = nom;
		this.graduacio = graduacio;
		this.sabor = sabor;
		this.ingredients = ingredients;
	}

	public Gin() {
		super();
	}

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement(name = "graduacio")
	public String getGraduacio() {
		return graduacio;
	}

	public void setGraduacio(String graduacio) {
		this.graduacio = graduacio;
	}

	@XmlElement(name = "sabor")
	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	
	@XmlElementWrapper(name="ingredients")
	@XmlElement(name="ingredient")
	public ArrayList<Ingredients> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<Ingredients> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return "Gin [nom=" + nom + ", graduacio=" + graduacio + ", sabor=" + sabor + ", ingredients=" + ingredients
				+ "]";
	}

	
	
	
	
}
