package examen;

import java.io.File;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XML {
	
	public static void showXML () throws JAXBException{
		Gins gin = llegir("gins.xml");
		escriureCopia(gin);
	}
	
	public static void afegirIngredient(String nomGin, String nomIngredient, String volum) throws JAXBException{
		Gins gin = llegir("gins.xml");
		ArrayList<Gin> gins = gin.getGin();
		ArrayList<Ingredients> nousIngredients = new ArrayList<Ingredients>();
		Ingredients ing = new Ingredients(nomIngredient, nomGin);
		System.out.println(gins);
		for (Gin j : gins) {
			if (j.getNom().equals(nomGin)) {
				nousIngredients.addAll(j.getIngredients());
				nousIngredients.add(ing);
				j.setIngredients(nousIngredients);
			}
		}

		gin.setGin(gins);
		escriure(gin);
	}
	
	public static void saborPrincipal(String nomGin) throws JAXBException {
		Gins gin = llegir("gins.xml");
		ArrayList<Gin> gins = gin.getGin();
		
		for (Gin j : gins) {
			if (j.getNom().equals(nomGin)) {
				
				ArrayList<Ingredients> nousIngredients = j.getIngredients();

			}
		}
		
	}
	
		public static Gin comprovacio(String nomGin) throws JAXBException {

			File f = new File("gins.xml");
			JAXBContext context = JAXBContext.newInstance(Gins.class);
			Unmarshaller um = context.createUnmarshaller();
			Gins gin = (Gins) um.unmarshal(f);
			ArrayList<Gin> gins = gin.getGin();

			Gin jj = null;

			for (Gin j : gins) {

				if (j.getNom().equals(nomGin)) {

					jj = j;
					return jj;
				}

			}

			return null;

		}

		public static Gins llegir(String xmlfile) throws JAXBException {

			File f = new File(xmlfile);
			JAXBContext context = JAXBContext.newInstance(Gins.class);
			Unmarshaller um = context.createUnmarshaller();
			Gins gin = (Gins) um.unmarshal(f);

			return gin;

		}

		public static void escriure(Gins j) throws JAXBException {

			File f = new File("gins.xml");
			JAXBContext context = JAXBContext.newInstance(Gin.class);
			Marshaller m = context.createMarshaller();
			m.marshal(j, f);

		}
		
		public static void escriureCopia(Gins j) throws JAXBException {

			File f = new File("gins.xml");
			JAXBContext context = JAXBContext.newInstance(Gin.class);
			Marshaller m = context.createMarshaller();
			m.marshal(j, f);

		}

}
