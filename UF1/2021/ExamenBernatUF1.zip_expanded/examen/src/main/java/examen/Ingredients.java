package examen;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


public class Ingredients {
	
	private String nom;
	private String vol;

	public Ingredients(){
		super();

	}

	public Ingredients(String nom, String vol) {
		super();
		this.nom = nom;
		this.vol = vol;
	}

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlAttribute
	public String getvol() {
		return vol;
	}

	@Override
	public String toString() {
		return "Ingredients [nom=" + nom + "]";
	}
	
	

}
