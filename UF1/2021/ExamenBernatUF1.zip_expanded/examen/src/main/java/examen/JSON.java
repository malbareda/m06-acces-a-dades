package examen;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JSON {
	
	public static void showJSON(String nomFitxer, String nomFitxerCopia) throws IOException{
		
		JSONArray arr = llegir(nomFitxer);

		for (Object o : arr) {

			HashMap<String, String> diccionari = new HashMap<String, String>();

			JSONObject gin = (JSONObject) o;
			String nom = gin.get("nom").toString();
			String graduacio = gin.get("graduacio").toString();
			String sabor = gin.get("sabor").toString();

			diccionari.put("nom", nom);
			diccionari.put("graduacio", graduacio);
			diccionari.put("sabor", sabor);

			System.out.println("Enemic: [nom = " + diccionari.get("nom") + ", graduacio = "
					+ diccionari.get("graduacio") + ", sabor = " + diccionari.get("sabor") + "]");
		}
		
		escriure(nomFitxerCopia, arr);
		
	}
		public static JSONArray llegir(String nomFitxer) throws IOException {

			JSONParser parser = new JSONParser();
			JSONArray arr = new JSONArray();

			try {

				FileReader reader = new FileReader(nomFitxer);

				arr = (JSONArray) parser.parse(reader);

				JSONObject oj = null;

				for (Object o : arr) {

					oj = (JSONObject) o;
					String object = oj.toString();

				}

				return arr;

			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;

		}

		public static void escriure(String nomFitxer, JSONArray arr) {

			try (FileWriter writer = new FileWriter(nomFitxer)) {

				if (arr != null) {

					writer.write(arr.toJSONString());
					writer.flush();

					System.out.println("S'ha escrit correctament");

				} else {

					System.err.println("No s'ha pogut escriure");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

}
