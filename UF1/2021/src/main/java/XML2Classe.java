import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XML2Classe {
	
	public static void main(String[] args) throws JAXBException {
		
		
		File f = new File("dani.xml");
		//magia
		JAXBContext context = JAXBContext.newInstance(Persona.class);
		//el reader se llama unmarshaller
		Unmarshaller um = context.createUnmarshaller();
		Persona dani = (Persona) um.unmarshal(f);
		
		System.out.println(dani);
		
		
		dani.setEdat(23);
		Dret d = new Dret("Dret a un burrito", 2021);
		dani.getDrets().add(d);
		
		
		System.out.println(dani);
		
		Marshaller m = context.createMarshaller();
		m.marshal(dani, f);
		
		
		
	}

}
