

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//etiquetas
//@algo

//arrel. Es posa a sobre de la classe
@XmlRootElement(name="persona")
public class Persona {
	

	private String nom;
	private int edat;
	private String sexe;
	
	private ArrayList<Dret> drets;
	
	
	
	public Persona() {
		super();
	}



	public Persona(String nom, int edat, String sexe, ArrayList<Dret> drets) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.sexe = sexe;
		this.drets = drets;
	}


	@XmlElement(name="nom")
	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	@XmlElement(name="edat")
	public int getEdat() {
		return edat;
	}



	public void setEdat(int edat) {
		this.edat = edat;
	}



	@XmlElement(name="sexe")
	public String getSexe() {
		return sexe;
	}



	public void setSexe(String sexe) {
		this.sexe = sexe;
	}



	public ArrayList<Dret> getDrets() {
		return drets;
	}

	@XmlElementWrapper(name="drets")
	@XmlElement(name="dret")
	public void setDrets(ArrayList<Dret> drets) {
		this.drets = drets;
	}



	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", edat=" + edat + ", sexe=" + sexe + ", drets=" + drets + "]";
	}
	
	
	
	

}

