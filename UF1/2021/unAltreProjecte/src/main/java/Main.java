import java.io.File;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Main {
	
	public static void main(String[] args) throws JAXBException {
		
		File f = new File("burrito.xml");
		//magia
		JAXBContext context = JAXBContext.newInstance(Burrito.class);
		//el reader se llama unmarshaller
		Unmarshaller um = context.createUnmarshaller();
		Burrito b = (Burrito) um.unmarshal(f);
		
		System.out.println(b);
		
		
		b.setPreu(3.5f);
		for (Iterator iterator = b.getSalses().iterator(); iterator.hasNext();) {
			String s = (String) iterator.next();
			if(s.equals("chimichurri")) {
				iterator.remove();
			}
			
		}
		
		System.out.println(b);
		
		Marshaller m = context.createMarshaller();
		m.marshal(b, f);
		
	}

}
