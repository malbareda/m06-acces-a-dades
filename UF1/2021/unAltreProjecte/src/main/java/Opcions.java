import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
//propOrder. ordena. Has de posar el nom de LES VARIABLES A JAVA, NO A XML
@XmlType(propOrder= {"vega","lactosa","gluten","tamany"})
public class Opcions {
	
	private boolean vega;
	private boolean lactosa;
	private boolean gluten;
	private String tamany;
	public Opcions(boolean vega, boolean lactosa, boolean gluten, String tamany) {
		super();
		this.vega = vega;
		this.lactosa = lactosa;
		this.gluten = gluten;
		this.tamany = tamany;
	}
	public Opcions() {
		super();
	}
	
	@XmlElement
	public boolean isVega() {
		return vega;
	}
	public void setVega(boolean vega) {
		this.vega = vega;
	}
	@XmlElement
	public boolean isLactosa() {
		return lactosa;
	}
	public void setLactosa(boolean lactosa) {
		this.lactosa = lactosa;
	}
	@XmlElement
	public boolean isGluten() {
		return gluten;
	}
	public void setGluten(boolean gluten) {
		this.gluten = gluten;
	}
	@XmlElement
	public String getTamany() {
		return tamany;
	}
	public void setTamany(String tamany) {
		this.tamany = tamany;
	}
	@Override
	public String toString() {
		return "Opcions [vega=" + vega + ", lactosa=" + lactosa + ", gluten=" + gluten + ", tamany=" + tamany + "]";
	}
	
	
	

}
