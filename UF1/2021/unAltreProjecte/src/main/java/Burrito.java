import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Burrito {
	
	private String nom;
	private float preu;
	private ArrayList<String> salses = new ArrayList<String>();
	private Opcions opcions;
	public Burrito(String nom, float preu, ArrayList<String> salses, Opcions opcions) {
		super();
		this.nom = nom;
		this.preu = preu;
		this.salses = salses;
		this.opcions = opcions;
	}
	public Burrito() {
		super();
	}
	@XmlAttribute
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@XmlElement
	public float getPreu() {
		return preu;
	}
	public void setPreu(float preu) {
		this.preu = preu;
	}
	@XmlElementWrapper(name="salses")
	@XmlElement(name="salsa")
	public ArrayList<String> getSalses() {
		return salses;
	}
	public void setSalses(ArrayList<String> salses) {
		this.salses = salses;
	}
	@XmlElement(name="opcions")
	public Opcions getOpcions() {
		return opcions;
	}
	public void setOpcions(Opcions opcions) {
		this.opcions = opcions;
	}
	@Override
	public String toString() {
		return "Burrito [nom=" + nom + ", preu=" + preu + ", salses=" + salses + ", opcions=" + opcions + "]";
	}
	
	

}
