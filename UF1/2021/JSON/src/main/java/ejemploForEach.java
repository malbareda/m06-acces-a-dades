import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class ejemploForEach {
	
	public static void main(String[] args) {
		
		int[] array = {1,2,3,4,5,6,7,8,9};
		ArrayList<String> lista = new ArrayList(Arrays.asList("hola","Sergio","Iker","Albert","Juan","Alex"));
		
		
		System.out.println(lista);
		int i =0;
		//foreach solo con lista o array
		for (String elem : lista) {
			i++;
			System.out.println(i+": "+ elem);
		}
		
		int acc =0;
		for (int elem : array) {
			System.out.println("El elemento es un "+elem);
			acc+=elem;
		}
		System.out.println(acc);
		
		
		
		
	}

}
