import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class LlegirJSON {
	
	public static void main(String[] args) {
		
		//5 tipus de dades en JSON -> Java
		//Object -> Diccionari (HashMap)
		//Array -> List
		//Numeric -> float
		//String -> String
		//Boolean  -> boolean
		
		//un diccionari es un conjunt de parelles clau-valor
		HashMap<String,String> diccionari = new HashMap<String,String>();
		
		//1. no es pot repetir
		//2. clau -> valor
		
		diccionari.put("nom", "Nunu");
		System.out.println(diccionari.get("nom"));
		diccionari.put("nom", "Anjuli");
		diccionari.put("diferenciaAltura","si");
		
		System.out.println(diccionari.get("nom"));
		System.out.println(diccionari.keySet());
		System.out.println(diccionari.entrySet());
		System.out.println(diccionari.size());
		
		
		//llegir JSON
		
		JSONParser parser = new JSONParser();
		
		
		try {
			
			JSONObject o = (JSONObject) parser.parse(new FileReader("jsonDeRulo.json"));
			System.out.println(o);
			
			System.out.println(o.get("nom"));
			
			JSONArray integrants = (JSONArray) o.get("integrantes");
			System.out.println(integrants);
			for (Object shipeados : integrants) {
				String ships = shipeados.toString();
				System.out.println(ships);
			}
			
			
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		
		
	}

}
