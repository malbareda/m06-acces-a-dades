import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class EscriureJSON {

	public static void main(String[] args) {

		JSONArray shipeos = new JSONArray();

		JSONObject ship = new JSONObject();
		ship.put("nom", "L'anchel");
		ship.put("diferenciaAltura", 50);
		ship.put("tensionSexual", true);

		JSONArray integrantes = new JSONArray();
		integrantes.add("juli");
		integrantes.add("angel");

		ship.put("integrantes", integrantes);

		shipeos.add(ship);

		JSONObject ship2 = new JSONObject();
		ship2.put("nom", "KanePau");
		ship2.put("diferenciaAltura", 10);
		ship2.put("tensionSexual", true);
		ship2.put("aficionPrincipal", "palizas");

		JSONArray integrantes2 = new JSONArray();
		integrantes2.add("Kane");
		integrantes2.add("Pau");

		ship2.put("integrantes", integrantes2);

		shipeos.add(ship2);

		try (FileWriter file = new FileWriter("shipeos.json")) {

			file.write(shipeos.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
