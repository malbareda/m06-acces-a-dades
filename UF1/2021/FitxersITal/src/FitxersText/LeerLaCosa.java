package FitxersText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeerLaCosa {
	
	public static void main(String[] args) throws IOException {
		
		
		File f = new File("cosa.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		
		while(br.ready()) {
			System.out.println(br.readLine());
		}
		br.close();
	}

}
