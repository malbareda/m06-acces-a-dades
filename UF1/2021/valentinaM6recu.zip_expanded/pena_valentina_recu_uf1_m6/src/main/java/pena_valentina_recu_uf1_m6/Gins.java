package pena_valentina_recu_uf1_m6;


import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "gins")
public class Gins {
	private ArrayList<Ginebra> gins;
	public Gins() {
		super();
	}
	public Gins(ArrayList<Ginebra> gins) {
		super();
		this.gins = gins;
	}
	@XmlElementWrapper(name = "giins")
	@XmlElement(name="gin")
	public ArrayList<Ginebra> getGins() {
		return gins;
	}
	public void setGins(ArrayList<Ginebra> gins) {
		this.gins = gins;
	};
	
}
