package pena_valentina_recu_uf1_m6;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ingredient")
public class Ingredient {
	private String nom;
	private String vol;

	public Ingredient() {
		super();
	}

	public Ingredient(String nom, String vol) {
		super();
		this.nom = nom;
		this.vol = vol;
	}
	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	@XmlAttribute(name = "vol")
	public String getVol() {
		return vol;
	}

	public void setVol(String vol) {
		this.vol = vol;
	}

	@Override
	public String toString() {
		return "Ingredient [nom=" + nom + ", vol=" + vol + "]";
	}

}
