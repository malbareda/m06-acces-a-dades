package pena_valentina_recu_uf1_m6;

import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class TestJSON {

	public static void main(String[] args)
			throws FileNotFoundException, IOException, org.json.simple.parser.ParseException {
		// TODO Auto-generated method stub
		/*
		 * Llegeix el fitxer “gin.json”, escriu per pantalla el nom de la ginebra i la
		 * seva graduació, i guarda el fitxer JSON sencer a gin_copia.json (0.5p) (no
		 * importa que no mantingui l’ordre)
		 * 
		 */
		JSONParser parser = new JSONParser();
		String nomFitxer = "gins.json";
		JSONArray gins = (JSONArray) parser.parse(new FileReader(nomFitxer));
		for (Object o : gins) {
			JSONObject ginebra = (JSONObject) o;
			String nom = (String) ginebra.get("nom");
			String graduacio = (String) ginebra.get("graduacio");
			System.out.println(nom + " amb graduacio " + graduacio);
		}
		try (FileWriter file = new FileWriter("gin_copia.json")) {
			file.write(gins.toJSONString());
			file.flush();
			System.out.println("SE HA ESCRITO");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void crearIngredient(String nomIngr, Double volum) throws FileNotFoundException, IOException, org.json.simple.parser.ParseException {
		/*
		 * Fes la funció “crearIngredient(String nomIngr, Double volum) que cerca si hi
		 * ha algun ingredient en alguna ginebra que tingui el mateix nomIngr i el
		 * mateix volum proporcionat. Si no els troba, crea un nou fitxer JSON
		 * nomIngredient.json amb només l’ingredient (és a dir, només la String) i un
		 * nou fitxer JSON volum.json amb només el volum (és a dir, només el Double)
		 */
		JSONParser parser = new JSONParser();
		String nomFitxer = "gins.json";
		JSONArray gins = (JSONArray) parser.parse(new FileReader(nomFitxer));
		for (Object o : gins) {
			JSONObject ginebra = (JSONObject) o;
			JSONArray ingredients = (JSONArray) ginebra.get("ingredients");
			for (Object a : ingredients) {
				JSONObject ingredient = (JSONObject) a;
				Double volum1 = (Double) ingredient.get("vol");
				if(ingredient.get("nom").equals("nom")&&volum.equals(volum1)) {
					System.out.println("si existeix");
				}
			}
		}
	}
}
