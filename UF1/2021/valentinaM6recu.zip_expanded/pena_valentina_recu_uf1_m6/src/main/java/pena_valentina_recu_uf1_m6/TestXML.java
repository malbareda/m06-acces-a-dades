package pena_valentina_recu_uf1_m6;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class TestXML {

	public static void main(String[] args) throws JAXBException {
		// TODO Auto-generated method stub
		llegirNomGin();
		afegirIngredient("Puerto de Indias", "dildo", "1%");
		System.out.println(saborPrincipal("Puerto de Indias"));
	}

	public static void afegirIngredient(String nomGin, String nomIngredient, String volum) {
		/*
		 * 1.3 Fes la funció “afegirIngredient(String nomGin, String nomIngredient,
		 * String volum)” que crea un nou ingredient amb els camps proporcionats i
		 * l’afegeix a la ginebra amb nom proporcionat.
		 */
		Gins gins;
		try {
			gins = ReadGinebraXML();
			ArrayList<Ginebra> alcoholes = gins.getGins();
			Ingredient in = new Ingredient();
			in.setNom(nomIngredient);
			in.setVol(volum);
			for (Ginebra gin : alcoholes) {
				if (gin.getNom().equals(nomGin)) {
					gin.getIngredients().add(in);
					System.out.println(gin.getNom() + " " + gin.getIngredients());
				}

				WriteXMLGinebra(gins, "gins.xml");
			}
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String saborPrincipal(String nomGin) throws JAXBException {
		/*
		 * 1.3 Fes una funció “saborPrincipal(String nomGin)” que compara el sabor de la
		 * ginebra amb els ingredients. Si hi ha un ingredient amb el mateix nom que el
		 * sabor, retorna el volum Si no hi ha un ingredient amb el mateix nom que el
		 * sabor, retorna “NO” Si la ginebra no té sabor, retorna “ORIGINAL”
		 * 
		 */
		Gins gins;
		gins = ReadGinebraXML();
		ArrayList<Ginebra> alcoholes = gins.getGins();
		for (Ginebra gin : alcoholes) {
			if (gin.getNom().equals(nomGin)) {
				for (Ingredient in : gin.getIngredients()) {
					if (in.getNom().equals(gin.getSabor())) {
						WriteXMLGinebra(gins, "gins.xml");
						return in.getVol();
					}
					if (gin.getSabor() == null) {
						WriteXMLGinebra(gins, "gins.xml");
						return "ORIGINAL";
					}
				}
				WriteXMLGinebra(gins, "gins.xml");
				return "NO";
			}

		}
		return "No s'ha trobat la ginebra";
	}

	public static void llegirNomGin() throws JAXBException {
		/*
		 * 1.2. Llegeix “gin.xml”, escriu per pantalla el nom de la ginebra i la seva
		 * graduació, i guarda el fitxer XML sencer a gin_copia.xml (0.5p) (no importa
		 * que no mantingui l’ordre)
		 */
		Gins gins = ReadGinebraXML();
		System.out.println(gins);
		System.out.println(gins.getGins());
		for (int i = 0; i < gins.getGins().size(); i++) {
			System.out.println(gins.getGins().get(i).getNom() + " amb graduacio " + gins.getGins().get(i).getNom());
		}
		WriteXMLGinebra(gins, "gin_copia.xml");
	}

	public static Gins ReadGinebraXML() throws JAXBException {
		File f = new File("gins.xml");
		JAXBContext context = JAXBContext.newInstance(Gins.class);
		Unmarshaller um = context.createUnmarshaller();
		Gins gins = (Gins) um.unmarshal(f);
		return gins;

	}

	public static void WriteXMLGinebra(Gins gins, String nomFitxer) throws JAXBException {
		File f = new File(nomFitxer);
		JAXBContext context = JAXBContext.newInstance(Gins.class);
		Marshaller m = context.createMarshaller();
		m.marshal(gins, f);
		System.out.println("SE HA ESCRITO");

	}
}
