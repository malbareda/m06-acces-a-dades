package pena_valentina_recu_uf1_m6;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "gin")
//ibaranda@ies-sabadell.cat
public class Ginebra {
	private String nom;
	private String graduacio;
	private String sabor;
	private ArrayList<Ingredient> ingredients;
	
	public Ginebra() {
		super();
	}

	public Ginebra(String nom, String graduacio, String sabor, ArrayList<Ingredient> ingredients) {
		super();
		this.nom = nom;
		this.graduacio = graduacio;
		this.sabor = sabor;
		this.ingredients = ingredients;
	}
	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	@XmlElement(name = "graduacio")
	public String getGraduacio() {
		return graduacio;
	}

	public void setGraduacio(String graduacio) {
		this.graduacio = graduacio;
	}
	@XmlElement(name = "sabor")
	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	@XmlElementWrapper(name = "ingredients")
	@XmlElement(name="ingredient")
	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return "Ginebra [nom=" + nom + ", graduacio=" + graduacio + ", sabor=" + sabor + ", ingredients=" + ingredients
				+ "]";
	}
	
}
