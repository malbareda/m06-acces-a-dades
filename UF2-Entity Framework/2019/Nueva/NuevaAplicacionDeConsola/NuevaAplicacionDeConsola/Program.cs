﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuevaAplicacionDeConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            using (var ctx = new SchoolContext())
            {
                var stud = new Student() { StudentName = "Jaume" };

                ctx.Students.Add(stud);
                ctx.SaveChanges();
            }
            */
            using (var ctx = new TestContext())
            {
                var marc = new Professor() { nom = "Marc", cognom = "Albareda" };
                var grego = new Professor() { nom = "Gregorio", cognom = "Santamaria" };
                var m6 = new Modul() { Codi = 6, nom = "Acces a Dades", horesSetmana = 4 };

                marc.Moduls.Add(m6);


                //inserts
                ctx.Modul.Add(m6);
                ctx.Professor.Add(marc);
                ctx.Professor.Add(grego);
                Console.WriteLine(marc.Moduls);
                ctx.SaveChanges();
                Console.WriteLine("Todo Ha salido a pedir de Milhouse");


                //select all
                List<Professor> listg = ctx.Professor.ToList();

                foreach (Professor p in listg)
                {

                    Console.WriteLine(p.cognom);
                    if (p.nom == "Marc")
                    {
                        Console.WriteLine("guapo");
                    }
                }


                //contar
                Console.WriteLine("nombre de professors");
                Console.WriteLine(ctx.Professor.Count());


                //select
                Professor test1 = ctx.Professor
                                    .Where(p => p.cognom == "Albareda")
                                    //en un default le pones a la funcion el velor default
                                    .FirstOrDefault();
                Console.WriteLine(test1.cognom);

                //find per clau primeria
                Professor test2 = ctx.Professor.Find(2);
                Console.WriteLine("El professor amb ID 2 es");
                Console.WriteLine(test2.cognom);
                Console.WriteLine("Prem enter");
                Console.ReadLine();


                //update
                test1.nom = "GregMarc";
                //no cal "guardar l'update", EF ho assumeix auto
                ctx.SaveChanges();



                //all
                bool flag = ctx.Professor.All(p => p.nom.StartsWith("G"));
                Console.WriteLine("Tots els professors començen amb G?");
                Console.WriteLine(flag);

                //delete
                Console.WriteLine("Esborrem el professor amb id 2");
                ctx.Professor.Remove(test2);
                ctx.SaveChanges();

                Console.WriteLine("Prem enter");
                Console.ReadLine();

            }
        }
    }
}
