﻿//using Microsoft.EntityFrameworkCore;
using System.Data.Entity;

namespace NuevaAplicacionDeConsola
{
    //context gestiona:
    /*
     * Sessions
     * Transaccions
     * Configuracio Global
     * Tots els repositoris
     * 
     * */
    public class TestContext : DbContext
    {
        //Crea el context. Crea una DB per defecte (es a dir, MS SQL Server, amb nom "Test")
        public TestContext() : base("Test")
        {

        }

        //Mapeig als POCOS.
        //Cada DbSet funciona similar a un repositori de Spring
        public DbSet<Professor> Professor { get; set; }
        public DbSet<Modul> Modul { get; set; }
        public DbSet<Curs> Curs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists); //update, pero crea la DB!
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<TestContext>());  //aquest seria un metode update pero si canvies el model (els POCOS) reinicia la DB
            Database.SetInitializer(new DropCreateDatabaseAlways<TestContext>());  //equivaldria a un create

            /*modelBuilder.Entity<Modul>()
            .HasRequired<Curs>(m => m.Curs)
            .WithMany(c => c.Modul)
            .HasForeignKey<int>(m => m.elSeuCurs)
            .WillCascadeOnDelete(false);
            */

            /*modelBuilder.Entity<Member>().HasMany(m => m.Friends).WithMany().Map(m =>
            {
                m.MapLeftKey("MemberId");
                m.MapRightKey("FriendId");
                m.ToTable("MembersFriends");
            }*/

            //relacio n->n entre Modul i Professors
            //agafes modul
            modelBuilder.Entity<Modul>().
                //dius que te n professors
                HasMany(m => m.Professors).
                //i professors també es n
                WithMany().
                //pots mapejar la relacio al teu gust
                Map(r =>
                {
                    //camp esquerra
                    r.MapLeftKey("Modulo");
                    //camp dret
                    r.MapRightKey("Profesor");
                    //nom de taula
                    r.ToTable("ProfesoresPorModulos");
                });


            //relacio 1->1 entre Professor i Curs
            //agafes curs
            modelBuilder.Entity<Curs>().
                //dius que te 1 professor (i es obligatori)
                //per tant, no pots crear un curs sense crear abans el seu profe
                HasRequired(c => c.Professor).
                //dius que professor tambe te una relacio amb curs pero es opcional
                WithOptional(p => p.cursTutoritzat).
                //configures la relacio
                Map(r =>
                {
                    //nom de la clau
                    r.MapKey("Tutor");
                });
                

            base.OnModelCreating(modelBuilder);


        }

    }
}
