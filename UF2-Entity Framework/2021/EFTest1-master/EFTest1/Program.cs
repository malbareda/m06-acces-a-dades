﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFTest1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new ProvaContext())
            {
                // CREATE - Creació d'entitats
                var marc = new Professor() { nom = "Marc", cognom = "Albareda" };
                var grego = new Professor() { nom = "Gregorio", cognom = "Santamaria" };
                var m6 = new Modul() { Codi = 6, nom = "Acces a Dades", horesSetmana = 4 };
                var m7 = new Modul() { Codi = 7, nom = "Interficies", horesSetmana = 4 };
                var segonDvi = new Curs()
                {
                    Cicle = CicleFormatiu.DAMVI,
                    Actiu = true,
                    Any = 2024,
                    Professor = marc
                };

                marc.Moduls.Add(m6);
                grego.Moduls.Add(m7);

                ctx.Modul.Add(m6);
                ctx.Modul.Add(m7);
                ctx.Professor.Add(marc);
                ctx.Professor.Add(grego);
                ctx.Curs.Add(segonDvi);
                ctx.SaveChanges();

                Console.WriteLine("\n=== RESULTAT DE LA INSERCIÓ DE DADES ===");
                Console.WriteLine("Tot ha sortit a pedir de Milhouse");

                // READ - Lectura d'entitats
                List<Professor> listg = ctx.Professor.ToList();

                Console.WriteLine("\n=== LLISTAT DE COGNOMS DE PROFESSORS ===");
                Console.WriteLine("Hauria de mostrar: Albareda, Santamaria");
                foreach (Professor p in listg)
                {
                    Console.WriteLine(p.cognom);
                    if (p.nom == "Marc")
                    {
                        Console.WriteLine("guapo");
                    }
                }

                Console.WriteLine("\n=== NOMBRE TOTAL DE PROFESSORS ===");
                Console.WriteLine("Hauria de ser 2");
                Console.WriteLine(ctx.Professor.Count());

                Console.WriteLine("\n=== CERCA DE PROFESSOR PER COGNOM (WHERE) ===");
                Console.WriteLine("Hauria de trobar: Albareda");
                Professor test1 = ctx.Professor
                                    .Where(p => p.cognom == "Albareda")
                                    .FirstOrDefault();
                Console.WriteLine(test1.cognom);

                Console.WriteLine("\n=== CERCA DE PROFESSOR PER ID (FIND) ===");
                Console.WriteLine("Hauria de trobar: Santamaria (ID=2)");
                Professor test2 = ctx.Professor.Find(2);
                Console.WriteLine(test2.cognom);

                Console.ReadLine();

                Console.WriteLine("\n=== ACTUALITZACIÓ DEL NOM D'UN PROFESSOR ===");
                Console.WriteLine("Canviem Marc a GregMarc");
                test1.nom = "GregMarc";
                ctx.SaveChanges();

                Console.WriteLine("\n=== COMPROVACIÓ SI TOTS ELS NOMS COMENCEN PER 'G' ===");
                Console.WriteLine("Hauria de ser True perquè tenim GregMarc i Gregorio");
                bool flag = ctx.Professor.All(p => p.nom.StartsWith("G"));
                Console.WriteLine(flag);

                Console.WriteLine("\n=== ELIMINACIÓ D'UN PROFESSOR ===");
                Console.WriteLine("Eliminem el professor Santamaria");
                ctx.Professor.Remove(test2);
                ctx.SaveChanges();

                Console.WriteLine("............");

                Console.WriteLine("\n=== NAVEGACIÓ 1-1: CURS -> PROFESSOR ===");
                Console.WriteLine("Mostrem el nom del professor del primer curs");
                Curs c = ctx.Curs.Find(1);
                Console.WriteLine(c.Professor.nom);

                Console.WriteLine("\n=== NAVEGACIÓ COMPLEXA: PROFESSOR -> CURS -> CICLE ===");
                Console.WriteLine("Mostrem el cicle on ensenya el GregMarc");
                Professor m = ctx.Professor.Where(p => p.nom == "GregMarc").FirstOrDefault();
                Console.WriteLine("GregMarc ensenya a " + m.Curs.Cicle);

                Console.WriteLine("\nPrem ENTER per sortir...");
                Console.ReadLine();
            }
        }
    }
}