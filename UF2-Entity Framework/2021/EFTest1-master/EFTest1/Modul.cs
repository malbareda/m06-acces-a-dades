﻿// Modul.cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;

namespace EFTest1
{
    // Aquesta classe representa un mòdul formatiu
    public class Modul
    {
        // [Key] indica explícitament que aquesta és la clau primària
        // Útil quan no seguim la convenció de nomenclatura ID
        [Key]
        public int Codi { get; set; }

        // [Required] fa que el camp sigui NOT NULL
        [Required]
        public string nom { get; set; }

        // [Column] permet especificar el nom exacte de la columna a la base de dades
        // Útil quan volem seguir una convenció de nomenclatura específica
        [Column("hores_setmana")]
        public int horesSetmana { get; set; }

        // SqlDateTime és més restrictiu que DateTime
        // Assegura compatibilitat amb SQL Server
        public SqlDateTime dataInici { get; set; }

        // Relació 1-N amb Curs
        // Un mòdul pertany a un únic curs
        public Curs Curs { get; set; }

        // Relació N-N amb Professor
        // Un mòdul pot tenir molts professors i un professor pot tenir molts mòduls
        // Entity Framework crearà una taula intermèdia automàticament
        public virtual ICollection<Professor> Professors { get; set; }

        // Constructor que inicialitza les col·leccions
        // Important per evitar NullReferenceException
        public Modul()
        {
            // HashSet és més eficient que List per col·leccions grans
            this.Professors = new HashSet<Professor>();
            this.dataInici = new SqlDateTime(DateTime.Now);
        }
    }
}
