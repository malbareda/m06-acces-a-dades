﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRapid
{
    class Pilot
    {
        public int PilotID { get; set; }

        public string nom { get; set; }
        public int punts { get; set; }
        public bool llagrimes { get; set; }

        public Escuderia Escuderia { get; set; }

    }
}
