﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRapid
{
    class MichaelContext : DbContext
    {

        public MichaelContext() : base("FIA")
        {

        }

        public DbSet<Pilot> Pilot { get; set; }
        public DbSet<Escuderia> Escuderia { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<MichaelContext>());
            base.OnModelCreating(modelBuilder);
        }
    }
}
