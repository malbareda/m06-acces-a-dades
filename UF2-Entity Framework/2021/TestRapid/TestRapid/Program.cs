﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRapid
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new MichaelContext())
            {
                


                Pilot lewis = new Pilot() { nom = "Lewis", punts = 365, llagrimes = true };
                Pilot bottas = new Pilot() { nom = "Valteri", punts = 226, llagrimes = false };
                Pilot max = new Pilot() { nom = "Max", punts = 395, llagrimes = false };

                Escuderia mercedes = new Escuderia() { nom = "Mercedes" };
                Escuderia redBull = new Escuderia() { nom = "Red Bull" };

                mercedes.Pilots.Add(lewis);
                mercedes.Pilots.Add(bottas);
                redBull.Pilots.Add(max);

                ctx.Pilot.Add(lewis);
                ctx.Pilot.Add(bottas);
                ctx.Pilot.Add(max);

                ctx.Escuderia.Add(mercedes);
                ctx.Escuderia.Add(redBull);


                ctx.SaveChanges();
                Console.WriteLine(  "todo ha salido a pedir de Milhouse");


                Escuderia mer = ctx.Escuderia.Find(1);

                foreach(Pilot p in mer.Pilots.ToList())
                {
                    if (p.nom == "Valteri")
                    {
                        mer.Pilots.Remove(p);
                    }
                }

                foreach(Pilot p in mer.Pilots)
                {
                    Console.WriteLine(p.nom);
                }
                ctx.SaveChanges();

                List<Pilot> cries = ctx.Pilot
                                .Where(p => p.llagrimes == true)
                                .ToList();

                foreach(Pilot p in cries)
                {
                    Console.WriteLine(p.nom);
                }

                Console.ReadLine();
            }

        }
    }
}
