﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRapid
{
    class Escuderia
    {
        public int EscuderiaID { get; set; }
        public string nom { get; set; }

        public ICollection<Pilot> Pilots { get; set; }

        public Escuderia()
        {
            Pilots = new HashSet<Pilot>();
        }
    }
}
